var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');
var mysql = require('mysql');

// MySQL database connection
var mysqlPool = mysql.createPool({
    connectionLimit: 10,
    host: process.env.sql_host,
    user: process.env.sql_user,
    password: process.env.sql_password,
    database: process.env.sql_database
});

var queryStr = "SELECT * FROM infinia.view_dashboard_campaigns_group_campaigns";
mysqlPool.query(queryStr, function(err, body) {
    if (err) {
        var errorMessage = 'Error getting reporting';
        console.log('Campaign GET/_report_csv', errorMessage, err, queryStr);
        // res.status(500).json({
        //     message: errorMessage,
        //     error: err
        // });
    } else {
        var csvStream = csv.createWriteStream({
                headers: true
            }),
            writableStream = fs.createWriteStream("my.csv");
        writableStream.on("finish", function() {
            console.log("DONE!", body.length);
        });
        csvStream.pipe(writableStream);
        for (var i = 0; i < body.length; i++) {
            csvStream.write(body[i]);
        }
        csvStream.end();
    }
});
