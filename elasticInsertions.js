var mysql = require('mysql');
var request = require('request');
var pg = require('pg');

var fieldPromise = new Promise((resolve, reject) => {
    // MySQL database connection
    var mysqlPool = mysql.createPool({
        connectionLimit: 10,
        host: process.env.sql_host,
        user: process.env.sql_user,
        password: process.env.sql_password,
        database: process.env.sql_database
    });
    mysqlPool.query("SELECT id_publisher, publisher_name, publisher_token_token, publisher_token_description  FROM publishers p INNER JOIN publishers_tokens as pt ON pt.publishers_id_publisher = p.id_publisher", function(err, data) {
        if (err) {
            reject(err)
        } else {
            resolve(data);
        }
    });
});
fieldPromise.then(values => {

    for (var i = 0; i < values.length; i++) {
        var keys = Object.keys(values[i]);
        var body = {};
        var idDocument = values[i].publisher_token_token;
        for (var j = 0; j < keys.length; j++) {
            if (values[i][keys[j]] != undefined) {
                body[keys[j]] = values[i][keys[j]];
            }
        }
        var options = {
            method: 'PUT',
            url: 'http://search-dmp-poi-b5ehns3mgq4rfs37d7edvn5pn4.eu-west-1.es.amazonaws.com/report/publisher/' + idDocument,
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(body)
        };

        request(options, function(error, response, body) {
            if (error) {
                console.log(error);
            } else if (typeof(JSON.parse(body).error) !== "undefined") {
                console.log(JSON.parse(body).error);
            } else {
                var itemCreated = JSON.parse(body);
                // console.log(itemCreated);
            }
        });
    }
}).catch(error => {
    console.log('Error in publisher query', error);
});
