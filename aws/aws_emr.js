var sys = require('sys')
var exec = require('child_process').exec;
var _ = require('lodash'); 
var moment = require('moment');
var AWS = require('aws-sdk');
var mysql = require('mysql');
var credentials = new AWS.SharedIniFileCredentials({profile: process.argv[2]});
AWS.config.credentials = credentials;

var emr = new AWS.EMR({region : 'eu-west-1'});

var infiniaV2SqlPool = mysql.createPool({
    connectionLimit: 10,
    host: 'infinia-staging.cbzqxna4rbqs.eu-west-1.rds.amazonaws.com',
    user: 'infinia_pre',
    password: 'YXzZe2kGgJ4W83ZY',
    database: 'infinia_platform'
});

var inserted = 0;
var startMonth = new Date().getMonth();
var endMonth = startMonth + 1;

var args = process.argv.slice(3);
var startTime, endTime;

if (args[0]){
	var dateStart = moment(args[0], 'YYYYMMDD');
	startTime = dateStart.unix();
}
else {
	var start = new Date();
	start.setMonth(startMonth);
	start.setDate(1)
	start.setHours(0);
	start.setHours(0);
	start.setMinutes(0);
	start.setSeconds(0);
	startTime = start.getTime() / 1000;
}
if (args[1]){
	var dateEnd = moment(args[1], 'YYYYMMDD');
	endTime = dateEnd.unix();
}
else {
	var end = new Date();
	end.setMonth(endMonth);
	end.setDate(1);
	end.setHours(0);
	end.setMinutes(0);
	end.setSeconds(0);
	endTime = end.getTime() / 1000;
}
main(startTime*1000, endTime*1000);

function main(startTime, endTime){
	
	console.log('main', new Date(startTime), new Date(endTime));
	getAwsClusters(startTime, startTime + (24*60*60*1000), null, () => {
		startTime = startTime + (24*60*60*1000);

		if (startTime >= endTime){
			console.log("FIN. Inserted : " + inserted);
			process.exit(0);
		}
		else {
			main(startTime, endTime);
		}
	});
}

// time in seconds
function getAwsClusters(startTime, endTime, marker, callback){

	console.log('getAwsClusters', new Date(startTime), new Date(endTime), marker);
	var params = {
		CreatedAfter: startTime/1000,
	  	CreatedBefore: endTime/1000
	}

	if (marker){
		params.Marker = marker
	}

	emr.listClusters(params, (err, data) => {
	// var command = "aws emr --profile " + process.argv[2] + " list-clusters --created-after " + startTime + " --created-before " + endTime + " > listadoClusters.json";
	// var child = exec(command, (error, stdout, stderr) => {

		if (data && data.Clusters && data.Clusters.length > 0){
			var marker = data.Marker;
			var clusters = data.Clusters;

			readCluster(clusters, 0, () => {
				if (marker){
					getAwsClusters(startTime, endTime, marker, callback);
				}
				else {
					callback();
				}
			});
		}
		else {
			callback();
		}
	});
}

function readCluster(clusters, index, callback){

	console.log('readCluster', clusters.length, index);
	if (index < clusters.length){

		var d = clusters[index];

		if (d.Name.includes("INFINIA.DMP.CLUSTER.CALCULATIONS") > 0){
			emr.describeCluster({
				ClusterId : d.Id
			}, (err, data) => {

				if (data){
					var cluster = data.Cluster;

					emr.listInstanceFleets({
						ClusterId : d.Id
					}, (err, instanceData) => {

						var ownerTags = _.filter(cluster.Tags, (tag) => { return tag.Key === 'owner' });
						var ownerTag = null;
						if (ownerTags.length > 0){
							ownerTag = ownerTags[0].Value;
						}

						var createdByTags = _.filter(cluster.Tags, (tag) => { return tag.Key === 'created_by' });
						var createdByTag = 0;
						if (createdByTags.length > 0){
							createdByTag = createdByTags[0].Value;
						}

						var idTags = _.filter(cluster.Tags, (tag) => { return tag.Key === 'id' });
						var idClusterMongo = null;
						if (idTags.length > 0){
							idClusterMongo = idTags[0].Value;
						}
						var diffMinutes = (moment(cluster.Status.Timeline.EndDateTime).unix() - moment(cluster.Status.Timeline.CreationDateTime).unix()) / 60;

						var totalCoresCore = 0;
						var totalCoresMaster = 0;
						var diskMaster = 0;
						var diskCore = 0;
						var numberMachinesCore = 0;
						var numberMachinesMaster = 0;

						var masterInstance = "none";
						var coreInstance = "none";

						var coresByMachineCore = 0;
						var price = 0;

						if (instanceData && instanceData.InstanceFleets){
							var instanceFleets = instanceData.InstanceFleets;
							var coreInstanceFleet = _.filter(instanceFleets, (ifs) => { return ifs.InstanceFleetType === 'CORE' })[0];
							var masterInstanceFleet = _.filter(instanceFleets, (ifs) => { return ifs.InstanceFleetType === 'MASTER' })[0];

							totalCoresCore = coreInstanceFleet.TargetSpotCapacity;
							totalCoresMaster = coreInstanceFleet.InstanceTypeSpecifications[0].WeightedCapacity;
							diskMaster = masterInstanceFleet.InstanceTypeSpecifications[0].EbsBlockDevices[0].VolumeSpecification.SizeInGB;
							diskCore = coreInstanceFleet.InstanceTypeSpecifications[0].EbsBlockDevices[0].VolumeSpecification.SizeInGB;
							numberMachinesCore = totalCoresCore / totalCoresMaster;
							numberMachinesMaster = totalCoresMaster;

							masterInstance = masterInstanceFleet.InstanceTypeSpecifications[0].InstanceType;
							coreInstance = coreInstanceFleet.InstanceTypeSpecifications[0].InstanceType;

							coresByMachineCore = coreInstanceFleet.InstanceTypeSpecifications[0].WeightedCapacity

							var priceMasterMachine = 0;
							if (masterInstance === 'r4.8xlarge'){
								priceMasterMachine = 0.6;
							}
							else if (masterInstance === 'm3.xlarge'){
								priceMasterMachine = 0.06;
							}
							var priceCoreMachine = 0;
							if (coreInstance === 'r4.8xlarge'){
								priceCoreMachine = 0.6;
							}
							else if (coreInstance === 'm3.xlarge'){
								priceCoreMachine = 0.06;
							}
							
							price = (diffMinutes / 60) * ((numberMachinesMaster * priceMasterMachine) + (numberMachinesCore * priceCoreMachine));
						}

						var values = {
							id_cluster_aws : cluster.Id, // id_cluster_aws
							id_cluster_mongo : idClusterMongo, // id_cluster_mongo
							name : d.Name,
							owner : ownerTag,
							user : createdByTag,
							master_instance : masterInstance, // master_instance
							core_instance : coreInstance, // core_instance
							disk_master : diskMaster, // disk_master
							disk_core : diskCore, // disk_core
							cores_by_machine_core : coresByMachineCore, // cores_by_machine_core
							total_cores_core : totalCoresCore, // total_cores_core
							total_cores_master : totalCoresMaster, // total_cores_master
							number_machines_core : numberMachinesCore,// number_machines_core
							number_machines_master : numberMachinesMaster,// number_machines_master
							disk_total_master : totalCoresMaster * diskMaster, // disk_total_master
							disk_total_core : numberMachinesCore * diskCore,// disk_total_core
							hours : cluster.NormalizedInstanceHours, // hours
							ready_date_time : new Date(moment(cluster.Status.Timeline.ReadyDateTime).unix()*1000), // ready_date_time
							creation_date_time : new Date(moment(cluster.Status.Timeline.CreationDateTime).unix()*1000), // creation_date_time
							end_date_time : new Date(moment(cluster.Status.Timeline.EndDateTime).unix()*1000), // end_date_time
							difference_minutes : diffMinutes,// difference_minutes,
							state : cluster.Status.State, // state
							state_message : cluster.Status.StateChangeReason.Message, // state_message
							tags : _.map(cluster.Tags, (v) => { return v.Value; }).join(','), // tags,
							status : 1,
							price : price
						};

						setTimeout(() => {
							// console.log(values);						
							infiniaV2SqlPool.query("INSERT INTO cluster_report set ? ", values, (error, results, fields) => {
								
							 	if (error){
							 		console.log(error);
							 	}
							 	else {
							 		console.log("Inserted");
							 		inserted ++;
							 	}

							 	index ++;
							 	readCluster(clusters, index, callback);
							});
						}, 1000);
					});
				} else {
					console.log('No cluster data', err);
					index ++;
					readCluster(clusters, index, callback);
				}
			});
		} 
		else {
			console.log('Wrong cluster type');
			index ++;
			readCluster(clusters, index, callback);
		}
	} 
	else {
		callback();
	}
}