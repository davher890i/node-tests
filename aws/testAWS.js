var aws = require('aws-sdk');
const firehose = new aws.Firehose({
    region: 'eu-west-1'
});

var outputBeacons = [];
var firehoseParams = {
    DeliveryStreamName: "log_beacon",
    Records: outputBeacons
};

firehose.putRecordBatch(firehoseParams, function(err, data) {
    if (err) {
        console.log('Error', err);
    } else {
        console.log(data);
    }
});
