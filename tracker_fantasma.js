var moment = require('moment');

let dates = [
	'e0:cb:bc:48:2e:84,premium_litoral,26-06-2018,18-05-2019'
]

for (let i=0; i<dates.length; i++){
	let mac = dates[i].split(",")[0].toLowerCase();
	let fm = dates[i].split(",")[1];
	let start = dates[i].split(",")[2];
	let end = dates[i].split(",")[3];
	

	let startTimestamp = moment(start, "DD-MM-YYYY");
	let endTimestamp = moment(end, "DD-MM-YYYY");

	let query = {
	  "query": {
	    "bool": {
	      "must": [
	        {
	          "term": {
	            "ap_mac": mac 
	          }
	        },
	        {
	          "range": {
	            "date": {
	              "gte": startTimestamp.unix()*1000,
	              "lte": endTimestamp.unix()*1000
	            }
	          }
	        }
	      ]
	    }
	  },
	  "script": {
	    "inline": "ctx._source.ap_mac = '" + fm+ "';"
	  }
	}

	console.log(JSON.stringify(query));

	let mongoFind = {
		date: {
		    $gte: "new Date('" + moment(start, "DD-MM-YYYY").format("YYYY-MM-DD") + "T00:00:00.000Z')",
		    $lte: "new Date('" + moment(end, "DD-MM-YYYY").format("YYYY-MM-DD") + "T23:59:59.000Z')"
		},
		ap_mac : mac
	}
	let mongoUpdate = {
	    $set : { ap_mac : fm }
	}
	console.log('bulk.find(' + JSON.stringify(mongoFind) + ').update(' + JSON.stringify(mongoUpdate) + ');');

	let clickhouseQuery = "ALTER TABLE indoor.indoor_day_trackers UPDATE ap_mac = '" + fm + "' WHERE ap_mac = '" + mac + "' AND date BETWEEN '" + moment(start, "DD-MM-YYYY").format("YYYY-MM-DD") + "' AND '" + moment(end, "DD-MM-YYYY").format("YYYY-MM-DD") + "';";
	console.log(clickhouseQuery);
}
