var Twitter = require('twitter');
var _ = require('lodash');
var request = require('request');
var fs = require('fs')
var csv = require('fast-csv');
var moment = require('moment');

var terms = ['inseguridad', 'alumbrado', 'empleo', 'salud', 'contaminacion', 'corrupcion', 'trump'];

var filePath = __dirname + "/tweets_mexico.csv";
var writableStream = fs.createWriteStream(filePath, {
    flags: 'a'
});

var csvStream = csv.createWriteStream({
    headers: true
});
csvStream.pipe(writableStream);

function twitterStreaming() {

    var client = new Twitter({
        consumer_key: 'PnjhoisROYAZpzz79L8De7ix4',
        consumer_secret: '51myDZg2tkepX7vWF39iW6rRngIVVSToma4toTpVT4mfO5nHTh',
        access_token_key: '251271726-3f29DiLixfTRm8TJMzDKELeDUybDxRO00hw2gdAt',
        access_token_secret: 'zHOyIrgy1UZWMZb4Ew2fwUO8QDQtza6v6iXOJpHBMfdIB'
    });

    client.stream('statuses/filter', {
        // track: terms.join(','),
        // stall_warnings: true,
        // filter_level: 'medium',
        // place_id: '25530ba03b7d90c6'
        // place: "mejico,mexico",
        locations: "-118.65,14.39,-86.59,32.72"
    }, (stream) => {
        stream.on('data', (tweet) => {
            parseTweet(tweet);
        });

        stream.on('error', (error) => {
            console.log('Twitter streaming error', error, JSON.stringify(error.source));
        });

        stream.on('end', () => {
            console.log('Reconnecting');
            twitterStreaming();
        });
    });
}
twitterStreaming();

function parseTweet(tweet) {

    if (tweet.coordinates) {
        var lon = tweet.coordinates.coordinates[0];
        var lat = tweet.coordinates.coordinates[1];

        var tweetDate = Date.parse(tweet.created_at.replace(/( \+)/, ' UTC$1'));

        var hashtags = tweet.entities.hashtags;
        var matches = _.map(hashtags, (tag) => {
            // console.log(lat + ',' + lon + ',' + tweet.created_at + ',' + tag.text + ',' + tweet.text);
            var url = "https://infiniamaps.carto.com/api/v2/sql?api_key=faf8ad771e162441a5f299ada5e044e63e2a2ed9&q=INSERT INTO infiniamaps.tweet_data (term, date, text, the_geom) VALUES ('" + tag.text + "', '" + moment.unix(tweetDate).format('YYYY-MM-DDThh:mm:ss') + "', '" + encodeURIComponent(tweet.text) + "', ST_SetSRID(ST_Point(" + lon + ", " + lat + "),4326))"
            request({
                method: 'GET',
                url: url
            }, function(error, response, body) {
                if (error) {
                    console.log('Error inserting carto', error, url);
                }
            });
            // csvStream.write([lat, lon, tweetDate, tag.text, tweet.text]);
        });
    }
}

// parseTweet({
//     "created_at": "Fri Jun 09 13:01:45 +0000 2017",
//     "id": 873163204981018600,
//     "id_str": "873163204981018624",
//     "text": "Want to work at Trump Hotels? We're #hiring in #NewYork, NY! Click for details: https://t.co/9L4YU5ETqU #HR #Job #Jobs #CareerArc",
//     "source": "<a href=\"http://www.tweetmyjobs.com\" rel=\"nofollow\">TweetMyJOBS</a>",
//     "truncated": false,
//     "in_reply_to_status_id": null,
//     "in_reply_to_status_id_str": null,
//     "in_reply_to_user_id": null,
//     "in_reply_to_user_id_str": null,
//     "in_reply_to_screen_name": null,
//     "user": {
//         "id": 4646810601,
//         "id_str": "4646810601",
//         "name": "Trump Hotels Jobs",
//         "screen_name": "weareteamtrump",
//         "location": "Worldwide",
//         "url": "http://trumphotelcollection.com/hospitality-employment.php",
//         "description": "Bring your passion, drive and enthusiasm to our A+ team. We are looking for you! #hoteljobs #weareteamtrump",
//         "protected": false,
//         "verified": false,
//         "followers_count": 1648,
//         "friends_count": 38,
//         "listed_count": 326,
//         "favourites_count": 15,
//         "statuses_count": 231,
//         "created_at": "Tue Dec 29 21:07:58 +0000 2015",
//         "utc_offset": -14400,
//         "time_zone": "Eastern Time (US & Canada)",
//         "geo_enabled": true,
//         "lang": "en",
//         "contributors_enabled": false,
//         "is_translator": false,
//         "profile_background_color": "F5F8FA",
//         "profile_background_image_url": "",
//         "profile_background_image_url_https": "",
//         "profile_background_tile": false,
//         "profile_link_color": "1DA1F2",
//         "profile_sidebar_border_color": "C0DEED",
//         "profile_sidebar_fill_color": "DDEEF6",
//         "profile_text_color": "333333",
//         "profile_use_background_image": true,
//         "profile_image_url": "http://pbs.twimg.com/profile_images/691659244932718592/snRNgx4H_normal.jpg",
//         "profile_image_url_https": "https://pbs.twimg.com/profile_images/691659244932718592/snRNgx4H_normal.jpg",
//         "profile_banner_url": "https://pbs.twimg.com/profile_banners/4646810601/1452029599",
//         "default_profile": true,
//         "default_profile_image": false,
//         "following": null,
//         "follow_request_sent": null,
//         "notifications": null
//     },
//     "geo": {
//         "type": "Point",
//         "coordinates": [40.76912, -73.9815581]
//     },
//     "coordinates": {
//         "type": "Point",
//         "coordinates": [-73.9815581, 40.76912]
//     },
//     "place": {
//         "id": "01a9a39529b27f36",
//         "url": "https://api.twitter.com/1.1/geo/id/01a9a39529b27f36.json",
//         "place_type": "city",
//         "name": "Manhattan",
//         "full_name": "Manhattan, NY",
//         "country_code": "US",
//         "country": "United States",
//         "bounding_box": {
//             "type": "Polygon",
//             "coordinates": [
//                 [
//                     [-74.026675, 40.683935],
//                     [-74.026675, 40.877483],
//                     [-73.910408, 40.877483],
//                     [-73.910408, 40.683935]
//                 ]
//             ]
//         },
//         "attributes": {}
//     },
//     "contributors": null,
//     "is_quote_status": false,
//     "retweet_count": 0,
//     "favorite_count": 0,
//     "entities": {
//         "hashtags": [{
//             "text": "hiring",
//             "indices": [36, 43]
//         }, {
//             "text": "NewYork",
//             "indices": [47, 55]
//         }, {
//             "text": "HR",
//             "indices": [104, 107]
//         }, {
//             "text": "Job",
//             "indices": [108, 112]
//         }, {
//             "text": "Jobs",
//             "indices": [113, 118]
//         }, {
//             "text": "CareerArc",
//             "indices": [119, 129]
//         }],
//         "urls": [{
//             "url": "https://t.co/9L4YU5ETqU",
//             "expanded_url": "http://bit.ly/2rpDm9E",
//             "display_url": "bit.ly/2rpDm9E",
//             "indices": [80, 103]
//         }],
//         "user_mentions": [],
//         "symbols": []
//     },
//     "favorited": false,
//     "retweeted": false,
//     "possibly_sensitive": false,
//     "filter_level": "low",
//     "lang": "en",
//     "timestamp_ms": "1497013305312"
// });
