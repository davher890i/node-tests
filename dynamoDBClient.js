var AWS = require('aws-sdk');
var sts = new AWS.STS();
var moment = require('moment');

var stsPromise = new Promise((resolve, reject) => {
    var params = {
        RoleArn: 'arn:aws:iam::304259841217:role/externalDynamo',
        RoleSessionName: 'clustersession'
    };
    sts.assumeRole(params, function(err, data) {
        if (err) {
            reject(err);
        } else {
            resolve(data);
        }
    });
});

function scan(dynamoClient, params, callback, items = []) {
    dynamoClient.scan(params, function(err, data) {
        if (err) {
            callback(err, null);
        } else {
            if (data.Items.length > 0) {
                items = items.concat(data.Items);
            }
            if (data.LastEvaluatedKey) {
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                scan(dynamoClient, params, callback, items);
            } else {
                data.Items = items;
                callback(null, data);
            }
        }
    });
}

stsPromise.then((data) => {
    var creds = new AWS.Credentials({
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    });
    var docClient = new AWS.DynamoDB.DocumentClient({
        region: "eu-west-1",
        credentials: creds
    });

    var db = new AWS.DynamoDB.DocumentClient({
        region: "eu-west-1",
        //credentials: creds
    });
    // db.listTables(function(err, data) {
    //     console.log(data);
    // });
    // var params = {
    //     TableName: "deviceTokens",
    //     FilterExpression: "#key = :value",
    //     ExpressionAttributeNames: {
    //         "#key": "advertisingId"
    //     },
    //     ExpressionAttributeValues: {
    //         ":value": "0994160f-f848-4f73-a2b1-80a1c2fe1891"
    //     }
    // };
    //
    // scan(db, params, (err, result) => {
    //     if (err){
    //         console.log('Error', err);
    //     }
    //     else {
    //         console.log('Result', result);
    //     }
    // });
    // var params = {
    //     TableName: 'deviceTokens',
    //     Item: {
    //         "info": {
    //             "user_ip": "95.131.169.238",
    //             "app_token": "cf5f182db5e9686cb751a0da63ac27a8",
    //             "device_token": "e2fwXLgNa8I:APA91bGp1Sc5dqwxXMe-ZEfrwoAxPiBNNNJhbxwRVfW81vvOhbpSybvmyyUCXmxw0VODr0XtJ-oNPT6bSZyyQVADcb8W_EioFkVhW1M1pfaxVwakw3ucmnkXTYr4Jxa-BiUT-LwAPvbl",
    //             "advertising_id": "8ff2e7d0-1395-4203-933b-4b993fdece09",
    //             "sdk_version": "android20",
    //             "platform": "android",
    //             "timestamp": moment().unix()
    //         },
    //         "app_token": "cf5f182db5e9686cb751a0da63ac27a8",
    //         "advertisingId": "8ff2e7d0-1395-4203-933b-4b993fdece09"
    //     }
    // };
    // db.put(params, (err, data) => {
    //     if (err) {
    //         console.error("Unable to insert. Error:", JSON.stringify(err, null, 2));
    //     } else {
    //         console.log("insert succeeded.", JSON.stringify(data));
    //     }
    // });
    // db.delete({
    //     TableName: "INFINIA.DMP.CLUSTER.RULES",
    //     Key: {
    //         "info.device_token": "e2fwXLgNa8I:APA91bGp1Sc5dqwxXMe-ZEfrwoAxPiBNNNJhbxwRVfW81vvOhbpSybvmyyUCXmxw0VODr0XtJ-oNPT6bSZyyQVADcb8W_EioFkVhW1M1pfaxVwakw3ucmnkXTYr4Jxa-BiUT-LwAPvbl"
    //     }
    // }, function(err, data) {
    //     if (err) {
    //         console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    //     } else {
    //         console.log("Query succeeded.", JSON.stringify(data));
    //     }
    // });

    // var params = {
    //     TableName: "INFINIA.DMP.CLUSTER.STATISTICS",
    //     KeyConditionExpression: "#yr = :yyyy",
    //     ExpressionAttributeNames: {
    //         "#yr": "cluster_id"
    //     },
    //     ExpressionAttributeValues: {
    //         ":yyyy": "47"
    //     }
    // };
    // docClient.scan(params, function(err, data) {
    //     if (err) {
    //         console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    //     } else {
    //         console.log("Query succeeded.", JSON.stringify(data));
    //     }
    // });

    // for (var i = 0; i < 90; i++) {
    // var params = {
    //     TableName: "INFINIA.DMP.CLUSTER.RULES",
    //     Key: {
    //         "rule_id": "infinia-rule-Geo",
    //         "id": "71"
    //     }
    // };
    // docClient.delete(params, function(err, data) {
    //     if (err) {
    //         console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    //     } else {
    //         console.log("Query succeeded.", JSON.stringify(data));
    //     }
    // });
    // }

    // var params = {
    //     TableName: "INFINIA.DMP.CLUSTER.RULES",
    //     FilterExpression: "#id = :id",
    //     ExpressionAttributeNames: {
    //         "#id": "id",
    //     },
    //     ExpressionAttributeValues: {
    //         ":id": "137"
    //     }
    // };

    var params = {
        TableName: "INFINIA.DMP.CLUSTER.METADATA",
        FilterExpression: "#id = :id",
        ExpressionAttributeNames: {
            "#id": "id",
        },
        ExpressionAttributeValues: {
            ":id": "208"
        }
    };
    scan(docClient, params, (err, result) => {
        if (err){
            console.log('Error', err);
        }
        else {
            console.log('Result', result);
        }
    });
}).catch((error) => {
    console.log('STS promise error', error);
    res.status(500).json({
        status: 'error',
        error: error
    });
});
