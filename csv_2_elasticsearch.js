var fs = require('fs');
var csv = require('fast-csv');
var request = require('request');

var csvFile = process.argv[2];
var stream = fs.createReadStream(csvFile);

let objs = [];

var csvStream = csv()
    .on("data", function(data) {

        let id = data[0]
        let name = data[1]

        let body = {
            id : id,
            name : name
        }

        objs.push(body)
    })
    .on("end", function() {
        console.log('done');
        insertES(objs, 0)
    });

stream.pipe(csvStream);


function insertES(array, index){

    if (index < array.length){

        let body = array[index]

        var options = {
            method: 'POST',
            url: 'http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/categories/iab',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(body)
        };

        request(options, function(error, response, body) {
            if (error) {
                console.log(error);
            } else if (typeof(JSON.parse(body).error) !== "undefined") {
                console.log(JSON.parse(body).error);
            } else {
                console.log(index, "success");
                index = ++index
                insertES(array, index)
            }
        });
    }
}