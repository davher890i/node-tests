var request = require('request');
var moment = require('moment');
var _ = require('lodash');
var csv = require('fast-csv');
var fs = require('fs');

var fromDate = new moment("2017-04-01");
var stop = false;

var level = 'retailer';
var entityId = '5';

var writableStream = fs.createWriteStream("stats.csv");
writableStream.on("finish", function() {
    console.log("DONE!");
});
var csvStream = csv.createWriteStream({
    headers: true
});

csvStream.pipe(writableStream);

csvStream.write(['Date', 'Unique Perm.', 'Total users', 'Percent']);

while (!stop) {

    if (fromDate.format("YYYY-MM-DD") === moment().format("YYYY-MM-DD")) {
        stop = true;
    }

    var promiseStats = new Promise((resolve, reject) => {
        request({
            method: 'POST',
            url: 'http://localhost:3000/retailer/statsAgg',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                level: level,
                entityId: entityId,
                from: fromDate.format("YYYY-MM-DD") + 'T00:00:00',
                to: fromDate.format("YYYY-MM-DD") + 'T23:59:59'
            },
            json: true
        }, function(error, response, body) {
            if (error) {
                reject(error);
            } else {
                resolve(body);
            }
        });
    });

    var promisePerm = new Promise((resolve, reject) => {
        request({
            method: 'POST',
            url: 'http://localhost:3000/retailer/timeUniqueAgg',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                level: level,
                entityId: entityId,
                from: fromDate.format("YYYY-MM-DD") + 'T00:00:00',
                to: fromDate.format("YYYY-MM-DD") + 'T23:59:59'
            },
            json: true
        }, function(error, response, body) {
            if (error) {
                reject(error);
            } else {
                resolve(body);
            }
        });
    });

    Promise.all([promiseStats, promisePerm]).then(values => {
        var stats = values[0];

        var key = Object.keys(values[1])[0];

        var arrayValAge = _.map(stats.age[key], (val) => {
            return val;
        });
        var arrayValGender = _.map(stats.gender[key], (val) => {
            return val;
        });
        var arrayValIncome = _.map(stats.income_level[key], (val) => {
            return val;
        });

        var totalAge = arrayValAge.reduce((a, b) => {
            return a + b;
        }, 0) | 0;
        var totalGender = arrayValGender.reduce((a, b) => {
            return a + b;
        }, 0) | 0;
        var totalIncome = arrayValIncome.reduce((a, b) => {
            return a + b;
        }, 0) | 0;

        if (values[1][key]) {
            unique = values[1][key].uniquePerm;

            var percent = (Math.max(totalAge, totalGender, totalIncome) * 100) / unique;

            csvStream.write([key, unique, Math.max(totalAge, totalGender, totalIncome), percent.toFixed(2)]);
        }
    }).catch(err => {
        console.log('Error', err);
    });

    fromDate.add(1, 'days');
}



// var promiseMonthStats = new Promise((resolve, reject) => {
//     request({
//         method: 'POST',
//         url: 'http://localhost:3000/retailer/statsAgg',
//         headers: {
//             'content-type': 'application/json'
//         },
//         body: {
//             level: level,
//             entityId: entityId,
//             from: "2017-04-01T00:00:00",
//             to: "2017-05-01T00:00:00"
//         },
//         json: true
//     }, function(error, response, body) {
//         if (error) {
//             reject(error);
//         } else {
//             resolve(body);
//         }
//     });
// });
//
// var promiseMonthPerm = new Promise((resolve, reject) => {
//     request({
//         method: 'POST',
//         url: 'http://localhost:3000/retailer/timeUniqueAgg',
//         headers: {
//             'content-type': 'application/json'
//         },
//         body: {
//             level: level,
//             entityId: entityId,
//             from: "2017-04-01T00:00:00",
//             to: "2017-05-01T00:00:00"
//         },
//         json: true
//     }, function(error, response, body) {
//         if (error) {
//             reject(error);
//         } else {
//             resolve(body);
//         }
//     });
// });
//
// Promise.all([promiseMonthStats, promiseMonthPerm]).then(values => {
//     var stats = values[0];
//
//     var totalAge = _.map(stats.age, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalGender = _.map(stats.gender, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalIncome = _.map(stats.income_level, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalUnique = _.map(values[1], (val) => {
//         return val.uniquePerm;
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var percent = (Math.max(totalAge, totalGender, totalIncome) * 100) / totalUnique;
//
//     csvStream.write(['2017-04', totalUnique, Math.max(totalAge, totalGender, totalIncome), percent.toFixed(2)]);
// }).catch(err => {
//     console.log('Error', err);
// });
//
//
// var promiseMonthStats = new Promise((resolve, reject) => {
//     request({
//         method: 'POST',
//         url: 'http://localhost:3000/retailer/statsAgg',
//         headers: {
//             'content-type': 'application/json'
//         },
//         body: {
//             level: level,
//             entityId: entityId,
//             from: "2017-05-01T00:00:00",
//             to: "2017-06-01T00:00:00"
//         },
//         json: true
//     }, function(error, response, body) {
//         if (error) {
//             reject(error);
//         } else {
//             resolve(body);
//         }
//     });
// });
//
// var promiseMonthPerm = new Promise((resolve, reject) => {
//     request({
//         method: 'POST',
//         url: 'http://localhost:3000/retailer/timeUniqueAgg',
//         headers: {
//             'content-type': 'application/json'
//         },
//         body: {
//             level: level,
//             entityId: entityId,
//             from: "2017-05-01T00:00:00",
//             to: "2017-06-01T00:00:00"
//         },
//         json: true
//     }, function(error, response, body) {
//         if (error) {
//             reject(error);
//         } else {
//             resolve(body);
//         }
//     });
// });
//
// Promise.all([promiseMonthStats, promiseMonthPerm]).then(values => {
//     var stats = values[0];
//
//     var totalAge = _.map(stats.age, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalGender = _.map(stats.gender, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalIncome = _.map(stats.income_level, (val) => {
//         var sum = _.map(val, (num) => {
//             return num;
//         });
//         return sum.reduce((a, b) => {
//             return a + b;
//         }, 0);
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var totalUnique = _.map(values[1], (val) => {
//         return val.uniquePerm;
//     }).reduce((a, b) => {
//         return a + b;
//     }, 0) | 0;
//
//     var percent = (Math.max(totalAge, totalGender, totalIncome) * 100) / totalUnique;
//
//     csvStream.write(['2017-04', totalUnique, Math.max(totalAge, totalGender, totalIncome), percent.toFixed(2)]);
// }).catch(err => {
//     console.log('Error', err);
// });
