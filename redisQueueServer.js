'use strict';

var RedisSMQ = require("rsmq");
var RSMQWorker = require("rsmq-worker");

var _ = require('lodash');

function redisQueueServer() {
    var rsmq = new RedisSMQ({});

    rsmq.listQueues(function(err, queues) {
        if (err) {
            console.error(err)
            return
        } else {
            var q = _.map(queues, (q) => {
                if (q === 'pushDevices') {
                    return q;
                }
            });
            if (q.length === 0) {
                rsmq.createQueue({
                    qname: 'pushDevices'
                }, function(err, resp) {
                    if (resp === 1) {
                        console.log("queue created");
                    }
                });
            }

            var worker = new RSMQWorker(q, {
                host: 'localhost'
            });
            worker.on("message", function(msg, next, id) {
                console.log(new Date());
                // process your message
                console.log("Message id : " + id);
                console.log(msg);
                next();
            });

            // optional error listeners
            worker.on('error', function(err, msg) {
                console.log("ERROR", err, msg.id);
            });
            worker.on('exceeded', function(msg) {
                console.log("EXCEEDED", msg.id);
            });
            worker.on('timeout', function(msg) {
                console.log("TIMEOUT", msg.id, msg.rc);
            });

            worker.start();

        }
    });
}

function getMessages(rsmq, q) {
    rsmq.popMessage({
        qname: q
    }, function(err, resp) {
        console.log(resp);
        // if (resp.id) {
        //     console.log("Message received.", resp)
        // } else {
        //     console.log("No messages for me...")
        // }
    });
}

redisQueueServer();
