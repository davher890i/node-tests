// var pg = require('pg');
// pg.connect("jdbc:redshift://infinia-reports.cggnqbnat9bc.eu-west-1.redshift.amazonaws.com:5439/reporter?user=infinia&password=Inf1n1a2016", (err, client, done) => {
//
//     if (err) {
//         console.log('Error connecting postgresql', err);
//         done();
//     } else {
//         const query = client.query('SELECT data.age, data.gender, data.income_level FROM enrichment_data AS data limit 100');
//
//         // Stream results back one row at a time
//         query.on('row', (row) => {
//             console.log(row);
//         });
//
//         query.on('end', () => {
//             client.end();
//         });
//     }
// });

var Redshift = require('node-redshift');

var client = {
    user: process.env.redshift_user,
    database: process.env.redshift_database,
    password: process.env.redshift_password,
    port: process.env.redshift_port,
    host: process.env.redshift_host,
};

var redshiftClient = new Redshift(client);
redshiftClient.connect(function(err) {
    if (err) throw err;
    else {
        redshiftClient.query('SELECT data.age, data.gender, data.income_level FROM enrichment_data AS data limit 100', function(err, data) {
            if (err) throw err;
            else {
                var results = []
                for (var i = 0; i < data.rows.length; i++) {
                    var row = data.rows[i];
                    results.push({
                        age: row.age,
                        gender: row.gender,
                        income_level: row.income_level
                    });
                }
                // console.log(results);
                redshiftClient.close();
            }
        });
    }
});
