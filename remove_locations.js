location = [ 
            {
                "search" : "",
                "lng" : -106.1180216,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6390324
            }, 
            {
                "search" : "",
                "lng" : -94.5572847,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0005464
            }, 
            {
                "search" : "",
                "lng" : -106.0654875,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6262114
            }, 
            {
                "search" : "",
                "lng" : -98.1996737,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0399436
            }, 
            {
                "search" : "",
                "lng" : -97.8331049,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2768755
            }, 
            {
                "search" : "",
                "lng" : -99.2168629,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2282522
            }, 
            {
                "search" : "",
                "lng" : -100.2217926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6645072
            }, 
            {
                "search" : "",
                "lng" : -99.1787492,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4193541
            }, 
            {
                "search" : "",
                "lng" : -98.2021962,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0402599
            }, 
            {
                "search" : "",
                "lng" : -98.2137782,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0456994
            }, 
            {
                "search" : "",
                "lng" : -98.077902,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0473836
            }, 
            {
                "search" : "",
                "lng" : -93.1019313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7885088
            }, 
            {
                "search" : "",
                "lng" : -98.7435886,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.114574
            }, 
            {
                "search" : "",
                "lng" : -98.5586367,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5867537
            }, 
            {
                "search" : "",
                "lng" : -96.3598367,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4458276
            }, 
            {
                "search" : "",
                "lng" : -94.4189523,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1523215
            }, 
            {
                "search" : "",
                "lng" : -100.1546637,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9079927
            }, 
            {
                "search" : "",
                "lng" : -98.221095,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0036187
            }, 
            {
                "search" : "",
                "lng" : -100.3177113,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6618982
            }, 
            {
                "search" : "",
                "lng" : -102.5065328,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5576612
            }, 
            {
                "search" : "",
                "lng" : -106.4600965,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.731403
            }, 
            {
                "search" : "",
                "lng" : -98.4429546,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9099843
            }, 
            {
                "search" : "",
                "lng" : -96.7141419,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0937536
            }, 
            {
                "search" : "",
                "lng" : -97.681943,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3413519
            }, 
            {
                "search" : "",
                "lng" : -106.8547133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.406434
            }, 
            {
                "search" : "",
                "lng" : -98.3051558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0618788
            }, 
            {
                "search" : "",
                "lng" : -98.4335341,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9099065
            }, 
            {
                "search" : "",
                "lng" : -97.3878482,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.464709
            }, 
            {
                "search" : "",
                "lng" : -98.221653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0583689
            }, 
            {
                "search" : "",
                "lng" : -98.2074772,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0256444
            }, 
            {
                "search" : "",
                "lng" : -103.9741314,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6969674
            }, 
            {
                "search" : "",
                "lng" : -96.1320268,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1827267
            }, 
            {
                "search" : "",
                "lng" : -99.6879767,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2913181
            }, 
            {
                "search" : "",
                "lng" : -99.1427441,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4187411
            }, 
            {
                "search" : "",
                "lng" : -96.9127833,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5294281
            }, 
            {
                "search" : "",
                "lng" : -98.1961828,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.053034
            }, 
            {
                "search" : "",
                "lng" : -98.977956,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8450921
            }, 
            {
                "search" : "",
                "lng" : -98.2160289,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0482992
            }, 
            {
                "search" : "",
                "lng" : -99.1668468,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7123504
            }, 
            {
                "search" : "",
                "lng" : -98.2056049,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.037246
            }, 
            {
                "search" : "",
                "lng" : -104.400429,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.5449297
            }, 
            {
                "search" : "",
                "lng" : -98.1973603,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0317668
            }, 
            {
                "search" : "",
                "lng" : -98.203497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.034829
            }, 
            {
                "search" : "",
                "lng" : -98.2337523,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0502988
            }, 
            {
                "search" : "",
                "lng" : -98.2133717,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0685392
            }, 
            {
                "search" : "",
                "lng" : -107.8881926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.4146145
            }, 
            {
                "search" : "",
                "lng" : -97.3742095,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4648622
            }, 
            {
                "search" : "",
                "lng" : -105.4762207,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1865241
            }, 
            {
                "search" : "",
                "lng" : -92.0973913,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.9066256
            }, 
            {
                "search" : "",
                "lng" : -99.1729723,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3878676
            }, 
            {
                "search" : "",
                "lng" : -92.9565502,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9761412
            }, 
            {
                "search" : "",
                "lng" : -98.2173364,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0172076
            }, 
            {
                "search" : "",
                "lng" : -97.7288354,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.886138
            }, 
            {
                "search" : "",
                "lng" : -97.4492321,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9949803
            }, 
            {
                "search" : "",
                "lng" : -98.3086889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0616223
            }, 
            {
                "search" : "",
                "lng" : -97.3917362,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4594935
            }, 
            {
                "search" : "",
                "lng" : -98.2944657,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0719412
            }, 
            {
                "search" : "",
                "lng" : -98.2149712,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0374679
            }, 
            {
                "search" : "",
                "lng" : -98.2263742,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0422367
            }, 
            {
                "search" : "",
                "lng" : -98.2084047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0300895
            }, 
            {
                "search" : "",
                "lng" : -100.3596581,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6957153
            }, 
            {
                "search" : "",
                "lng" : -100.3673148,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5939445
            }, 
            {
                "search" : "",
                "lng" : -108.1413845,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1941834
            }, 
            {
                "search" : "",
                "lng" : -93.1000586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7302605
            }, 
            {
                "search" : "",
                "lng" : -93.118744,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.751399
            }, 
            {
                "search" : "",
                "lng" : -98.1951141,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0420883
            }, 
            {
                "search" : "",
                "lng" : -98.2441573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0414215
            }, 
            {
                "search" : "",
                "lng" : -105.6717613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9283372
            }, 
            {
                "search" : "",
                "lng" : -93.1135425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7499079
            }, 
            {
                "search" : "",
                "lng" : -104.6759861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0224412
            }, 
            {
                "search" : "",
                "lng" : -101.6789437,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1199799
            }, 
            {
                "search" : "",
                "lng" : -110.9515915,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3142247
            }, 
            {
                "search" : "",
                "lng" : -109.9327565,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5144516
            }, 
            {
                "search" : "",
                "lng" : -103.4644679,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7162199
            }, 
            {
                "search" : "",
                "lng" : -96.9875569,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3331209
            }, 
            {
                "search" : "",
                "lng" : -99.9899507,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3888248
            }, 
            {
                "search" : "",
                "lng" : -102.2965552,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8791098
            }, 
            {
                "search" : "",
                "lng" : -97.4044948,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4666002
            }, 
            {
                "search" : "",
                "lng" : -95.2086031,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.1883931
            }, 
            {
                "search" : "",
                "lng" : -97.4634451,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5293011
            }, 
            {
                "search" : "",
                "lng" : -100.381869,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5967359
            }, 
            {
                "search" : "",
                "lng" : -99.5140653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.283935
            }, 
            {
                "search" : "",
                "lng" : -99.9937626,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.392789
            }, 
            {
                "search" : "",
                "lng" : -95.0222115,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.439128
            }, 
            {
                "search" : "",
                "lng" : -97.3893865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4664631
            }, 
            {
                "search" : "",
                "lng" : -98.2120492,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0495613
            }, 
            {
                "search" : "",
                "lng" : -98.3113111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0624211
            }, 
            {
                "search" : "",
                "lng" : -98.2042325,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0420748
            }, 
            {
                "search" : "",
                "lng" : -97.0700124,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.8590639
            }, 
            {
                "search" : "",
                "lng" : -100.2936861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6649345
            }, 
            {
                "search" : "",
                "lng" : -97.3964313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4691768
            }, 
            {
                "search" : "",
                "lng" : -97.7305907,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8872703
            }, 
            {
                "search" : "",
                "lng" : -100.9472278,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1858721
            }, 
            {
                "search" : "",
                "lng" : -98.1810563,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0773212
            }, 
            {
                "search" : "",
                "lng" : -98.2164497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0286545
            }, 
            {
                "search" : "",
                "lng" : -98.2116986,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0354396
            }, 
            {
                "search" : "",
                "lng" : -104.6691643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.9776359
            }, 
            {
                "search" : "",
                "lng" : -98.2214652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0552897
            }, 
            {
                "search" : "",
                "lng" : -94.9018808,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9455691
            }, 
            {
                "search" : "",
                "lng" : -104.9162317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.5198625
            }, 
            {
                "search" : "",
                "lng" : -102.4677037,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5889218
            }, 
            {
                "search" : "",
                "lng" : -99.2321191,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9190186
            }, 
            {
                "search" : "",
                "lng" : -101.1829654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1314546
            }, 
            {
                "search" : "",
                "lng" : -101.4576663,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2343775
            }, 
            {
                "search" : "",
                "lng" : -99.239983,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9403439
            }, 
            {
                "search" : "",
                "lng" : -100.4131009,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6043214
            }, 
            {
                "search" : "",
                "lng" : -100.4336984,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.635797
            }, 
            {
                "search" : "",
                "lng" : -100.436124,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6348016
            }, 
            {
                "search" : "",
                "lng" : -100.4276555,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6374594
            }, 
            {
                "search" : "",
                "lng" : -100.1482315,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3773422
            }, 
            {
                "search" : "",
                "lng" : -99.1381298,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2976032
            }, 
            {
                "search" : "",
                "lng" : -99.1364572,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5262454
            }, 
            {
                "search" : "",
                "lng" : -101.972422,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2731532
            }, 
            {
                "search" : "",
                "lng" : -101.17676,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6964612
            }, 
            {
                "search" : "",
                "lng" : -110.9313886,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.2893227
            }, 
            {
                "search" : "",
                "lng" : -102.2894008,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9063748
            }, 
            {
                "search" : "",
                "lng" : -102.8706822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.1642562
            }, 
            {
                "search" : "",
                "lng" : -99.1732036,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3994829
            }, 
            {
                "search" : "",
                "lng" : -99.2313194,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4332792
            }, 
            {
                "search" : "",
                "lng" : -99.2099308,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9685815
            }, 
            {
                "search" : "",
                "lng" : -110.9547707,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0562485
            }, 
            {
                "search" : "",
                "lng" : -96.9252699,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5413582
            }, 
            {
                "search" : "",
                "lng" : -97.0640542,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0590154
            }, 
            {
                "search" : "",
                "lng" : -101.9329672,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3572948
            }, 
            {
                "search" : "",
                "lng" : -102.9359305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3827452
            }, 
            {
                "search" : "",
                "lng" : -103.3982418,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6293198
            }, 
            {
                "search" : "",
                "lng" : -96.7097304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0962631
            }, 
            {
                "search" : "",
                "lng" : -103.416983,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6749307
            }, 
            {
                "search" : "",
                "lng" : -96.1269135,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1870698
            }, 
            {
                "search" : "",
                "lng" : -103.9244593,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4317797
            }, 
            {
                "search" : "",
                "lng" : -101.2509656,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0175902
            }, 
            {
                "search" : "",
                "lng" : -109.0129996,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7736535
            }, 
            {
                "search" : "",
                "lng" : -100.2903615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6569749
            }, 
            {
                "search" : "",
                "lng" : -103.2952038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6194466
            }, 
            {
                "search" : "",
                "lng" : -105.2256973,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6573656
            }, 
            {
                "search" : "",
                "lng" : -98.7806169,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0686476
            }, 
            {
                "search" : "",
                "lng" : -102.2749355,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9189219
            }, 
            {
                "search" : "",
                "lng" : -100.2941428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6281779
            }, 
            {
                "search" : "",
                "lng" : -96.1477575,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1764441
            }, 
            {
                "search" : "",
                "lng" : -99.2881217,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5908932
            }, 
            {
                "search" : "",
                "lng" : -106.0790487,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6327848
            }, 
            {
                "search" : "",
                "lng" : -110.9643653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0944608
            }, 
            {
                "search" : "",
                "lng" : -98.45407,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.711382
            }, 
            {
                "search" : "",
                "lng" : -100.4201745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7491344
            }, 
            {
                "search" : "",
                "lng" : -100.4054038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7411411
            }, 
            {
                "search" : "",
                "lng" : -97.0825056,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.464465
            }, 
            {
                "search" : "",
                "lng" : -100.341868,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.639282
            }, 
            {
                "search" : "",
                "lng" : -100.4111789,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6572889
            }, 
            {
                "search" : "",
                "lng" : -103.2407237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2990448
            }, 
            {
                "search" : "",
                "lng" : -98.5722665,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7794323
            }, 
            {
                "search" : "",
                "lng" : -98.8610049,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5106021
            }, 
            {
                "search" : "",
                "lng" : -98.9521357,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2743499
            }, 
            {
                "search" : "",
                "lng" : -101.3496238,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.674872
            }, 
            {
                "search" : "",
                "lng" : -103.5339269,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5381646
            }, 
            {
                "search" : "",
                "lng" : -98.4709132,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6070079
            }, 
            {
                "search" : "",
                "lng" : -95.1081939,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8040998
            }, 
            {
                "search" : "",
                "lng" : -100.4475775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6343899
            }, 
            {
                "search" : "",
                "lng" : -107.6957224,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.765837
            }, 
            {
                "search" : "",
                "lng" : -99.2160192,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4254738
            }, 
            {
                "search" : "",
                "lng" : -99.2363899,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9348036
            }, 
            {
                "search" : "",
                "lng" : -97.9211555,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.4077462
            }, 
            {
                "search" : "",
                "lng" : -99.5898567,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9543976
            }, 
            {
                "search" : "",
                "lng" : -100.7336555,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8031036
            }, 
            {
                "search" : "",
                "lng" : -97.0869062,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8379174
            }, 
            {
                "search" : "",
                "lng" : -100.9748785,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1529714
            }, 
            {
                "search" : "",
                "lng" : -97.822187,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2607005
            }, 
            {
                "search" : "",
                "lng" : -102.2937496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8860161
            }, 
            {
                "search" : "",
                "lng" : -102.3029507,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9572996
            }, 
            {
                "search" : "",
                "lng" : -111.9835831,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.8922164
            }, 
            {
                "search" : "",
                "lng" : -103.374907,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6847753
            }, 
            {
                "search" : "",
                "lng" : -104.9230692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.1301401
            }, 
            {
                "search" : "",
                "lng" : -90.2618068,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.931225
            }, 
            {
                "search" : "",
                "lng" : -101.1169701,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8569888
            }, 
            {
                "search" : "",
                "lng" : -100.8994044,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6314909
            }, 
            {
                "search" : "",
                "lng" : -99.2655797,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9670687
            }, 
            {
                "search" : "",
                "lng" : -99.1637282,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.39382
            }, 
            {
                "search" : "",
                "lng" : -99.0540304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5691732
            }, 
            {
                "search" : "",
                "lng" : -99.1720176,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4106287
            }, 
            {
                "search" : "",
                "lng" : -99.5096746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.462675
            }, 
            {
                "search" : "",
                "lng" : -100.1872135,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6849
            }, 
            {
                "search" : "",
                "lng" : -99.659683,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.278213
            }, 
            {
                "search" : "",
                "lng" : -103.3759216,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6433833
            }, 
            {
                "search" : "",
                "lng" : -103.830502,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3674922
            }, 
            {
                "search" : "",
                "lng" : -104.7098448,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.023836
            }, 
            {
                "search" : "",
                "lng" : -103.4553049,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6816363
            }, 
            {
                "search" : "",
                "lng" : -99.176709,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.303659
            }, 
            {
                "search" : "",
                "lng" : -105.6567304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9202429
            }, 
            {
                "search" : "",
                "lng" : -100.3466317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6348606
            }, 
            {
                "search" : "",
                "lng" : -99.1277289,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.729385
            }, 
            {
                "search" : "",
                "lng" : -103.4742188,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5474803
            }, 
            {
                "search" : "",
                "lng" : -99.278615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3836031
            }, 
            {
                "search" : "",
                "lng" : -99.2775224,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3866269
            }, 
            {
                "search" : "",
                "lng" : -99.1521711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6710974
            }, 
            {
                "search" : "",
                "lng" : -99.1259478,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2639736
            }, 
            {
                "search" : "",
                "lng" : -99.0759223,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7813389
            }, 
            {
                "search" : "",
                "lng" : -100.4040899,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5325828
            }, 
            {
                "search" : "",
                "lng" : -102.3662811,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3436284
            }, 
            {
                "search" : "",
                "lng" : -100.6940013,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0265178
            }, 
            {
                "search" : "",
                "lng" : -107.3834048,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7948663
            }, 
            {
                "search" : "",
                "lng" : -96.8305364,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5308587
            }, 
            {
                "search" : "",
                "lng" : -103.0213677,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9891511
            }, 
            {
                "search" : "",
                "lng" : -102.3140884,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8807407
            }, 
            {
                "search" : "",
                "lng" : -104.6729682,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0268756
            }, 
            {
                "search" : "",
                "lng" : -98.3021528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1661717
            }, 
            {
                "search" : "",
                "lng" : -102.5865558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7641984
            }, 
            {
                "search" : "",
                "lng" : -96.8880497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5039304
            }, 
            {
                "search" : "",
                "lng" : -99.1801571,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3499235
            }, 
            {
                "search" : "",
                "lng" : -100.302295,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7061687
            }, 
            {
                "search" : "",
                "lng" : -100.5445681,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6789875
            }, 
            {
                "search" : "",
                "lng" : 2.21031119999998,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 41.4045492
            }, 
            {
                "search" : "",
                "lng" : -96.7087663,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0525626
            }, 
            {
                "search" : "",
                "lng" : -90.5310711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8510358
            }, 
            {
                "search" : "",
                "lng" : -98.2278027,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.05764
            }, 
            {
                "search" : "",
                "lng" : -101.6906416,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1329452
            }, 
            {
                "search" : "",
                "lng" : -99.0713692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.349205
            }, 
            {
                "search" : "",
                "lng" : -98.1593991,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1189154
            }, 
            {
                "search" : "",
                "lng" : -99.0603023,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4544923
            }, 
            {
                "search" : "",
                "lng" : -99.2092527,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4782406
            }, 
            {
                "search" : "",
                "lng" : -103.3914909,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5223726
            }, 
            {
                "search" : "",
                "lng" : -99.235415,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8421366
            }, 
            {
                "search" : "",
                "lng" : -100.0010898,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3901359
            }, 
            {
                "search" : "",
                "lng" : -99.1636439,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9141091
            }, 
            {
                "search" : "",
                "lng" : -99.0415502,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6047553
            }, 
            {
                "search" : "",
                "lng" : -117.1196037,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5276317
            }, 
            {
                "search" : "",
                "lng" : -116.6076009,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.576024
            }, 
            {
                "search" : "",
                "lng" : -98.3758762,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2165831
            }, 
            {
                "search" : "",
                "lng" : -99.212775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6870191
            }, 
            {
                "search" : "",
                "lng" : -98.8625075,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6974693
            }, 
            {
                "search" : "",
                "lng" : -109.9519407,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.8992534
            }, 
            {
                "search" : "",
                "lng" : -96.9385932,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5452888
            }, 
            {
                "search" : "",
                "lng" : -97.3944703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4552283
            }, 
            {
                "search" : "",
                "lng" : -98.1137319,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4067652
            }, 
            {
                "search" : "",
                "lng" : -96.9309462,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.899317
            }, 
            {
                "search" : "",
                "lng" : -99.0132678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.685235
            }, 
            {
                "search" : "",
                "lng" : -100.3762619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5874674
            }, 
            {
                "search" : "",
                "lng" : -96.9384619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8765289
            }, 
            {
                "search" : "",
                "lng" : -99.061639,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.462008
            }, 
            {
                "search" : "",
                "lng" : -99.0638389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4520189
            }, 
            {
                "search" : "",
                "lng" : -116.6098778,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8569131
            }, 
            {
                "search" : "",
                "lng" : -100.9694534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4571563
            }, 
            {
                "search" : "",
                "lng" : -103.3733016,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5601006
            }, 
            {
                "search" : "",
                "lng" : -99.0914098,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4076076
            }, 
            {
                "search" : "",
                "lng" : -103.4494933,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.554722
            }, 
            {
                "search" : "",
                "lng" : -99.592205,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2866587
            }, 
            {
                "search" : "",
                "lng" : -86.8261132,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1323217
            }, 
            {
                "search" : "",
                "lng" : -99.0248849,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4272786
            }, 
            {
                "search" : "",
                "lng" : -101.1625098,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6868555
            }, 
            {
                "search" : "",
                "lng" : -86.8564904,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1336779
            }, 
            {
                "search" : "",
                "lng" : -100.3955953,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5474244
            }, 
            {
                "search" : "",
                "lng" : -99.2773807,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3719393
            }, 
            {
                "search" : "",
                "lng" : -99.2584149,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3597191
            }, 
            {
                "search" : "",
                "lng" : -101.0188053,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1253111
            }, 
            {
                "search" : "",
                "lng" : -101.0197428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1204433
            }, 
            {
                "search" : "",
                "lng" : -99.2610945,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4850411
            }, 
            {
                "search" : "",
                "lng" : -99.2144828,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3972664
            }, 
            {
                "search" : "",
                "lng" : -103.4389864,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.546067
            }, 
            {
                "search" : "",
                "lng" : -99.8723935,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8681428
            }, 
            {
                "search" : "",
                "lng" : -102.2707978,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8767292
            }, 
            {
                "search" : "",
                "lng" : -97.5460958,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8797115
            }, 
            {
                "search" : "",
                "lng" : -99.4878016,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.53288
            }, 
            {
                "search" : "",
                "lng" : -99.2068775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5139138
            }, 
            {
                "search" : "",
                "lng" : -99.1738351,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4961935
            }, 
            {
                "search" : "",
                "lng" : -99.1744838,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4871774
            }, 
            {
                "search" : "",
                "lng" : -99.1668422,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8953931
            }, 
            {
                "search" : "",
                "lng" : -99.1662613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4533267
            }, 
            {
                "search" : "",
                "lng" : -99.5235317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3449338
            }, 
            {
                "search" : "",
                "lng" : -98.2344878,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3424667
            }, 
            {
                "search" : "",
                "lng" : -103.4144713,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6133424
            }, 
            {
                "search" : "",
                "lng" : -100.4165544,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6481333
            }, 
            {
                "search" : "",
                "lng" : -98.2654434,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0836909
            }, 
            {
                "search" : "",
                "lng" : -92.6438295,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7361855
            }, 
            {
                "search" : "",
                "lng" : -95.2164141,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4326157
            }, 
            {
                "search" : "",
                "lng" : -103.5802093,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2311592
            }, 
            {
                "search" : "",
                "lng" : -100.9340534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1621922
            }, 
            {
                "search" : "",
                "lng" : -106.4178692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2195697
            }, 
            {
                "search" : "",
                "lng" : -106.3873823,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6772556
            }, 
            {
                "search" : "",
                "lng" : -102.5086788,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7349636
            }, 
            {
                "search" : "",
                "lng" : -103.454912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7350077
            }, 
            {
                "search" : "",
                "lng" : -117.022844,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5163609
            }, 
            {
                "search" : "",
                "lng" : -99.2377976,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3724537
            }, 
            {
                "search" : "",
                "lng" : -99.1943645,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5830425
            }, 
            {
                "search" : "",
                "lng" : -99.2306436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5775525
            }, 
            {
                "search" : "",
                "lng" : -99.2221643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0569848
            }, 
            {
                "search" : "",
                "lng" : -96.7245583,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0618966
            }, 
            {
                "search" : "",
                "lng" : -98.2403598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3053529
            }, 
            {
                "search" : "",
                "lng" : -99.5471781,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.954779
            }, 
            {
                "search" : "",
                "lng" : -98.9358561,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.7137319
            }, 
            {
                "search" : "",
                "lng" : -99.5776153,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2571731
            }, 
            {
                "search" : "",
                "lng" : -104.9076322,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.5069143
            }, 
            {
                "search" : "",
                "lng" : -99.1592335,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4670805
            }, 
            {
                "search" : "",
                "lng" : -98.8553814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8015354
            }, 
            {
                "search" : "",
                "lng" : -99.0803366,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4566884
            }, 
            {
                "search" : "",
                "lng" : -99.588613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.109783
            }, 
            {
                "search" : "",
                "lng" : -100.1333083,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2011401
            }, 
            {
                "search" : "",
                "lng" : -99.5057584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5582946
            }, 
            {
                "search" : "",
                "lng" : -106.4444584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2691291
            }, 
            {
                "search" : "",
                "lng" : -97.0580058,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0744486
            }, 
            {
                "search" : "",
                "lng" : -102.1025389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0145478
            }, 
            {
                "search" : "",
                "lng" : -98.6595678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6477105
            }, 
            {
                "search" : "",
                "lng" : -99.1660876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4562002
            }, 
            {
                "search" : "",
                "lng" : -113.5388865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.2974281
            }, 
            {
                "search" : "",
                "lng" : -99.8678786,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8670559
            }, 
            {
                "search" : "",
                "lng" : -99.2739947,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6023581
            }, 
            {
                "search" : "",
                "lng" : -99.6561092,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2880781
            }, 
            {
                "search" : "",
                "lng" : -100.422313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.662034
            }, 
            {
                "search" : "",
                "lng" : -99.2654903,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3070481
            }, 
            {
                "search" : "",
                "lng" : -102.2977536,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8457943
            }, 
            {
                "search" : "",
                "lng" : -100.9194993,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1471716
            }, 
            {
                "search" : "",
                "lng" : -117.1108207,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5276396
            }, 
            {
                "search" : "",
                "lng" : -99.1654865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4273939
            }, 
            {
                "search" : "",
                "lng" : -100.0050296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3912325
            }, 
            {
                "search" : "",
                "lng" : -107.9046705,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.4260453
            }, 
            {
                "search" : "",
                "lng" : -99.1766306,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5212062
            }, 
            {
                "search" : "",
                "lng" : -99.6662628,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3208058
            }, 
            {
                "search" : "",
                "lng" : -99.2026027,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6708689
            }, 
            {
                "search" : "",
                "lng" : -100.9315622,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1376036
            }, 
            {
                "search" : "",
                "lng" : -98.9471849,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8238906
            }, 
            {
                "search" : "",
                "lng" : -97.0471556,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0683148
            }, 
            {
                "search" : "",
                "lng" : -116.6015229,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8674298
            }, 
            {
                "search" : "",
                "lng" : -100.2726352,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6298231
            }, 
            {
                "search" : "",
                "lng" : -99.2065202,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7185096
            }, 
            {
                "search" : "",
                "lng" : -99.2533636,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5568569
            }, 
            {
                "search" : "",
                "lng" : -102.308585,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2299416
            }, 
            {
                "search" : "",
                "lng" : -99.164589,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.363234
            }, 
            {
                "search" : "",
                "lng" : -99.2826309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5030891
            }, 
            {
                "search" : "",
                "lng" : -99.1141689,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.328661
            }, 
            {
                "search" : "",
                "lng" : -99.651298,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2817678
            }, 
            {
                "search" : "",
                "lng" : -99.1290319,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.340553
            }, 
            {
                "search" : "",
                "lng" : -99.1682101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5297608
            }, 
            {
                "search" : "",
                "lng" : -98.769241,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5903626
            }, 
            {
                "search" : "",
                "lng" : -99.2373846,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9822001
            }, 
            {
                "search" : "",
                "lng" : -99.8352001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8915742
            }, 
            {
                "search" : "",
                "lng" : -98.9055665,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4088656
            }, 
            {
                "search" : "",
                "lng" : -103.078149,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6149566
            }, 
            {
                "search" : "",
                "lng" : -101.5847655,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.6699151
            }, 
            {
                "search" : "",
                "lng" : -99.1098022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3609403
            }, 
            {
                "search" : "",
                "lng" : -107.4150845,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7946397
            }, 
            {
                "search" : "",
                "lng" : -105.4734047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1883467
            }, 
            {
                "search" : "",
                "lng" : -98.8981261,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2027585
            }, 
            {
                "search" : "",
                "lng" : -107.3602723,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8525723
            }, 
            {
                "search" : "",
                "lng" : -96.7240789,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9706023
            }, 
            {
                "search" : "",
                "lng" : -101.1761746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1313852
            }, 
            {
                "search" : "",
                "lng" : -106.4227946,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.739002
            }, 
            {
                "search" : "",
                "lng" : -99.0908058,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4666824
            }, 
            {
                "search" : "",
                "lng" : -99.1270862,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2617042
            }, 
            {
                "search" : "",
                "lng" : -99.0950473,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6428915
            }, 
            {
                "search" : "",
                "lng" : -99.1743317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4773807
            }, 
            {
                "search" : "",
                "lng" : -99.1388807,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2658448
            }, 
            {
                "search" : "",
                "lng" : -99.0950463,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4861838
            }, 
            {
                "search" : "",
                "lng" : -102.3400343,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.932612
            }, 
            {
                "search" : "",
                "lng" : -99.200367,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.560208
            }, 
            {
                "search" : "",
                "lng" : -103.5243131,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5380038
            }, 
            {
                "search" : "",
                "lng" : -107.4070599,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.771947
            }, 
            {
                "search" : "",
                "lng" : -99.0062545,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5968999
            }, 
            {
                "search" : "",
                "lng" : -99.0754891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3091446
            }, 
            {
                "search" : "",
                "lng" : -99.1952364,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3684684
            }, 
            {
                "search" : "",
                "lng" : -102.2357954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.5318122
            }, 
            {
                "search" : "",
                "lng" : -103.428421,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6129065
            }, 
            {
                "search" : "",
                "lng" : -99.1549154,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5314759
            }, 
            {
                "search" : "",
                "lng" : -106.1110039,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6952449
            }, 
            {
                "search" : "",
                "lng" : -99.0002023,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3466023
            }, 
            {
                "search" : "",
                "lng" : -99.2131413,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4519742
            }, 
            {
                "search" : "",
                "lng" : -109.4454922,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.0849355
            }, 
            {
                "search" : "",
                "lng" : -99.0990087,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3927752
            }, 
            {
                "search" : "",
                "lng" : -105.6992673,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9372162
            }, 
            {
                "search" : "",
                "lng" : -104.8665007,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4769662
            }, 
            {
                "search" : "",
                "lng" : -106.0809188,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6605254
            }, 
            {
                "search" : "",
                "lng" : -98.1681694,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0688824
            }, 
            {
                "search" : "",
                "lng" : -100.5684392,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.690615
            }, 
            {
                "search" : "",
                "lng" : -102.2501649,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8708754
            }, 
            {
                "search" : "",
                "lng" : -96.9226416,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5544859
            }, 
            {
                "search" : "",
                "lng" : -99.1192277,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.351169
            }, 
            {
                "search" : "",
                "lng" : -101.4231185,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9278479
            }, 
            {
                "search" : "",
                "lng" : -101.9242511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.319029
            }, 
            {
                "search" : "",
                "lng" : -99.0980607,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4153992
            }, 
            {
                "search" : "",
                "lng" : -91.0431367,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1859294
            }, 
            {
                "search" : "",
                "lng" : -98.234699,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9947328
            }, 
            {
                "search" : "",
                "lng" : -100.3013679,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9143557
            }, 
            {
                "search" : "",
                "lng" : -96.9217473,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8786617
            }, 
            {
                "search" : "",
                "lng" : -99.0651798,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4666784
            }, 
            {
                "search" : "",
                "lng" : -86.8663978,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1398997
            }, 
            {
                "search" : "",
                "lng" : -99.5300954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0182414
            }, 
            {
                "search" : "",
                "lng" : -103.3474504,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6707751
            }, 
            {
                "search" : "",
                "lng" : -96.126296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1890273
            }, 
            {
                "search" : "",
                "lng" : -98.7523941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2123609
            }, 
            {
                "search" : "",
                "lng" : -99.1425936,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4938486
            }, 
            {
                "search" : "",
                "lng" : -109.5437033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3247638
            }, 
            {
                "search" : "",
                "lng" : -105.4770363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1951374
            }, 
            {
                "search" : "",
                "lng" : -96.9945776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9011103
            }, 
            {
                "search" : "",
                "lng" : -100.4940446,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7843284
            }, 
            {
                "search" : "",
                "lng" : -98.1365294,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3609915
            }, 
            {
                "search" : "",
                "lng" : -97.4334147,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5324409
            }, 
            {
                "search" : "",
                "lng" : -96.1295794,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1686643
            }, 
            {
                "search" : "",
                "lng" : -96.9208506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5364548
            }, 
            {
                "search" : "",
                "lng" : -92.9500838,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5614037
            }, 
            {
                "search" : "",
                "lng" : -98.3673028,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0794486
            }, 
            {
                "search" : "",
                "lng" : -96.9435078,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8884901
            }, 
            {
                "search" : "",
                "lng" : -99.5155766,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2899107
            }, 
            {
                "search" : "",
                "lng" : -96.9365313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8947325
            }, 
            {
                "search" : "",
                "lng" : -99.1828781,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3929429
            }, 
            {
                "search" : "",
                "lng" : -99.985314,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.399482
            }, 
            {
                "search" : "",
                "lng" : -96.9255121,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8797965
            }, 
            {
                "search" : "",
                "lng" : -105.4705999,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1956387
            }, 
            {
                "search" : "",
                "lng" : -98.2006131,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.047377
            }, 
            {
                "search" : "",
                "lng" : -102.1943485,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9567646
            }, 
            {
                "search" : "",
                "lng" : -106.4239413,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2052801
            }, 
            {
                "search" : "",
                "lng" : -99.1828499,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6697165
            }, 
            {
                "search" : "",
                "lng" : -96.5943562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3317796
            }, 
            {
                "search" : "",
                "lng" : -98.9324616,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8031406
            }, 
            {
                "search" : "",
                "lng" : -89.6195111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9900525
            }, 
            {
                "search" : "",
                "lng" : -99.0703023,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.457212
            }, 
            {
                "search" : "",
                "lng" : -96.9410464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8938017
            }, 
            {
                "search" : "",
                "lng" : -103.4139348,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7187486
            }, 
            {
                "search" : "",
                "lng" : -99.1244668,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5121374
            }, 
            {
                "search" : "",
                "lng" : -99.2127658,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4808759
            }, 
            {
                "search" : "",
                "lng" : -99.5892496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2618401
            }, 
            {
                "search" : "",
                "lng" : -99.0161735,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6845276
            }, 
            {
                "search" : "",
                "lng" : -96.9355901,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5492026
            }, 
            {
                "search" : "",
                "lng" : -102.2630392,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.889625
            }, 
            {
                "search" : "",
                "lng" : -90.5549797,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8282881
            }, 
            {
                "search" : "",
                "lng" : -99.1414643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.399121
            }, 
            {
                "search" : "",
                "lng" : -103.3477463,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6887723
            }, 
            {
                "search" : "",
                "lng" : -103.3462533,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7122168
            }, 
            {
                "search" : "",
                "lng" : -110.8802562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9270358
            }, 
            {
                "search" : "",
                "lng" : -86.8463243,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0842528
            }, 
            {
                "search" : "",
                "lng" : -103.4983303,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5691649
            }, 
            {
                "search" : "",
                "lng" : -99.2187464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9150598
            }, 
            {
                "search" : "",
                "lng" : -108.9309776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.3688543
            }, 
            {
                "search" : "",
                "lng" : -103.442465,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5586405
            }, 
            {
                "search" : "",
                "lng" : -96.9128654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5362446
            }, 
            {
                "search" : "",
                "lng" : -96.9272869,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5437105
            }, 
            {
                "search" : "",
                "lng" : -99.8955916,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8582528
            }, 
            {
                "search" : "",
                "lng" : -98.9989338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7683562
            }, 
            {
                "search" : "",
                "lng" : -98.950392,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.844228
            }, 
            {
                "search" : "",
                "lng" : -99.575349,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2540988
            }, 
            {
                "search" : "",
                "lng" : -99.1970432,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5333311
            }, 
            {
                "search" : "",
                "lng" : -102.2061015,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9673581
            }, 
            {
                "search" : "",
                "lng" : -104.8811043,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4866305
            }, 
            {
                "search" : "",
                "lng" : -103.4295033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6840711
            }, 
            {
                "search" : "",
                "lng" : -103.3861582,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7194444
            }, 
            {
                "search" : "",
                "lng" : -103.3789354,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7145204
            }, 
            {
                "search" : "",
                "lng" : -99.1874911,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4700467
            }, 
            {
                "search" : "",
                "lng" : -95.3024094,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4634757
            }, 
            {
                "search" : "",
                "lng" : -102.2820883,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9881879
            }, 
            {
                "search" : "",
                "lng" : -94.4586566,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3985129
            }, 
            {
                "search" : "",
                "lng" : -101.4265355,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9084376
            }, 
            {
                "search" : "",
                "lng" : -105.4805986,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.2095495
            }, 
            {
                "search" : "",
                "lng" : -102.2597837,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.5741984
            }, 
            {
                "search" : "",
                "lng" : -99.0169534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5993367
            }, 
            {
                "search" : "",
                "lng" : -98.8773767,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5013794
            }, 
            {
                "search" : "",
                "lng" : -101.1450232,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6987136
            }, 
            {
                "search" : "",
                "lng" : -86.82347,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1713186
            }, 
            {
                "search" : "",
                "lng" : -103.4253586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.62117
            }, 
            {
                "search" : "",
                "lng" : -98.1705872,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0777037
            }, 
            {
                "search" : "",
                "lng" : -112.1626425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.7254575
            }, 
            {
                "search" : "",
                "lng" : -102.3536368,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0939968
            }, 
            {
                "search" : "",
                "lng" : -103.7129887,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2596901
            }, 
            {
                "search" : "",
                "lng" : -88.2964778,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.5477801
            }, 
            {
                "search" : "",
                "lng" : -99.1877294,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4138253
            }, 
            {
                "search" : "",
                "lng" : -107.3966484,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7747796
            }, 
            {
                "search" : "",
                "lng" : -99.1561883,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3437444
            }, 
            {
                "search" : "",
                "lng" : -99.1668107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3888992
            }, 
            {
                "search" : "",
                "lng" : -99.1164571,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.400237
            }, 
            {
                "search" : "",
                "lng" : -93.1627391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7631375
            }, 
            {
                "search" : "",
                "lng" : -9.16510470000003,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 38.6765238
            }, 
            {
                "search" : "",
                "lng" : -106.0828158,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6295326
            }, 
            {
                "search" : "",
                "lng" : -99.909025,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8498649
            }, 
            {
                "search" : "",
                "lng" : -97.3890972,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9486521
            }, 
            {
                "search" : "",
                "lng" : -98.9467946,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3134428
            }, 
            {
                "search" : "",
                "lng" : -101.0201242,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4409038
            }, 
            {
                "search" : "",
                "lng" : -98.801705,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6919672
            }, 
            {
                "search" : "",
                "lng" : -103.4018631,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.658252
            }, 
            {
                "search" : "",
                "lng" : -93.1935391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7404338
            }, 
            {
                "search" : "",
                "lng" : -100.9773227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4477102
            }, 
            {
                "search" : "",
                "lng" : -101.7093338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1668637
            }, 
            {
                "search" : "",
                "lng" : -104.8874461,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4868134
            }, 
            {
                "search" : "",
                "lng" : -99.0247075,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5588601
            }, 
            {
                "search" : "",
                "lng" : -93.3939942,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.8431524
            }, 
            {
                "search" : "",
                "lng" : -100.4689314,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6309888
            }, 
            {
                "search" : "",
                "lng" : -106.4375692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2425824
            }, 
            {
                "search" : "",
                "lng" : -116.9719967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5323062
            }, 
            {
                "search" : "",
                "lng" : -99.1900273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5100652
            }, 
            {
                "search" : "",
                "lng" : -99.2056663,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3266005
            }, 
            {
                "search" : "",
                "lng" : -99.6136967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.24943
            }, 
            {
                "search" : "",
                "lng" : -106.4192824,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2625852
            }, 
            {
                "search" : "",
                "lng" : -99.2483995,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3305188
            }, 
            {
                "search" : "",
                "lng" : -99.2493254,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4703443
            }, 
            {
                "search" : "",
                "lng" : -99.2574651,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4009524
            }, 
            {
                "search" : "",
                "lng" : -99.2572694,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4892675
            }, 
            {
                "search" : "",
                "lng" : -87.0740767,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6239801
            }, 
            {
                "search" : "",
                "lng" : -106.4570915,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7322827
            }, 
            {
                "search" : "",
                "lng" : -99.2192734,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5399884
            }, 
            {
                "search" : "",
                "lng" : -103.4428879,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8193378
            }, 
            {
                "search" : "",
                "lng" : -93.2127162,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4006144
            }, 
            {
                "search" : "",
                "lng" : -103.4627874,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6978314
            }, 
            {
                "search" : "",
                "lng" : -103.4667118,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.698284
            }, 
            {
                "search" : "",
                "lng" : -100.3683113,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6138297
            }, 
            {
                "search" : "",
                "lng" : -101.1587262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.686077
            }, 
            {
                "search" : "",
                "lng" : -101.5010099,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0816722
            }, 
            {
                "search" : "",
                "lng" : -103.2996458,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6619758
            }, 
            {
                "search" : "",
                "lng" : -99.2224888,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4930711
            }, 
            {
                "search" : "",
                "lng" : -98.9059808,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6373825
            }, 
            {
                "search" : "",
                "lng" : -98.7082786,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0954249
            }, 
            {
                "search" : "",
                "lng" : -99.2454968,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4599669
            }, 
            {
                "search" : "",
                "lng" : -99.9135199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8327281
            }, 
            {
                "search" : "",
                "lng" : -98.8818916,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5522915
            }, 
            {
                "search" : "",
                "lng" : -105.4434192,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.2388693
            }, 
            {
                "search" : "",
                "lng" : -99.2398307,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9672954
            }, 
            {
                "search" : "",
                "lng" : -99.2356534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9514495
            }, 
            {
                "search" : "",
                "lng" : -102.2082076,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9597382
            }, 
            {
                "search" : "",
                "lng" : -99.6354801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3173092
            }, 
            {
                "search" : "",
                "lng" : -101.0086998,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.140217
            }, 
            {
                "search" : "",
                "lng" : -114.7807737,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4831355
            }, 
            {
                "search" : "",
                "lng" : -106.416035,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2151464
            }, 
            {
                "search" : "",
                "lng" : -107.3715231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8104675
            }, 
            {
                "search" : "",
                "lng" : -99.1947509,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8260173
            }, 
            {
                "search" : "",
                "lng" : -117.0272175,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5208086
            }, 
            {
                "search" : "",
                "lng" : -92.9252206,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9780118
            }, 
            {
                "search" : "",
                "lng" : -99.5960463,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2504964
            }, 
            {
                "search" : "",
                "lng" : -100.2900069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6479219
            }, 
            {
                "search" : "",
                "lng" : -96.9791201,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8919318
            }, 
            {
                "search" : "",
                "lng" : -100.5636175,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6916063
            }, 
            {
                "search" : "",
                "lng" : -101.690244,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1377021
            }, 
            {
                "search" : "",
                "lng" : -102.3666217,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1045865
            }, 
            {
                "search" : "",
                "lng" : -105.2208644,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6418192
            }, 
            {
                "search" : "",
                "lng" : -106.1164409,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6165136
            }, 
            {
                "search" : "",
                "lng" : -102.3143436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8996161
            }, 
            {
                "search" : "",
                "lng" : -99.470753,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.181091
            }, 
            {
                "search" : "",
                "lng" : -104.3035935,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0739803
            }, 
            {
                "search" : "",
                "lng" : -99.0595898,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2802114
            }, 
            {
                "search" : "",
                "lng" : -103.5962546,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8833369
            }, 
            {
                "search" : "",
                "lng" : -102.7668673,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8191965
            }, 
            {
                "search" : "",
                "lng" : -103.4443376,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6590944
            }, 
            {
                "search" : "",
                "lng" : -98.7503319,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1254757
            }, 
            {
                "search" : "",
                "lng" : -99.5076175,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4756868
            }, 
            {
                "search" : "",
                "lng" : -99.5278276,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9542064
            }, 
            {
                "search" : "",
                "lng" : -99.9988006,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9163675
            }, 
            {
                "search" : "",
                "lng" : -99.2208401,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9094045
            }, 
            {
                "search" : "",
                "lng" : -103.7336342,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2776758
            }, 
            {
                "search" : "",
                "lng" : -99.1152286,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4690443
            }, 
            {
                "search" : "",
                "lng" : -100.3653285,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6017499
            }, 
            {
                "search" : "",
                "lng" : -98.2257175,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0702823
            }, 
            {
                "search" : "",
                "lng" : -98.2249246,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0680492
            }, 
            {
                "search" : "",
                "lng" : -102.2811269,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8779361
            }, 
            {
                "search" : "",
                "lng" : -102.6076284,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7729091
            }, 
            {
                "search" : "",
                "lng" : -99.8472576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8283915
            }, 
            {
                "search" : "",
                "lng" : -103.3670363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.676637
            }, 
            {
                "search" : "",
                "lng" : -99.2247662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4608684
            }, 
            {
                "search" : "",
                "lng" : -101.1209037,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2167586
            }, 
            {
                "search" : "",
                "lng" : -98.877708,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6853675
            }, 
            {
                "search" : "",
                "lng" : -99.0809991,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3561588
            }, 
            {
                "search" : "",
                "lng" : -99.0842096,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6320312
            }, 
            {
                "search" : "",
                "lng" : -97.8730812,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2729518
            }, 
            {
                "search" : "",
                "lng" : -103.3984444,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5791414
            }, 
            {
                "search" : "",
                "lng" : -99.6076135,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2651483
            }, 
            {
                "search" : "",
                "lng" : -103.7101352,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2559179
            }, 
            {
                "search" : "",
                "lng" : -104.3579956,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7733497
            }, 
            {
                "search" : "",
                "lng" : -102.2970714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9390465
            }, 
            {
                "search" : "",
                "lng" : -106.0745793,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6352431
            }, 
            {
                "search" : "",
                "lng" : -96.1296416,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0827151
            }, 
            {
                "search" : "",
                "lng" : -104.881882,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4998618
            }, 
            {
                "search" : "",
                "lng" : -115.4023797,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6665207
            }, 
            {
                "search" : "",
                "lng" : -106.1051979,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6235221
            }, 
            {
                "search" : "",
                "lng" : -99.1849925,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.354913
            }, 
            {
                "search" : "",
                "lng" : -99.1753279,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2917476
            }, 
            {
                "search" : "",
                "lng" : -95.8132139,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.8304479
            }, 
            {
                "search" : "",
                "lng" : -98.9392558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2912274
            }, 
            {
                "search" : "",
                "lng" : -107.3603872,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8094629
            }, 
            {
                "search" : "",
                "lng" : -102.5730113,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6776042
            }, 
            {
                "search" : "",
                "lng" : -98.7687141,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1093058
            }, 
            {
                "search" : "",
                "lng" : -99.2887371,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3964691
            }, 
            {
                "search" : "",
                "lng" : -99.293192,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3620327
            }, 
            {
                "search" : "",
                "lng" : -95.0579736,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.5054186
            }, 
            {
                "search" : "",
                "lng" : -98.9590436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8132804
            }, 
            {
                "search" : "",
                "lng" : -90.5317112,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8146662
            }, 
            {
                "search" : "",
                "lng" : -99.2291033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.895176
            }, 
            {
                "search" : "",
                "lng" : -99.2011193,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6568784
            }, 
            {
                "search" : "",
                "lng" : -99.2240022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9686177
            }, 
            {
                "search" : "",
                "lng" : -103.396119,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5279107
            }, 
            {
                "search" : "",
                "lng" : -98.5473453,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7897601
            }, 
            {
                "search" : "",
                "lng" : -103.4275343,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7552765
            }, 
            {
                "search" : "",
                "lng" : -103.4419497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7664359
            }, 
            {
                "search" : "",
                "lng" : -101.6766907,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1008794
            }, 
            {
                "search" : "",
                "lng" : -104.0477548,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1995313
            }, 
            {
                "search" : "",
                "lng" : -96.7207303,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0661498
            }, 
            {
                "search" : "",
                "lng" : -99.220391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.575651
            }, 
            {
                "search" : "",
                "lng" : -103.4520461,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5382877
            }, 
            {
                "search" : "",
                "lng" : -99.1432543,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4342325
            }, 
            {
                "search" : "",
                "lng" : -97.4702643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4868797
            }, 
            {
                "search" : "",
                "lng" : -99.5050327,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.556267
            }, 
            {
                "search" : "",
                "lng" : -99.0909022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6389038
            }, 
            {
                "search" : "",
                "lng" : -97.4103254,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0963729
            }, 
            {
                "search" : "",
                "lng" : -103.3404601,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6751199
            }, 
            {
                "search" : "",
                "lng" : -110.289179,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.9897648
            }, 
            {
                "search" : "",
                "lng" : -99.1626542,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9506768
            }, 
            {
                "search" : "",
                "lng" : -98.3701843,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0772383
            }, 
            {
                "search" : "",
                "lng" : -115.4524405,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6664233
            }, 
            {
                "search" : "",
                "lng" : -99.1670769,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7195604
            }, 
            {
                "search" : "",
                "lng" : -98.3976759,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2164186
            }, 
            {
                "search" : "",
                "lng" : -103.1175503,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4119119
            }, 
            {
                "search" : "",
                "lng" : -102.0385427,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3411726
            }, 
            {
                "search" : "",
                "lng" : -99.1450881,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5164403
            }, 
            {
                "search" : "",
                "lng" : -100.9926662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4517189
            }, 
            {
                "search" : "",
                "lng" : -96.1339231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1878435
            }, 
            {
                "search" : "",
                "lng" : -101.3762953,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6705921
            }, 
            {
                "search" : "",
                "lng" : -99.5566573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4177152
            }, 
            {
                "search" : "",
                "lng" : -99.8984556,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8362809
            }, 
            {
                "search" : "",
                "lng" : -99.2324577,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9008623
            }, 
            {
                "search" : "",
                "lng" : -97.4475759,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5467357
            }, 
            {
                "search" : "",
                "lng" : -98.1313006,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0431159
            }, 
            {
                "search" : "",
                "lng" : -97.8419943,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6635953
            }, 
            {
                "search" : "",
                "lng" : -102.0470441,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4140039
            }, 
            {
                "search" : "",
                "lng" : -105.2526867,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.748196
            }, 
            {
                "search" : "",
                "lng" : -99.3170436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0500286
            }, 
            {
                "search" : "",
                "lng" : -98.211141,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0758776
            }, 
            {
                "search" : "",
                "lng" : -103.348517,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.606435
            }, 
            {
                "search" : "",
                "lng" : -99.2739674,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3843849
            }, 
            {
                "search" : "",
                "lng" : -99.2711803,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3778524
            }, 
            {
                "search" : "",
                "lng" : -99.265537,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5147225
            }, 
            {
                "search" : "",
                "lng" : -90.5477665,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8330726
            }, 
            {
                "search" : "",
                "lng" : -103.4003975,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6542859
            }, 
            {
                "search" : "",
                "lng" : -100.5375039,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.7078898
            }, 
            {
                "search" : "",
                "lng" : -103.4074569,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6840686
            }, 
            {
                "search" : "",
                "lng" : -99.3492732,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9129979
            }, 
            {
                "search" : "",
                "lng" : -99.1707691,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4484946
            }, 
            {
                "search" : "",
                "lng" : -102.2948222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8598325
            }, 
            {
                "search" : "",
                "lng" : -99.1286722,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4969063
            }, 
            {
                "search" : "",
                "lng" : -98.9529864,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8027878
            }, 
            {
                "search" : "",
                "lng" : -96.9297733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.529468
            }, 
            {
                "search" : "",
                "lng" : -103.3666709,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6955884
            }, 
            {
                "search" : "",
                "lng" : -99.2217534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5300458
            }, 
            {
                "search" : "",
                "lng" : -103.4074073,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6340162
            }, 
            {
                "search" : "",
                "lng" : -96.9594511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.451938
            }, 
            {
                "search" : "",
                "lng" : -103.4379044,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.540064
            }, 
            {
                "search" : "",
                "lng" : -86.836408,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1513643
            }, 
            {
                "search" : "",
                "lng" : -97.0499235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0656048
            }, 
            {
                "search" : "",
                "lng" : -102.2201055,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9627541
            }, 
            {
                "search" : "",
                "lng" : -99.3408423,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9058191
            }, 
            {
                "search" : "",
                "lng" : -99.2679926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4860774
            }, 
            {
                "search" : "",
                "lng" : -102.5328299,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.758792
            }, 
            {
                "search" : "",
                "lng" : -107.9205937,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.481348
            }, 
            {
                "search" : "",
                "lng" : -102.5463567,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7640214
            }, 
            {
                "search" : "",
                "lng" : -99.1205082,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2646027
            }, 
            {
                "search" : "",
                "lng" : -99.1129149,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2643164
            }, 
            {
                "search" : "",
                "lng" : -92.9482945,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9704455
            }, 
            {
                "search" : "",
                "lng" : -99.9047381,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8677707
            }, 
            {
                "search" : "",
                "lng" : -108.4719616,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5523758
            }, 
            {
                "search" : "",
                "lng" : -90.5271374,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8517673
            }, 
            {
                "search" : "",
                "lng" : -99.2171174,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4831412
            }, 
            {
                "search" : "",
                "lng" : -99.6720374,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2889709
            }, 
            {
                "search" : "",
                "lng" : -92.2742664,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.1056503
            }, 
            {
                "search" : "",
                "lng" : -103.4249771,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6501096
            }, 
            {
                "search" : "",
                "lng" : -99.6023839,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2705034
            }, 
            {
                "search" : "",
                "lng" : -107.3866575,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7836543
            }, 
            {
                "search" : "",
                "lng" : -91.7669623,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.7361878
            }, 
            {
                "search" : "",
                "lng" : -103.4070741,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5393031
            }, 
            {
                "search" : "",
                "lng" : -100.463372,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.667599
            }, 
            {
                "search" : "",
                "lng" : -99.1090642,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4081409
            }, 
            {
                "search" : "",
                "lng" : -99.0807232,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6444058
            }, 
            {
                "search" : "",
                "lng" : -99.0416466,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5976375
            }, 
            {
                "search" : "",
                "lng" : -99.8832876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8044862
            }, 
            {
                "search" : "",
                "lng" : -99.161411,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6360984
            }, 
            {
                "search" : "",
                "lng" : -98.7930535,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2654492
            }, 
            {
                "search" : "",
                "lng" : -99.3368219,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0330588
            }, 
            {
                "search" : "",
                "lng" : -99.2207155,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6736399
            }, 
            {
                "search" : "",
                "lng" : -99.0417299,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3938456
            }, 
            {
                "search" : "",
                "lng" : -99.0335559,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4093275
            }, 
            {
                "search" : "",
                "lng" : -99.5829974,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3038122
            }, 
            {
                "search" : "",
                "lng" : -99.2842972,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5991668
            }, 
            {
                "search" : "",
                "lng" : -99.2251838,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6698969
            }, 
            {
                "search" : "",
                "lng" : -99.3824614,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7366982
            }, 
            {
                "search" : "",
                "lng" : -106.1451636,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6903126
            }, 
            {
                "search" : "",
                "lng" : -103.4447421,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5427439
            }, 
            {
                "search" : "",
                "lng" : -96.9300258,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5425533
            }, 
            {
                "search" : "",
                "lng" : -105.6597338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9281451
            }, 
            {
                "search" : "",
                "lng" : -106.4958071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7387978
            }, 
            {
                "search" : "",
                "lng" : -101.0219237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1239384
            }, 
            {
                "search" : "",
                "lng" : -101.6920858,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1424142
            }, 
            {
                "search" : "",
                "lng" : -99.2241565,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4566698
            }, 
            {
                "search" : "",
                "lng" : -99.2922296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5703819
            }, 
            {
                "search" : "",
                "lng" : -104.3115909,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1076591
            }, 
            {
                "search" : "",
                "lng" : -99.2034608,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6783228
            }, 
            {
                "search" : "",
                "lng" : -99.203083,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6393483
            }, 
            {
                "search" : "",
                "lng" : -103.4154613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6914945
            }, 
            {
                "search" : "",
                "lng" : -103.4149209,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6944832
            }, 
            {
                "search" : "",
                "lng" : -103.3856248,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7102229
            }, 
            {
                "search" : "",
                "lng" : -103.3667006,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5784423
            }, 
            {
                "search" : "",
                "lng" : -97.5088247,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8561193
            }, 
            {
                "search" : "",
                "lng" : -101.0028698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1831751
            }, 
            {
                "search" : "",
                "lng" : -105.206344,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6397184
            }, 
            {
                "search" : "",
                "lng" : -99.0933478,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6806593
            }, 
            {
                "search" : "",
                "lng" : -102.5557599,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7685459
            }, 
            {
                "search" : "",
                "lng" : -93.0853586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7544991
            }, 
            {
                "search" : "",
                "lng" : -100.745235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9144491
            }, 
            {
                "search" : "",
                "lng" : -99.2037508,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8277305
            }, 
            {
                "search" : "",
                "lng" : -96.9237751,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8838909
            }, 
            {
                "search" : "",
                "lng" : -97.459183,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.544685
            }, 
            {
                "search" : "",
                "lng" : -99.9113375,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8733958
            }, 
            {
                "search" : "",
                "lng" : -99.0264165,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5559617
            }, 
            {
                "search" : "",
                "lng" : -99.132336,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6308465
            }, 
            {
                "search" : "",
                "lng" : -101.0391173,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1384352
            }, 
            {
                "search" : "",
                "lng" : -101.0785944,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8564935
            }, 
            {
                "search" : "",
                "lng" : -105.2417653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6954936
            }, 
            {
                "search" : "",
                "lng" : -99.1842162,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4048092
            }, 
            {
                "search" : "",
                "lng" : -90.5189258,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.84064
            }, 
            {
                "search" : "",
                "lng" : -99.1855873,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3922009
            }, 
            {
                "search" : "",
                "lng" : -99.0496797,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.606044
            }, 
            {
                "search" : "",
                "lng" : -99.2719775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1846619
            }, 
            {
                "search" : "",
                "lng" : -102.3147821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.847463
            }, 
            {
                "search" : "",
                "lng" : -100.9480078,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1498193
            }, 
            {
                "search" : "",
                "lng" : -99.1846333,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3711279
            }, 
            {
                "search" : "",
                "lng" : -99.1746379,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3618153
            }, 
            {
                "search" : "",
                "lng" : -99.2215265,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5655261
            }, 
            {
                "search" : "",
                "lng" : -98.0368647,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.828001
            }, 
            {
                "search" : "",
                "lng" : -99.8979243,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.874533
            }, 
            {
                "search" : "",
                "lng" : -100.4378487,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.658311
            }, 
            {
                "search" : "",
                "lng" : -96.1338144,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1698219
            }, 
            {
                "search" : "",
                "lng" : -99.2370852,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3236969
            }, 
            {
                "search" : "",
                "lng" : -99.2105154,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.942589
            }, 
            {
                "search" : "",
                "lng" : -99.2302821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3154363
            }, 
            {
                "search" : "",
                "lng" : -98.2013891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0113868
            }, 
            {
                "search" : "",
                "lng" : -99.1008784,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4798941
            }, 
            {
                "search" : "",
                "lng" : -99.12059,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3337779
            }, 
            {
                "search" : "",
                "lng" : -100.2082549,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6563631
            }, 
            {
                "search" : "",
                "lng" : -101.6626001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1118104
            }, 
            {
                "search" : "",
                "lng" : -99.2141125,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3709773
            }, 
            {
                "search" : "",
                "lng" : -106.3496563,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6016657
            }, 
            {
                "search" : "",
                "lng" : -99.2299277,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5181791
            }, 
            {
                "search" : "",
                "lng" : -110.9036847,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9236287
            }, 
            {
                "search" : "",
                "lng" : -99.228679,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1301776
            }, 
            {
                "search" : "",
                "lng" : -111.0132959,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.135544
            }, 
            {
                "search" : "",
                "lng" : -98.0881166,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.9872338
            }, 
            {
                "search" : "",
                "lng" : -99.2496919,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3620013
            }, 
            {
                "search" : "",
                "lng" : -100.4044897,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6349424
            }, 
            {
                "search" : "",
                "lng" : -106.426688,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.688066
            }, 
            {
                "search" : "",
                "lng" : -110.9471069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3034472
            }, 
            {
                "search" : "",
                "lng" : -100.8200317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5375337
            }, 
            {
                "search" : "",
                "lng" : -98.1523934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.286355
            }, 
            {
                "search" : "",
                "lng" : -98.2289067,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.053781
            }, 
            {
                "search" : "",
                "lng" : -98.8896885,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2669146
            }, 
            {
                "search" : "",
                "lng" : -99.0783711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3182621
            }, 
            {
                "search" : "",
                "lng" : -99.1897795,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5352929
            }, 
            {
                "search" : "",
                "lng" : -99.2225306,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3388262
            }, 
            {
                "search" : "",
                "lng" : -99.1396544,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2893428
            }, 
            {
                "search" : "",
                "lng" : -98.2250575,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0796974
            }, 
            {
                "search" : "",
                "lng" : -102.209101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9716609
            }, 
            {
                "search" : "",
                "lng" : -105.2197156,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6776864
            }, 
            {
                "search" : "",
                "lng" : -106.4612009,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7565357
            }, 
            {
                "search" : "",
                "lng" : -99.181389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3424949
            }, 
            {
                "search" : "",
                "lng" : -99.183114,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6161391
            }, 
            {
                "search" : "",
                "lng" : -99.1820863,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3405588
            }, 
            {
                "search" : "",
                "lng" : -106.4466475,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.758715
            }, 
            {
                "search" : "",
                "lng" : -99.9454817,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3874494
            }, 
            {
                "search" : "",
                "lng" : -101.1804977,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7609661
            }, 
            {
                "search" : "",
                "lng" : -97.8600838,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2660622
            }, 
            {
                "search" : "",
                "lng" : -103.6981876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.246878
            }, 
            {
                "search" : "",
                "lng" : -98.0263736,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1868741
            }, 
            {
                "search" : "",
                "lng" : -97.9555202,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2586561
            }, 
            {
                "search" : "",
                "lng" : -106.4266402,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2298348
            }, 
            {
                "search" : "",
                "lng" : -103.3868454,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5074729
            }, 
            {
                "search" : "",
                "lng" : -100.8381551,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5027801
            }, 
            {
                "search" : "",
                "lng" : -97.4943028,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8637731
            }, 
            {
                "search" : "",
                "lng" : -94.4666488,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1404081
            }, 
            {
                "search" : "",
                "lng" : -94.4691851,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.140874
            }, 
            {
                "search" : "",
                "lng" : -103.4358344,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6830551
            }, 
            {
                "search" : "",
                "lng" : -106.101135,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.672739
            }, 
            {
                "search" : "",
                "lng" : -99.2532014,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5391392
            }, 
            {
                "search" : "",
                "lng" : -100.3310103,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6827741
            }, 
            {
                "search" : "",
                "lng" : -107.3966266,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.742547
            }, 
            {
                "search" : "",
                "lng" : -99.0490829,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5579877
            }, 
            {
                "search" : "",
                "lng" : -99.3059736,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5515395
            }, 
            {
                "search" : "",
                "lng" : -99.107086,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.9743096
            }, 
            {
                "search" : "",
                "lng" : -99.2295447,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9620293
            }, 
            {
                "search" : "",
                "lng" : -99.5413435,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9530644
            }, 
            {
                "search" : "",
                "lng" : -104.688576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0219686
            }, 
            {
                "search" : "",
                "lng" : -98.1284549,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0606998
            }, 
            {
                "search" : "",
                "lng" : -102.7781821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8210527
            }, 
            {
                "search" : "",
                "lng" : -103.4435247,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5531775
            }, 
            {
                "search" : "",
                "lng" : -99.1596774,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7503783
            }, 
            {
                "search" : "",
                "lng" : -104.8855023,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.512212
            }, 
            {
                "search" : "",
                "lng" : -99.0877118,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3871577
            }, 
            {
                "search" : "",
                "lng" : -93.1227621,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7546748
            }, 
            {
                "search" : "",
                "lng" : -100.3854025,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5576076
            }, 
            {
                "search" : "",
                "lng" : -99.1029664,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2572314
            }, 
            {
                "search" : "",
                "lng" : -99.191394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.551937
            }, 
            {
                "search" : "",
                "lng" : -98.7582493,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7240495
            }, 
            {
                "search" : "",
                "lng" : -101.5631576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.6423921
            }, 
            {
                "search" : "",
                "lng" : -95.1129658,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4212621
            }, 
            {
                "search" : "",
                "lng" : -92.1877922,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.036302
            }, 
            {
                "search" : "",
                "lng" : -99.7456561,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.251356
            }, 
            {
                "search" : "",
                "lng" : -99.1658433,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4061649
            }, 
            {
                "search" : "",
                "lng" : -102.3299191,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2271632
            }, 
            {
                "search" : "",
                "lng" : -103.4037343,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6919697
            }, 
            {
                "search" : "",
                "lng" : -99.1638071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4035455
            }, 
            {
                "search" : "",
                "lng" : -98.150072,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4165874
            }, 
            {
                "search" : "",
                "lng" : -99.2272939,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9960567
            }, 
            {
                "search" : "",
                "lng" : -97.3590833,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8329388
            }, 
            {
                "search" : "",
                "lng" : -97.6890668,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.2683238
            }, 
            {
                "search" : "",
                "lng" : -100.3374991,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6474874
            }, 
            {
                "search" : "",
                "lng" : -100.889099,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2078497
            }, 
            {
                "search" : "",
                "lng" : -99.2196342,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5357284
            }, 
            {
                "search" : "",
                "lng" : -99.0222408,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.0100693
            }, 
            {
                "search" : "",
                "lng" : -99.1342735,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4377876
            }, 
            {
                "search" : "",
                "lng" : -98.734077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.121094
            }, 
            {
                "search" : "",
                "lng" : -100.3443038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6723479
            }, 
            {
                "search" : "",
                "lng" : -107.4129114,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.2497759
            }, 
            {
                "search" : "",
                "lng" : -99.0253869,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3954077
            }, 
            {
                "search" : "",
                "lng" : -94.4388374,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1500934
            }, 
            {
                "search" : "",
                "lng" : -100.3600908,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4376678
            }, 
            {
                "search" : "",
                "lng" : -100.9868383,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1475363
            }, 
            {
                "search" : "",
                "lng" : -101.2532665,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.699862
            }, 
            {
                "search" : "",
                "lng" : -100.1467214,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6599334
            }, 
            {
                "search" : "",
                "lng" : -98.4062404,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.6849556
            }, 
            {
                "search" : "",
                "lng" : -98.2289028,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0253492
            }, 
            {
                "search" : "",
                "lng" : -96.9232166,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5291379
            }, 
            {
                "search" : "",
                "lng" : -99.6717256,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8031091
            }, 
            {
                "search" : "",
                "lng" : -107.9184745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4825548
            }, 
            {
                "search" : "",
                "lng" : -99.6552069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2965541
            }, 
            {
                "search" : "",
                "lng" : -98.8866854,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5013161
            }, 
            {
                "search" : "",
                "lng" : -93.2734517,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7978347
            }, 
            {
                "search" : "",
                "lng" : -92.1377962,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.2257754
            }, 
            {
                "search" : "",
                "lng" : -101.105332,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.727685
            }, 
            {
                "search" : "",
                "lng" : -116.8955269,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5124815
            }, 
            {
                "search" : "",
                "lng" : -100.7626844,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.4292706
            }, 
            {
                "search" : "",
                "lng" : -99.1716951,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5902918
            }, 
            {
                "search" : "",
                "lng" : -99.2321889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5409729
            }, 
            {
                "search" : "",
                "lng" : -116.9964281,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5074828
            }, 
            {
                "search" : "",
                "lng" : -101.3621458,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7019361
            }, 
            {
                "search" : "",
                "lng" : -99.2544286,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5234734
            }, 
            {
                "search" : "",
                "lng" : -101.7355598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1381483
            }, 
            {
                "search" : "",
                "lng" : -117.0140436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4907516
            }, 
            {
                "search" : "",
                "lng" : -109.8994041,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5221607
            }, 
            {
                "search" : "",
                "lng" : -107.3850725,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8372662
            }, 
            {
                "search" : "",
                "lng" : -115.194273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.39524
            }, 
            {
                "search" : "",
                "lng" : -93.1715491,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.759245
            }, 
            {
                "search" : "",
                "lng" : -110.922861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.098471
            }, 
            {
                "search" : "",
                "lng" : -116.9527939,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4966265
            }, 
            {
                "search" : "",
                "lng" : -116.9611506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5020477
            }, 
            {
                "search" : "",
                "lng" : -104.6408926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0333863
            }, 
            {
                "search" : "",
                "lng" : -116.9264418,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5375749
            }, 
            {
                "search" : "",
                "lng" : -102.532395,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7562209
            }, 
            {
                "search" : "",
                "lng" : -101.7608342,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1721046
            }, 
            {
                "search" : "",
                "lng" : -110.958301,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.14461
            }, 
            {
                "search" : "",
                "lng" : -108.978979,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7976471
            }, 
            {
                "search" : "",
                "lng" : -110.9423338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0809291
            }, 
            {
                "search" : "",
                "lng" : -104.2902573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0824964
            }, 
            {
                "search" : "",
                "lng" : -116.945806,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5020172
            }, 
            {
                "search" : "",
                "lng" : -97.9089497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3821251
            }, 
            {
                "search" : "",
                "lng" : -101.6354221,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1478418
            }, 
            {
                "search" : "",
                "lng" : -106.3964414,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7174905
            }, 
            {
                "search" : "",
                "lng" : -98.7522588,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0916261
            }, 
            {
                "search" : "",
                "lng" : -109.0263929,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.786378
            }, 
            {
                "search" : "",
                "lng" : -99.1784195,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.667147
            }, 
            {
                "search" : "",
                "lng" : -100.3028631,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6349913
            }, 
            {
                "search" : "",
                "lng" : -99.2071921,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3990486
            }, 
            {
                "search" : "",
                "lng" : -99.138773,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.303582
            }, 
            {
                "search" : "",
                "lng" : -99.2578787,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3910907
            }, 
            {
                "search" : "",
                "lng" : -99.252586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.415266
            }, 
            {
                "search" : "",
                "lng" : -99.2385304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6177238
            }, 
            {
                "search" : "",
                "lng" : -98.1352213,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.060456
            }, 
            {
                "search" : "",
                "lng" : -102.303258,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9249794
            }, 
            {
                "search" : "",
                "lng" : -102.2754233,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8982817
            }, 
            {
                "search" : "",
                "lng" : -109.000449,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8070612
            }, 
            {
                "search" : "",
                "lng" : -101.6311104,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1209181
            }, 
            {
                "search" : "",
                "lng" : -98.2004253,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0183774
            }, 
            {
                "search" : "",
                "lng" : -98.1970146,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0084785
            }, 
            {
                "search" : "",
                "lng" : -100.8038395,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5198363
            }, 
            {
                "search" : "",
                "lng" : -101.2149161,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9371829
            }, 
            {
                "search" : "",
                "lng" : -98.4030889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1486456
            }, 
            {
                "search" : "",
                "lng" : -97.45837,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5345273
            }, 
            {
                "search" : "",
                "lng" : -101.1270594,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7102009
            }, 
            {
                "search" : "",
                "lng" : -97.9337733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3954054
            }, 
            {
                "search" : "",
                "lng" : -100.9619032,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1051897
            }, 
            {
                "search" : "",
                "lng" : -113.5428799,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.303546
            }, 
            {
                "search" : "",
                "lng" : -99.2316765,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9099103
            }, 
            {
                "search" : "",
                "lng" : -101.4376307,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.920294
            }, 
            {
                "search" : "",
                "lng" : -100.3185678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.807458
            }, 
            {
                "search" : "",
                "lng" : -115.445999,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6308408
            }, 
            {
                "search" : "",
                "lng" : -100.3694032,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5932073
            }, 
            {
                "search" : "",
                "lng" : -103.4792931,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5656714
            }, 
            {
                "search" : "",
                "lng" : -100.9028148,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.303812
            }, 
            {
                "search" : "",
                "lng" : -101.7130643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1510667
            }, 
            {
                "search" : "",
                "lng" : -103.4370068,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5357561
            }, 
            {
                "search" : "",
                "lng" : -99.2372582,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5386411
            }, 
            {
                "search" : "",
                "lng" : -101.719198,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1516572
            }, 
            {
                "search" : "",
                "lng" : -98.1998889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9861986
            }, 
            {
                "search" : "",
                "lng" : -98.1471738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0526949
            }, 
            {
                "search" : "",
                "lng" : -98.2425541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0305972
            }, 
            {
                "search" : "",
                "lng" : -102.3223322,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2583144
            }, 
            {
                "search" : "",
                "lng" : -97.8951109,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3245674
            }, 
            {
                "search" : "",
                "lng" : -107.3759609,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.820745
            }, 
            {
                "search" : "",
                "lng" : -103.3979027,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7349292
            }, 
            {
                "search" : "",
                "lng" : -100.9682792,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4589122
            }, 
            {
                "search" : "",
                "lng" : -93.0929245,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7666938
            }, 
            {
                "search" : "",
                "lng" : -98.2900967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0752575
            }, 
            {
                "search" : "",
                "lng" : -106.9673073,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.9649767
            }, 
            {
                "search" : "",
                "lng" : -101.4200675,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9250144
            }, 
            {
                "search" : "",
                "lng" : -101.6792606,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1008821
            }, 
            {
                "search" : "",
                "lng" : -98.3361883,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0682335
            }, 
            {
                "search" : "",
                "lng" : -99.254866,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5928654
            }, 
            {
                "search" : "",
                "lng" : -103.4241635,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5680521
            }, 
            {
                "search" : "",
                "lng" : -99.641541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2964928
            }, 
            {
                "search" : "",
                "lng" : -101.1718793,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6742259
            }, 
            {
                "search" : "",
                "lng" : -98.882655,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.503633
            }, 
            {
                "search" : "",
                "lng" : -99.9844449,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5701915
            }, 
            {
                "search" : "",
                "lng" : -101.6350805,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1416907
            }, 
            {
                "search" : "",
                "lng" : -102.3426124,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8569174
            }, 
            {
                "search" : "",
                "lng" : -110.936451,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1074187
            }, 
            {
                "search" : "",
                "lng" : -93.0873671,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.744808
            }, 
            {
                "search" : "",
                "lng" : -101.183197,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5930236
            }, 
            {
                "search" : "",
                "lng" : -101.3605197,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.681112
            }, 
            {
                "search" : "",
                "lng" : -102.511889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7565759
            }, 
            {
                "search" : "",
                "lng" : -94.3943813,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0644271
            }, 
            {
                "search" : "",
                "lng" : -98.2814839,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0854292
            }, 
            {
                "search" : "",
                "lng" : -86.8322791,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.126163
            }, 
            {
                "search" : "",
                "lng" : -86.8458534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0668916
            }, 
            {
                "search" : "",
                "lng" : -98.7717585,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.108077
            }, 
            {
                "search" : "",
                "lng" : -98.7741911,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1099993
            }, 
            {
                "search" : "",
                "lng" : -98.7577514,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0953512
            }, 
            {
                "search" : "",
                "lng" : -110.9367091,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9229619
            }, 
            {
                "search" : "",
                "lng" : -99.2760105,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3999042
            }, 
            {
                "search" : "",
                "lng" : -107.4290523,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8031951
            }, 
            {
                "search" : "",
                "lng" : -103.5119311,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5506808
            }, 
            {
                "search" : "",
                "lng" : -98.2162759,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.99521
            }, 
            {
                "search" : "",
                "lng" : -98.7762796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0578862
            }, 
            {
                "search" : "",
                "lng" : -107.4213931,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7982711
            }, 
            {
                "search" : "",
                "lng" : -99.5155737,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4486496
            }, 
            {
                "search" : "",
                "lng" : -103.4050868,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5033226
            }, 
            {
                "search" : "",
                "lng" : -99.226434,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5488455
            }, 
            {
                "search" : "",
                "lng" : -102.7192577,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.994231
            }, 
            {
                "search" : "",
                "lng" : -98.9668417,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7323846
            }, 
            {
                "search" : "",
                "lng" : -101.417349,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9504633
            }, 
            {
                "search" : "",
                "lng" : -97.6750256,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9103291
            }, 
            {
                "search" : "",
                "lng" : -103.4498673,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5359385
            }, 
            {
                "search" : "",
                "lng" : -103.3513794,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5489344
            }, 
            {
                "search" : "",
                "lng" : -103.4498914,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5727366
            }, 
            {
                "search" : "",
                "lng" : -101.4319975,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9284821
            }, 
            {
                "search" : "",
                "lng" : -98.8825021,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3037999
            }, 
            {
                "search" : "",
                "lng" : -98.2191875,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.07148
            }, 
            {
                "search" : "",
                "lng" : -104.8970851,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4965541
            }, 
            {
                "search" : "",
                "lng" : -106.403414,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7236402
            }, 
            {
                "search" : "",
                "lng" : -110.9091706,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9278853
            }, 
            {
                "search" : "",
                "lng" : -99.5986712,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4632331
            }, 
            {
                "search" : "",
                "lng" : -103.4409113,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5816646
            }, 
            {
                "search" : "",
                "lng" : -98.2018963,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9920283
            }, 
            {
                "search" : "",
                "lng" : -100.980074,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4675929
            }, 
            {
                "search" : "",
                "lng" : -100.9923861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4408249
            }, 
            {
                "search" : "",
                "lng" : -100.4527148,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7082228
            }, 
            {
                "search" : "",
                "lng" : -100.1253427,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8069827
            }, 
            {
                "search" : "",
                "lng" : -97.8462841,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2550595
            }, 
            {
                "search" : "",
                "lng" : -106.1074512,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6458489
            }, 
            {
                "search" : "",
                "lng" : -103.383336,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.70628
            }, 
            {
                "search" : "",
                "lng" : -100.9514187,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.3232963
            }, 
            {
                "search" : "",
                "lng" : -99.0930528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8039297
            }, 
            {
                "search" : "",
                "lng" : -105.7221341,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.3427743
            }, 
            {
                "search" : "",
                "lng" : -98.6609785,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6090963
            }, 
            {
                "search" : "",
                "lng" : -98.3103184,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0852044
            }, 
            {
                "search" : "",
                "lng" : -105.8554774,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.9922507
            }, 
            {
                "search" : "",
                "lng" : -100.8171082,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8692587
            }, 
            {
                "search" : "",
                "lng" : -99.166728,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.753497
            }, 
            {
                "search" : "",
                "lng" : -105.1702584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.6922029
            }, 
            {
                "search" : "",
                "lng" : -99.1130675,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2983829
            }, 
            {
                "search" : "",
                "lng" : -99.128101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4864543
            }, 
            {
                "search" : "",
                "lng" : -92.6394706,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7385316
            }, 
            {
                "search" : "",
                "lng" : -96.9592787,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9087945
            }, 
            {
                "search" : "",
                "lng" : -102.0430476,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4064492
            }, 
            {
                "search" : "",
                "lng" : -89.6208131,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0178153
            }, 
            {
                "search" : "",
                "lng" : -87.0752738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6298421
            }, 
            {
                "search" : "",
                "lng" : -117.028466,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5243546
            }, 
            {
                "search" : "",
                "lng" : -100.2530676,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6738848
            }, 
            {
                "search" : "",
                "lng" : -106.0734107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6276459
            }, 
            {
                "search" : "",
                "lng" : -86.9369373,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5101179
            }, 
            {
                "search" : "",
                "lng" : -88.2022488,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.68964
            }, 
            {
                "search" : "",
                "lng" : -98.3199904,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0655355
            }, 
            {
                "search" : "",
                "lng" : -99.1818822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3892601
            }, 
            {
                "search" : "",
                "lng" : -106.0743052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6194666
            }, 
            {
                "search" : "",
                "lng" : -96.9430648,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8963116
            }, 
            {
                "search" : "",
                "lng" : -89.587924,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0173465
            }, 
            {
                "search" : "",
                "lng" : -97.9601111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2706802
            }, 
            {
                "search" : "",
                "lng" : -91.8199863,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6420691
            }, 
            {
                "search" : "",
                "lng" : -89.5709913,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.016738
            }, 
            {
                "search" : "",
                "lng" : -96.9348355,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8959836
            }, 
            {
                "search" : "",
                "lng" : -109.5375848,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3322894
            }, 
            {
                "search" : "",
                "lng" : -94.5452355,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9961146
            }, 
            {
                "search" : "",
                "lng" : -91.4325968,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.4669936
            }, 
            {
                "search" : "",
                "lng" : -100.9834272,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4887225
            }, 
            {
                "search" : "",
                "lng" : -90.7164421,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3534908
            }, 
            {
                "search" : "",
                "lng" : -106.0765518,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6270728
            }, 
            {
                "search" : "",
                "lng" : -89.6140848,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0028461
            }, 
            {
                "search" : "",
                "lng" : -109.5424861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3150869
            }, 
            {
                "search" : "",
                "lng" : -89.6587796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0108802
            }, 
            {
                "search" : "",
                "lng" : -99.1556511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7288114
            }, 
            {
                "search" : "",
                "lng" : -87.0720503,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6335335
            }, 
            {
                "search" : "",
                "lng" : -89.5953541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.993654
            }, 
            {
                "search" : "",
                "lng" : -99.0405678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4193108
            }, 
            {
                "search" : "",
                "lng" : -89.6274718,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2908318
            }, 
            {
                "search" : "",
                "lng" : -88.2073622,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6992127
            }, 
            {
                "search" : "",
                "lng" : -99.2437231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4729906
            }, 
            {
                "search" : "",
                "lng" : -90.1402339,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1812782
            }, 
            {
                "search" : "",
                "lng" : -91.822557,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6555735
            }, 
            {
                "search" : "",
                "lng" : -89.6112433,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0250846
            }, 
            {
                "search" : "",
                "lng" : -87.0648145,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6367236
            }, 
            {
                "search" : "",
                "lng" : -98.1856584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0045476
            }, 
            {
                "search" : "",
                "lng" : -89.625424,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9784263
            }, 
            {
                "search" : "",
                "lng" : -89.6151619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9439985
            }, 
            {
                "search" : "",
                "lng" : -88.1455902,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1565151
            }, 
            {
                "search" : "",
                "lng" : -109.9298641,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5117005
            }, 
            {
                "search" : "",
                "lng" : -99.6808305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2611052
            }, 
            {
                "search" : "",
                "lng" : -94.5468163,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9999275
            }, 
            {
                "search" : "",
                "lng" : -97.5545143,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7693245
            }, 
            {
                "search" : "",
                "lng" : -89.6008006,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.94132
            }, 
            {
                "search" : "",
                "lng" : -109.5489617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.3277755
            }, 
            {
                "search" : "",
                "lng" : -89.6211409,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9157374
            }, 
            {
                "search" : "",
                "lng" : -91.8314882,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6527604
            }, 
            {
                "search" : "",
                "lng" : -91.8340839,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6530276
            }, 
            {
                "search" : "",
                "lng" : -89.6183776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9800736
            }, 
            {
                "search" : "",
                "lng" : -89.6197924,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9873267
            }, 
            {
                "search" : "",
                "lng" : -89.6148394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9674454
            }, 
            {
                "search" : "",
                "lng" : -111.0766884,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0842037
            }, 
            {
                "search" : "",
                "lng" : -99.193547,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5945894
            }, 
            {
                "search" : "",
                "lng" : -97.860129,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2636933
            }, 
            {
                "search" : "",
                "lng" : -89.623128,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.96965
            }, 
            {
                "search" : "",
                "lng" : -90.5399391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8411638
            }, 
            {
                "search" : "",
                "lng" : -89.6307371,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9691939
            }, 
            {
                "search" : "",
                "lng" : -89.6338209,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9612618
            }, 
            {
                "search" : "",
                "lng" : -89.6319318,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0534685
            }, 
            {
                "search" : "",
                "lng" : -86.8584202,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1645569
            }, 
            {
                "search" : "",
                "lng" : -104.4026633,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8097706
            }, 
            {
                "search" : "",
                "lng" : -102.0630878,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.410695
            }, 
            {
                "search" : "",
                "lng" : -112.8487425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8630857
            }, 
            {
                "search" : "",
                "lng" : -101.6174925,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0875909
            }, 
            {
                "search" : "",
                "lng" : -99.6654598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.277053
            }, 
            {
                "search" : "",
                "lng" : -97.968109,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9314942
            }, 
            {
                "search" : "",
                "lng" : -98.2225318,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3442343
            }, 
            {
                "search" : "",
                "lng" : -102.2603746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9117381
            }, 
            {
                "search" : "",
                "lng" : -107.0797314,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.8184645
            }, 
            {
                "search" : "",
                "lng" : -99.5751545,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2571625
            }, 
            {
                "search" : "",
                "lng" : -99.0274305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5345916
            }, 
            {
                "search" : "",
                "lng" : -101.6949587,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1489065
            }, 
            {
                "search" : "",
                "lng" : -98.3007749,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0057633
            }, 
            {
                "search" : "",
                "lng" : -110.9422033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9323977
            }, 
            {
                "search" : "",
                "lng" : -93.2216864,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.261156
            }, 
            {
                "search" : "",
                "lng" : 53.688046,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.427908
            }, 
            {
                "search" : "",
                "lng" : -99.0501327,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4663818
            }, 
            {
                "search" : "",
                "lng" : -111.0227976,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0859917
            }, 
            {
                "search" : "",
                "lng" : -100.3940095,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6773984
            }, 
            {
                "search" : "",
                "lng" : -101.6654325,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1785148
            }, 
            {
                "search" : "",
                "lng" : -100.7293015,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8076518
            }, 
            {
                "search" : "",
                "lng" : -98.7730506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1116954
            }, 
            {
                "search" : "",
                "lng" : -93.2740241,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.231139
            }, 
            {
                "search" : "",
                "lng" : -109.0022854,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8117026
            }, 
            {
                "search" : "",
                "lng" : -99.2231324,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9360754
            }, 
            {
                "search" : "",
                "lng" : -101.421776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.88838
            }, 
            {
                "search" : "",
                "lng" : -104.6298779,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0485032
            }, 
            {
                "search" : "",
                "lng" : -101.4274582,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.920951
            }, 
            {
                "search" : "",
                "lng" : -97.5076738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8038636
            }, 
            {
                "search" : "",
                "lng" : -101.1657394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6998575
            }, 
            {
                "search" : "",
                "lng" : -98.8217586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.703746
            }, 
            {
                "search" : "",
                "lng" : -99.2934228,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.376754
            }, 
            {
                "search" : "",
                "lng" : -98.9571528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4197596
            }, 
            {
                "search" : "",
                "lng" : -92.6289332,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7094476
            }, 
            {
                "search" : "",
                "lng" : -97.1366803,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8420422
            }, 
            {
                "search" : "",
                "lng" : -99.16326,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2983513
            }, 
            {
                "search" : "",
                "lng" : -99.3517008,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0540139
            }, 
            {
                "search" : "",
                "lng" : -103.4347151,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2812337
            }, 
            {
                "search" : "",
                "lng" : -97.861582,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2512176
            }, 
            {
                "search" : "",
                "lng" : -99.2247467,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7099427
            }, 
            {
                "search" : "",
                "lng" : -103.3877123,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7038189
            }, 
            {
                "search" : "",
                "lng" : -100.180513,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.780459
            }, 
            {
                "search" : "",
                "lng" : -98.4578132,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.5961576
            }, 
            {
                "search" : "",
                "lng" : -104.3346437,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1034047
            }, 
            {
                "search" : "",
                "lng" : -99.0247751,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6696845
            }, 
            {
                "search" : "",
                "lng" : -103.4579726,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7100571
            }, 
            {
                "search" : "",
                "lng" : -98.9437195,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8266945
            }, 
            {
                "search" : "",
                "lng" : -100.3808758,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5869547
            }, 
            {
                "search" : "",
                "lng" : -100.8069799,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5225036
            }, 
            {
                "search" : "",
                "lng" : -99.6691331,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3250641
            }, 
            {
                "search" : "",
                "lng" : -101.1776103,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7014272
            }, 
            {
                "search" : "",
                "lng" : -99.2276811,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4297639
            }, 
            {
                "search" : "",
                "lng" : -103.3487804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6734428
            }, 
            {
                "search" : "",
                "lng" : -101.194239,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6998223
            }, 
            {
                "search" : "",
                "lng" : -107.3937536,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8065527
            }, 
            {
                "search" : "",
                "lng" : -103.3231249,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6716045
            }, 
            {
                "search" : "",
                "lng" : -103.3511859,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6827089
            }, 
            {
                "search" : "",
                "lng" : -115.5055817,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6298472
            }, 
            {
                "search" : "",
                "lng" : -100.7974465,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.516577
            }, 
            {
                "search" : "",
                "lng" : -103.3874432,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6739868
            }, 
            {
                "search" : "",
                "lng" : -100.8211662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5208025
            }, 
            {
                "search" : "",
                "lng" : -99.1373136,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4302678
            }, 
            {
                "search" : "",
                "lng" : -99.5610504,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4157473
            }, 
            {
                "search" : "",
                "lng" : -103.3871865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6332114
            }, 
            {
                "search" : "",
                "lng" : -99.2165822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9260271
            }, 
            {
                "search" : "",
                "lng" : -99.4791442,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2245954
            }, 
            {
                "search" : "",
                "lng" : -103.2843857,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6713596
            }, 
            {
                "search" : "",
                "lng" : -99.2882606,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3916612
            }, 
            {
                "search" : "",
                "lng" : -99.6676511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2887702
            }, 
            {
                "search" : "",
                "lng" : -96.7315382,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0633287
            }, 
            {
                "search" : "",
                "lng" : -99.651918,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2777739
            }, 
            {
                "search" : "",
                "lng" : -104.0072348,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1222634
            }, 
            {
                "search" : "",
                "lng" : -117.0345239,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5256347
            }, 
            {
                "search" : "",
                "lng" : -97.6245012,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1068915
            }, 
            {
                "search" : "",
                "lng" : -99.2961887,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6190093
            }, 
            {
                "search" : "",
                "lng" : -101.0301591,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1333736
            }, 
            {
                "search" : "",
                "lng" : -116.6256752,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.563609
            }, 
            {
                "search" : "",
                "lng" : -110.3210126,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1340535
            }, 
            {
                "search" : "",
                "lng" : -99.2010361,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6504842
            }, 
            {
                "search" : "",
                "lng" : -103.711384,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6569671
            }, 
            {
                "search" : "",
                "lng" : -96.9260693,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5219378
            }, 
            {
                "search" : "",
                "lng" : -105.2209435,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6871848
            }, 
            {
                "search" : "",
                "lng" : -99.232174,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6193242
            }, 
            {
                "search" : "",
                "lng" : -102.5081224,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5502447
            }, 
            {
                "search" : "",
                "lng" : -100.3950208,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5978839
            }, 
            {
                "search" : "",
                "lng" : -103.4540669,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7069491
            }, 
            {
                "search" : "",
                "lng" : -116.6428323,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8682781
            }, 
            {
                "search" : "",
                "lng" : -96.7271633,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.054279
            }, 
            {
                "search" : "",
                "lng" : -98.673811,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1436705
            }, 
            {
                "search" : "",
                "lng" : -99.666984,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2831323
            }, 
            {
                "search" : "",
                "lng" : -97.2943668,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3763266
            }, 
            {
                "search" : "",
                "lng" : -97.0631043,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0792585
            }, 
            {
                "search" : "",
                "lng" : -97.0947084,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8580008
            }, 
            {
                "search" : "",
                "lng" : -115.385189,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6212
            }, 
            {
                "search" : "",
                "lng" : -108.9405555,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.9102777
            }, 
            {
                "search" : "",
                "lng" : -116.5882934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5690653
            }, 
            {
                "search" : "",
                "lng" : -99.0735014,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4050463
            }, 
            {
                "search" : "",
                "lng" : -98.2598632,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0221928
            }, 
            {
                "search" : "",
                "lng" : -99.6852069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8403815
            }, 
            {
                "search" : "",
                "lng" : -109.9540825,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.9040473
            }, 
            {
                "search" : "",
                "lng" : -101.6538382,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1021282
            }, 
            {
                "search" : "",
                "lng" : -103.1844717,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2973901
            }, 
            {
                "search" : "",
                "lng" : 20.4005005,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 39.2853344
            }, 
            {
                "search" : "",
                "lng" : -101.2219193,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6599095
            }, 
            {
                "search" : "",
                "lng" : -100.8818401,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2090023
            }, 
            {
                "search" : "",
                "lng" : -99.8985096,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5278894
            }, 
            {
                "search" : "",
                "lng" : -97.5497201,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.869965
            }, 
            {
                "search" : "",
                "lng" : -109.7192313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.0997781
            }, 
            {
                "search" : "",
                "lng" : -97.0616876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8520797
            }, 
            {
                "search" : "",
                "lng" : -100.9815971,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4317882
            }, 
            {
                "search" : "",
                "lng" : -96.366048,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4538069
            }, 
            {
                "search" : "",
                "lng" : -107.3728746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8054029
            }, 
            {
                "search" : "",
                "lng" : -100.4140284,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.611354
            }, 
            {
                "search" : "",
                "lng" : -101.175548,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7019674
            }, 
            {
                "search" : "",
                "lng" : -96.3752477,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3697823
            }, 
            {
                "search" : "",
                "lng" : -110.9540435,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0929652
            }, 
            {
                "search" : "",
                "lng" : -102.5549021,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7664959
            }, 
            {
                "search" : "",
                "lng" : -106.41995,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2197005
            }, 
            {
                "search" : "",
                "lng" : -98.8260884,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7048394
            }, 
            {
                "search" : "",
                "lng" : -93.7685885,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.1047935
            }, 
            {
                "search" : "",
                "lng" : -99.2493342,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5525798
            }, 
            {
                "search" : "",
                "lng" : -99.1172533,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3152294
            }, 
            {
                "search" : "",
                "lng" : -100.3545357,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4388854
            }, 
            {
                "search" : "",
                "lng" : -115.4979749,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6499235
            }, 
            {
                "search" : "",
                "lng" : -100.4045438,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5973922
            }, 
            {
                "search" : "",
                "lng" : -100.9854628,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1565651
            }, 
            {
                "search" : "",
                "lng" : -105.7870988,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.8270654
            }, 
            {
                "search" : "",
                "lng" : -99.0932875,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6374772
            }, 
            {
                "search" : "",
                "lng" : -103.3294796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6927708
            }, 
            {
                "search" : "",
                "lng" : -110.9584136,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.074869
            }, 
            {
                "search" : "",
                "lng" : -98.3836008,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1498205
            }, 
            {
                "search" : "",
                "lng" : -100.4002512,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5832201
            }, 
            {
                "search" : "",
                "lng" : -98.4101984,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.6908954
            }, 
            {
                "search" : "",
                "lng" : -103.5126049,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5714085
            }, 
            {
                "search" : "",
                "lng" : -110.3075167,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1620201
            }, 
            {
                "search" : "",
                "lng" : -99.2958327,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3749767
            }, 
            {
                "search" : "",
                "lng" : -105.3413558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7619
            }, 
            {
                "search" : "",
                "lng" : -110.9322562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0106504
            }, 
            {
                "search" : "",
                "lng" : -105.2243698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.634015
            }, 
            {
                "search" : "",
                "lng" : -98.4913013,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.5352515
            }, 
            {
                "search" : "",
                "lng" : -99.818164,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2272392
            }, 
            {
                "search" : "",
                "lng" : -98.1774362,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.078407
            }, 
            {
                "search" : "",
                "lng" : -98.6023477,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.2950734
            }, 
            {
                "search" : "",
                "lng" : -97.453208,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9851573
            }, 
            {
                "search" : "",
                "lng" : -98.9563231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8161567
            }, 
            {
                "search" : "",
                "lng" : -92.8963776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.9965515
            }, 
            {
                "search" : "",
                "lng" : -99.2408044,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5341741
            }, 
            {
                "search" : "",
                "lng" : -112.1500215,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.7045622
            }, 
            {
                "search" : "",
                "lng" : -100.4117306,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6129969
            }, 
            {
                "search" : "",
                "lng" : -100.5142872,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6992553
            }, 
            {
                "search" : "",
                "lng" : -99.5438229,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.261897
            }, 
            {
                "search" : "",
                "lng" : -110.8891965,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9296824
            }, 
            {
                "search" : "",
                "lng" : -104.8962222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.502232
            }, 
            {
                "search" : "",
                "lng" : -99.0287315,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.380752
            }, 
            {
                "search" : "",
                "lng" : -103.4740033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5630073
            }, 
            {
                "search" : "",
                "lng" : -99.7349133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2533571
            }, 
            {
                "search" : "",
                "lng" : -101.0152819,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4032648
            }, 
            {
                "search" : "",
                "lng" : -99.1372059,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2794071
            }, 
            {
                "search" : "",
                "lng" : -99.7404796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.264908
            }, 
            {
                "search" : "",
                "lng" : -102.5357005,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7680243
            }, 
            {
                "search" : "",
                "lng" : -99.1726457,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4633398
            }, 
            {
                "search" : "",
                "lng" : -115.3952385,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6531595
            }, 
            {
                "search" : "",
                "lng" : -103.4533891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6778121
            }, 
            {
                "search" : "",
                "lng" : -96.1183262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1683219
            }, 
            {
                "search" : "",
                "lng" : -103.4355341,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5297063
            }, 
            {
                "search" : "",
                "lng" : -99.2446117,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9306988
            }, 
            {
                "search" : "",
                "lng" : -99.124534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4665622
            }, 
            {
                "search" : "",
                "lng" : -99.1209134,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4757756
            }, 
            {
                "search" : "",
                "lng" : -99.252531,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3414953
            }, 
            {
                "search" : "",
                "lng" : -99.1268163,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4187147
            }, 
            {
                "search" : "",
                "lng" : -99.1206481,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.478878
            }, 
            {
                "search" : "",
                "lng" : -99.1588661,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.293104
            }, 
            {
                "search" : "",
                "lng" : -99.1372271,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.321355
            }, 
            {
                "search" : "",
                "lng" : -99.1505277,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3028607
            }, 
            {
                "search" : "",
                "lng" : -99.1665609,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2779914
            }, 
            {
                "search" : "",
                "lng" : -99.1368814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3987873
            }, 
            {
                "search" : "",
                "lng" : -99.1344768,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3077024
            }, 
            {
                "search" : "",
                "lng" : -99.1276814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3049366
            }, 
            {
                "search" : "",
                "lng" : -100.1340697,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.194733
            }, 
            {
                "search" : "",
                "lng" : -103.4309346,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7358566
            }, 
            {
                "search" : "",
                "lng" : -100.4140644,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6347731
            }, 
            {
                "search" : "",
                "lng" : -99.2519338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3350554
            }, 
            {
                "search" : "",
                "lng" : -104.6854484,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0291907
            }, 
            {
                "search" : "",
                "lng" : -110.9020064,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9262544
            }, 
            {
                "search" : "",
                "lng" : -102.5835945,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7709975
            }, 
            {
                "search" : "",
                "lng" : -103.3281501,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7009638
            }, 
            {
                "search" : "",
                "lng" : -99.4904525,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5224022
            }, 
            {
                "search" : "",
                "lng" : -100.3760998,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4297947
            }, 
            {
                "search" : "",
                "lng" : -99.1529635,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2945602
            }, 
            {
                "search" : "",
                "lng" : -110.9429585,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0744936
            }, 
            {
                "search" : "",
                "lng" : -102.5343513,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7604373
            }, 
            {
                "search" : "",
                "lng" : -100.1972748,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7164324
            }, 
            {
                "search" : "",
                "lng" : -100.3688142,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6626225
            }, 
            {
                "search" : "",
                "lng" : -110.9378492,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0762403
            }, 
            {
                "search" : "",
                "lng" : -99.2197305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4947225
            }, 
            {
                "search" : "",
                "lng" : -96.7167981,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0901164
            }, 
            {
                "search" : "",
                "lng" : -103.4035858,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6044248
            }, 
            {
                "search" : "",
                "lng" : -103.3886171,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5155977
            }, 
            {
                "search" : "",
                "lng" : -99.3171795,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3491498
            }, 
            {
                "search" : "",
                "lng" : -97.4491363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8966116
            }, 
            {
                "search" : "",
                "lng" : -105.2324781,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6610819
            }, 
            {
                "search" : "",
                "lng" : -99.6613899,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2283033
            }, 
            {
                "search" : "",
                "lng" : -99.8744237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7847857
            }, 
            {
                "search" : "",
                "lng" : -101.594919,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0936096
            }, 
            {
                "search" : "",
                "lng" : -101.0319203,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1482003
            }, 
            {
                "search" : "",
                "lng" : 99.1966559,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 61.0137097
            }, 
            {
                "search" : "",
                "lng" : -98.2692784,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0516645
            }, 
            {
                "search" : "",
                "lng" : -108.5227825,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.148904
            }, 
            {
                "search" : "",
                "lng" : -101.2845558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9598711
            }, 
            {
                "search" : "",
                "lng" : -92.9550619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9515587
            }, 
            {
                "search" : "",
                "lng" : -96.7219722,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0219997
            }, 
            {
                "search" : "",
                "lng" : -100.8475474,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5343005
            }, 
            {
                "search" : "",
                "lng" : -100.8495387,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5349356
            }, 
            {
                "search" : "",
                "lng" : -96.1223774,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0894833
            }, 
            {
                "search" : "",
                "lng" : -99.2117564,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3090831
            }, 
            {
                "search" : "",
                "lng" : -99.2185398,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3112615
            }, 
            {
                "search" : "",
                "lng" : -99.22232,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3118331
            }, 
            {
                "search" : "",
                "lng" : -103.4725101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7817941
            }, 
            {
                "search" : "",
                "lng" : -100.187872,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.969046
            }, 
            {
                "search" : "",
                "lng" : -100.4069987,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5223467
            }, 
            {
                "search" : "",
                "lng" : -97.0434589,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0658698
            }, 
            {
                "search" : "",
                "lng" : -99.3680902,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7277938
            }, 
            {
                "search" : "",
                "lng" : -99.2794375,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.405516
            }, 
            {
                "search" : "",
                "lng" : -98.280296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0754454
            }, 
            {
                "search" : "",
                "lng" : -103.703374,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2428717
            }, 
            {
                "search" : "",
                "lng" : -96.6402701,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0518575
            }, 
            {
                "search" : "",
                "lng" : -98.3007086,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0378006
            }, 
            {
                "search" : "",
                "lng" : -98.2024674,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4007769
            }, 
            {
                "search" : "",
                "lng" : -100.4386457,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5379233
            }, 
            {
                "search" : "",
                "lng" : -100.7388162,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9051872
            }, 
            {
                "search" : "",
                "lng" : -98.2667679,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0365419
            }, 
            {
                "search" : "",
                "lng" : -99.196093,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.299013
            }, 
            {
                "search" : "",
                "lng" : -106.3925967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6910146
            }, 
            {
                "search" : "",
                "lng" : -99.0813124,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7914731
            }, 
            {
                "search" : "",
                "lng" : -99.1813322,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6864698
            }, 
            {
                "search" : "",
                "lng" : -101.1611117,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1613613
            }, 
            {
                "search" : "",
                "lng" : -105.3307239,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7587793
            }, 
            {
                "search" : "",
                "lng" : -99.1181302,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3537417
            }, 
            {
                "search" : "",
                "lng" : -102.319158,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.919818
            }, 
            {
                "search" : "",
                "lng" : -98.7622714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.11318
            }, 
            {
                "search" : "",
                "lng" : -99.1900662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4296057
            }, 
            {
                "search" : "",
                "lng" : -99.1078588,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3253543
            }, 
            {
                "search" : "",
                "lng" : -99.8647881,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8648479
            }, 
            {
                "search" : "",
                "lng" : -106.346771,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 56.130366
            }, 
            {
                "search" : "",
                "lng" : -100.4355732,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5418714
            }, 
            {
                "search" : "",
                "lng" : -99.8406196,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.1895436
            }, 
            {
                "search" : "",
                "lng" : -101.0008457,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1545402
            }, 
            {
                "search" : "",
                "lng" : -108.941309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.0254869
            }, 
            {
                "search" : "",
                "lng" : -99.1946195,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.375219
            }, 
            {
                "search" : "",
                "lng" : -102.5771333,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7597
            }, 
            {
                "search" : "",
                "lng" : -92.9232963,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9791126
            }, 
            {
                "search" : "",
                "lng" : -99.0170619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6258181
            }, 
            {
                "search" : "",
                "lng" : -99.122455,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4668366
            }, 
            {
                "search" : "",
                "lng" : -98.9049339,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.247647
            }, 
            {
                "search" : "",
                "lng" : -98.3308028,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0061482
            }, 
            {
                "search" : "",
                "lng" : -96.86363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.507535
            }, 
            {
                "search" : "",
                "lng" : -103.5139534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5831267
            }, 
            {
                "search" : "",
                "lng" : -99.6681941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8210047
            }, 
            {
                "search" : "",
                "lng" : -109.7024376,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.0636562
            }, 
            {
                "search" : "",
                "lng" : -99.2170323,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5797626
            }, 
            {
                "search" : "",
                "lng" : -98.8495087,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5165389
            }, 
            {
                "search" : "",
                "lng" : -101.9459804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1314521
            }, 
            {
                "search" : "",
                "lng" : -102.0479286,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3980176
            }, 
            {
                "search" : "",
                "lng" : -99.282393,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2001094
            }, 
            {
                "search" : "",
                "lng" : -99.480643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.278223
            }, 
            {
                "search" : "",
                "lng" : -101.5400321,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.0576275
            }, 
            {
                "search" : "",
                "lng" : -99.1207129,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0915602
            }, 
            {
                "search" : "",
                "lng" : -98.9665722,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8173135
            }, 
            {
                "search" : "",
                "lng" : -99.888764,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1497994
            }, 
            {
                "search" : "",
                "lng" : -101.418384,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.6128234
            }, 
            {
                "search" : "",
                "lng" : -105.5225259,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1727242
            }, 
            {
                "search" : "",
                "lng" : -100.3554035,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4323039
            }, 
            {
                "search" : "",
                "lng" : -104.8454619,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.7513844
            }, 
            {
                "search" : "",
                "lng" : -99.518526,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.977148
            }, 
            {
                "search" : "",
                "lng" : -99.4687836,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9155761
            }, 
            {
                "search" : "",
                "lng" : -98.9944157,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6818568
            }, 
            {
                "search" : "",
                "lng" : -98.360652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.096169
            }, 
            {
                "search" : "",
                "lng" : -100.2137821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7246818
            }, 
            {
                "search" : "",
                "lng" : -99.813088,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8335098
            }, 
            {
                "search" : "",
                "lng" : -102.274822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.144474
            }, 
            {
                "search" : "",
                "lng" : -116.605954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8480827
            }, 
            {
                "search" : "",
                "lng" : -103.3227309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5343589
            }, 
            {
                "search" : "",
                "lng" : -109.0353789,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.2692099
            }, 
            {
                "search" : "",
                "lng" : -101.7956703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8125119
            }, 
            {
                "search" : "",
                "lng" : -92.1992505,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9115572
            }, 
            {
                "search" : "",
                "lng" : -98.9754328,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8147514
            }, 
            {
                "search" : "",
                "lng" : -98.8456443,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1130519
            }, 
            {
                "search" : "",
                "lng" : -100.5917105,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2817754
            }, 
            {
                "search" : "",
                "lng" : -101.4139848,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.943651
            }, 
            {
                "search" : "",
                "lng" : -94.1013235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.894152
            }, 
            {
                "search" : "",
                "lng" : -96.9527185,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9044379
            }, 
            {
                "search" : "",
                "lng" : -101.0374182,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1299859
            }, 
            {
                "search" : "",
                "lng" : -108.4682155,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5810763
            }, 
            {
                "search" : "",
                "lng" : -101.6471215,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1853447
            }, 
            {
                "search" : "",
                "lng" : -105.156801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.6700874
            }, 
            {
                "search" : "",
                "lng" : -102.55686,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7704832
            }, 
            {
                "search" : "",
                "lng" : -104.7023299,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0114586
            }, 
            {
                "search" : "",
                "lng" : -98.7484668,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0877193
            }, 
            {
                "search" : "",
                "lng" : -105.4595306,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1871201
            }, 
            {
                "search" : "",
                "lng" : -100.2921917,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7797966
            }, 
            {
                "search" : "",
                "lng" : -110.873642,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.119441
            }, 
            {
                "search" : "",
                "lng" : -101.2703745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0203192
            }, 
            {
                "search" : "",
                "lng" : -96.712861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.9970083
            }, 
            {
                "search" : "",
                "lng" : -92.2842721,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.874747
            }, 
            {
                "search" : "",
                "lng" : -99.1129429,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2389575
            }, 
            {
                "search" : "",
                "lng" : -98.3543052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1033268
            }, 
            {
                "search" : "",
                "lng" : -100.4453504,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5918583
            }, 
            {
                "search" : "",
                "lng" : -100.4198145,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5875733
            }, 
            {
                "search" : "",
                "lng" : -101.9274144,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3352907
            }, 
            {
                "search" : "",
                "lng" : -109.644919,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.8377513
            }, 
            {
                "search" : "",
                "lng" : -101.3703974,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.510838
            }, 
            {
                "search" : "",
                "lng" : -99.819242,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8147002
            }, 
            {
                "search" : "",
                "lng" : -99.0280293,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2484432
            }, 
            {
                "search" : "",
                "lng" : -100.8994862,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.168635
            }, 
            {
                "search" : "",
                "lng" : -103.5706878,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7816215
            }, 
            {
                "search" : "",
                "lng" : -99.2276742,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5949154
            }, 
            {
                "search" : "",
                "lng" : -99.5548586,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9122946
            }, 
            {
                "search" : "",
                "lng" : -98.1730481,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9710009
            }, 
            {
                "search" : "",
                "lng" : -97.9319224,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1555761
            }, 
            {
                "search" : "",
                "lng" : -100.0052044,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9198874
            }, 
            {
                "search" : "",
                "lng" : -98.5858382,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8985071
            }, 
            {
                "search" : "",
                "lng" : -98.9063204,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2445587
            }, 
            {
                "search" : "",
                "lng" : -104.0519444,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0522222
            }, 
            {
                "search" : "",
                "lng" : -98.98876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.902012
            }, 
            {
                "search" : "",
                "lng" : -96.5130597,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7854551
            }, 
            {
                "search" : "",
                "lng" : -100.9633749,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4701245
            }, 
            {
                "search" : "",
                "lng" : -95.2315383,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4476662
            }, 
            {
                "search" : "",
                "lng" : -99.4705582,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1987962
            }, 
            {
                "search" : "",
                "lng" : -100.1815077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.6722741
            }, 
            {
                "search" : "",
                "lng" : -99.8120864,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6968449
            }, 
            {
                "search" : "",
                "lng" : -98.1302002,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.39429
            }, 
            {
                "search" : "",
                "lng" : -60.2110494,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : -33.3334669
            }, 
            {
                "search" : "",
                "lng" : -100.1507188,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9062177
            }, 
            {
                "search" : "",
                "lng" : -93.2162634,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3894222
            }, 
            {
                "search" : "",
                "lng" : -99.2184876,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4784902
            }, 
            {
                "search" : "",
                "lng" : -99.1652258,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1895877
            }, 
            {
                "search" : "",
                "lng" : -99.3777733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6044719
            }, 
            {
                "search" : "",
                "lng" : -99.184055,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9160696
            }, 
            {
                "search" : "",
                "lng" : -103.3842127,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6590038
            }, 
            {
                "search" : "",
                "lng" : -98.908036,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.43195
            }, 
            {
                "search" : "",
                "lng" : -97.8960488,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3151422
            }, 
            {
                "search" : "",
                "lng" : -97.0787538,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.8682649
            }, 
            {
                "search" : "",
                "lng" : -103.8281316,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.370728
            }, 
            {
                "search" : "",
                "lng" : -110.9569732,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.079867
            }, 
            {
                "search" : "",
                "lng" : -98.3627312,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1618984
            }, 
            {
                "search" : "",
                "lng" : -99.0380063,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9623978
            }, 
            {
                "search" : "",
                "lng" : -99.6726036,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5287194
            }, 
            {
                "search" : "",
                "lng" : -99.3361882,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.2991872
            }, 
            {
                "search" : "",
                "lng" : -96.7456909,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0868396
            }, 
            {
                "search" : "",
                "lng" : -110.91019,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.149828
            }, 
            {
                "search" : "",
                "lng" : -96.7355476,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0757632
            }, 
            {
                "search" : "",
                "lng" : -109.930463,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4933015
            }, 
            {
                "search" : "",
                "lng" : -106.407828,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2687554
            }, 
            {
                "search" : "",
                "lng" : -110.9713981,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6248118
            }, 
            {
                "search" : "",
                "lng" : -99.4714352,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2190312
            }, 
            {
                "search" : "",
                "lng" : -101.0809814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3744323
            }, 
            {
                "search" : "",
                "lng" : -99.2604328,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7403264
            }, 
            {
                "search" : "",
                "lng" : -98.8460125,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.5958003
            }, 
            {
                "search" : "",
                "lng" : -106.4046682,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.71218
            }, 
            {
                "search" : "",
                "lng" : -95.036675,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.4749695
            }, 
            {
                "search" : "",
                "lng" : -107.5961111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7263888
            }, 
            {
                "search" : "",
                "lng" : -107.3287106,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.4770426
            }, 
            {
                "search" : "",
                "lng" : -99.0773253,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7923826
            }, 
            {
                "search" : "",
                "lng" : -100.4505256,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8802204
            }, 
            {
                "search" : "",
                "lng" : -106.4111425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2494148
            }, 
            {
                "search" : "",
                "lng" : -99.6110386,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2473395
            }, 
            {
                "search" : "",
                "lng" : -98.9717745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7232618
            }, 
            {
                "search" : "",
                "lng" : -98.8891957,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4921334
            }, 
            {
                "search" : "",
                "lng" : -99.3537573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2946869
            }, 
            {
                "search" : "",
                "lng" : -98.9206199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.798281
            }, 
            {
                "search" : "",
                "lng" : -98.8397227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.514139
            }, 
            {
                "search" : "",
                "lng" : -101.1938775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0873419
            }, 
            {
                "search" : "",
                "lng" : -100.2461186,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5758734
            }, 
            {
                "search" : "",
                "lng" : -100.2851372,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3151227
            }, 
            {
                "search" : "",
                "lng" : -99.8321738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.1739803
            }, 
            {
                "search" : "",
                "lng" : -102.7305879,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0873501
            }, 
            {
                "search" : "",
                "lng" : -99.5342431,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3902449
            }, 
            {
                "search" : "",
                "lng" : -99.6546967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2548057
            }, 
            {
                "search" : "",
                "lng" : -90.1490879,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4014077
            }, 
            {
                "search" : "",
                "lng" : -98.2234552,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3165725
            }, 
            {
                "search" : "",
                "lng" : -98.4333801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.6888581
            }, 
            {
                "search" : "",
                "lng" : -98.7276029,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0305942
            }, 
            {
                "search" : "",
                "lng" : -98.6792987,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9199156
            }, 
            {
                "search" : "",
                "lng" : -98.8854912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.122817
            }, 
            {
                "search" : "",
                "lng" : -100.7832383,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5180343
            }, 
            {
                "search" : "",
                "lng" : -93.7285585,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.6859535
            }, 
            {
                "search" : "",
                "lng" : -93.1934116,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.768536
            }, 
            {
                "search" : "",
                "lng" : -102.1701988,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4375148
            }, 
            {
                "search" : "",
                "lng" : -97.3388459,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9602825
            }, 
            {
                "search" : "",
                "lng" : -106.0935133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6304096
            }, 
            {
                "search" : "",
                "lng" : -97.4091464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4936266
            }, 
            {
                "search" : "",
                "lng" : -105.123077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.952071
            }, 
            {
                "search" : "",
                "lng" : -100.8813571,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2314842
            }, 
            {
                "search" : "",
                "lng" : -99.9725425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4289297
            }, 
            {
                "search" : "",
                "lng" : -103.2315034,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6918495
            }, 
            {
                "search" : "",
                "lng" : -99.136111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6622222
            }, 
            {
                "search" : "",
                "lng" : -99.1517438,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5379811
            }, 
            {
                "search" : "",
                "lng" : -99.2325914,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.131508
            }, 
            {
                "search" : "",
                "lng" : -98.1850427,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3999482
            }, 
            {
                "search" : "",
                "lng" : -99.2366179,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0905166
            }, 
            {
                "search" : "",
                "lng" : -100.3428198,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4394776
            }, 
            {
                "search" : "",
                "lng" : -103.322624,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7169115
            }, 
            {
                "search" : "",
                "lng" : -103.3196856,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7206373
            }, 
            {
                "search" : "",
                "lng" : -95.2190497,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3427191
            }, 
            {
                "search" : "",
                "lng" : -98.9890083,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9760923
            }, 
            {
                "search" : "",
                "lng" : -98.6399939,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7251259
            }, 
            {
                "search" : "",
                "lng" : -98.6669345,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6082646
            }, 
            {
                "search" : "",
                "lng" : -103.3139733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9233778
            }, 
            {
                "search" : "",
                "lng" : -103.0681254,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6332276
            }, 
            {
                "search" : "",
                "lng" : -100.914841,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.4811449
            }, 
            {
                "search" : "",
                "lng" : -100.3887334,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0114141
            }, 
            {
                "search" : "",
                "lng" : -98.2213233,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3007905
            }, 
            {
                "search" : "",
                "lng" : -101.2829654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9811276
            }, 
            {
                "search" : "",
                "lng" : -103.3547825,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8166175
            }, 
            {
                "search" : "",
                "lng" : -103.3537955,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8065748
            }, 
            {
                "search" : "",
                "lng" : -99.2176638,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.447946
            }, 
            {
                "search" : "",
                "lng" : -99.1938311,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3100608
            }, 
            {
                "search" : "",
                "lng" : -107.3927513,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8225531
            }, 
            {
                "search" : "",
                "lng" : -102.2825886,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.983783
            }, 
            {
                "search" : "",
                "lng" : -104.8928738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4896274
            }, 
            {
                "search" : "",
                "lng" : -99.1614643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4508006
            }, 
            {
                "search" : "",
                "lng" : -93.3920085,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9892938
            }, 
            {
                "search" : "",
                "lng" : -99.9719171,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4026715
            }, 
            {
                "search" : "",
                "lng" : -100.950399,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4664914
            }, 
            {
                "search" : "",
                "lng" : -98.9319142,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3162517
            }, 
            {
                "search" : "",
                "lng" : -98.1913799,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3160862
            }, 
            {
                "search" : "",
                "lng" : -100.5534637,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2476512
            }, 
            {
                "search" : "",
                "lng" : -92.2555963,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9204992
            }, 
            {
                "search" : "",
                "lng" : -105.487363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1881893
            }, 
            {
                "search" : "",
                "lng" : -96.7306673,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.5647494
            }, 
            {
                "search" : "",
                "lng" : -96.7673038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8972018
            }, 
            {
                "search" : "",
                "lng" : -98.9801651,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8405004
            }, 
            {
                "search" : "",
                "lng" : -99.6323321,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5326359
            }, 
            {
                "search" : "",
                "lng" : -99.503046,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5371555
            }, 
            {
                "search" : "",
                "lng" : -97.0817305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.092749
            }, 
            {
                "search" : "",
                "lng" : -99.1847452,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2906421
            }, 
            {
                "search" : "",
                "lng" : -99.1823345,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4040974
            }, 
            {
                "search" : "",
                "lng" : -99.2620342,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4915951
            }, 
            {
                "search" : "",
                "lng" : -99.2316227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5626539
            }, 
            {
                "search" : "",
                "lng" : -99.167063,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3640851
            }, 
            {
                "search" : "",
                "lng" : -97.7751064,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.8053352
            }, 
            {
                "search" : "",
                "lng" : -98.9906231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.762605
            }, 
            {
                "search" : "",
                "lng" : -99.1945346,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2421786
            }, 
            {
                "search" : "",
                "lng" : -99.2222674,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3246757
            }, 
            {
                "search" : "",
                "lng" : -98.8842259,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.505384
            }, 
            {
                "search" : "",
                "lng" : -114.7907732,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4534336
            }, 
            {
                "search" : "",
                "lng" : -100.3649974,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5754009
            }, 
            {
                "search" : "",
                "lng" : -108.9685455,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8117089
            }, 
            {
                "search" : "",
                "lng" : -99.3534348,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7717876
            }, 
            {
                "search" : "",
                "lng" : -99.2144686,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5433158
            }, 
            {
                "search" : "",
                "lng" : -99.1356714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3453649
            }, 
            {
                "search" : "",
                "lng" : -100.4172469,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6227062
            }, 
            {
                "search" : "",
                "lng" : -100.296606,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6267751
            }, 
            {
                "search" : "",
                "lng" : -99.1765707,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3453633
            }, 
            {
                "search" : "",
                "lng" : -99.7489222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4208245
            }, 
            {
                "search" : "",
                "lng" : -97.8769234,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2440701
            }, 
            {
                "search" : "",
                "lng" : -99.2356048,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9674282
            }, 
            {
                "search" : "",
                "lng" : -101.6996787,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1237975
            }, 
            {
                "search" : "",
                "lng" : -103.4030449,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6520173
            }, 
            {
                "search" : "",
                "lng" : -92.9182564,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9825648
            }, 
            {
                "search" : "",
                "lng" : -100.2687153,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6942158
            }, 
            {
                "search" : "",
                "lng" : -97.8391377,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2483381
            }, 
            {
                "search" : "",
                "lng" : -99.2593544,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4903769
            }, 
            {
                "search" : "",
                "lng" : -99.1575504,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6268779
            }, 
            {
                "search" : "",
                "lng" : -99.231356,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5003001
            }, 
            {
                "search" : "",
                "lng" : -103.4420478,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6049519
            }, 
            {
                "search" : "",
                "lng" : -99.2619933,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5688863
            }, 
            {
                "search" : "",
                "lng" : -98.1974141,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0154683
            }, 
            {
                "search" : "",
                "lng" : -93.0736177,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7414834
            }, 
            {
                "search" : "",
                "lng" : -103.2516555,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6305919
            }, 
            {
                "search" : "",
                "lng" : -99.1260866,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6860054
            }, 
            {
                "search" : "",
                "lng" : -100.362948,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.576287
            }, 
            {
                "search" : "",
                "lng" : -99.2425285,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5003505
            }, 
            {
                "search" : "",
                "lng" : -99.237358,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5012157
            }, 
            {
                "search" : "",
                "lng" : -101.0337693,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1371112
            }, 
            {
                "search" : "",
                "lng" : -103.3247949,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6926446
            }, 
            {
                "search" : "",
                "lng" : -99.2390027,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.520513
            }, 
            {
                "search" : "",
                "lng" : -100.3522475,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7097571
            }, 
            {
                "search" : "",
                "lng" : -107.3938691,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7919812
            }, 
            {
                "search" : "",
                "lng" : -98.9049827,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6353646
            }, 
            {
                "search" : "",
                "lng" : -103.647125,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.6226032
            }, 
            {
                "search" : "",
                "lng" : -92.909102,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.178269
            }, 
            {
                "search" : "",
                "lng" : -106.4130279,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7092433
            }, 
            {
                "search" : "",
                "lng" : -99.2083918,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3134528
            }, 
            {
                "search" : "",
                "lng" : -99.2471129,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4916761
            }, 
            {
                "search" : "",
                "lng" : -99.2487006,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4970901
            }, 
            {
                "search" : "",
                "lng" : -111.007,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1182524
            }, 
            {
                "search" : "",
                "lng" : -105.4243804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.0426222
            }, 
            {
                "search" : "",
                "lng" : -98.7237239,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1045194
            }, 
            {
                "search" : "",
                "lng" : -96.9786326,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1610811
            }, 
            {
                "search" : "",
                "lng" : -92.2402728,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.8966075
            }, 
            {
                "search" : "",
                "lng" : -96.3664872,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3579329
            }, 
            {
                "search" : "",
                "lng" : -98.885203,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2550681
            }, 
            {
                "search" : "",
                "lng" : -92.1155691,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.2299602
            }, 
            {
                "search" : "",
                "lng" : -101.1665993,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3997407
            }, 
            {
                "search" : "",
                "lng" : -99.2448936,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5612358
            }, 
            {
                "search" : "",
                "lng" : -100.9783646,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1269076
            }, 
            {
                "search" : "",
                "lng" : -99.008047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3832226
            }, 
            {
                "search" : "",
                "lng" : -100.2498481,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7180084
            }, 
            {
                "search" : "",
                "lng" : -101.6369054,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1131177
            }, 
            {
                "search" : "",
                "lng" : -99.5249616,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.6520624
            }, 
            {
                "search" : "",
                "lng" : -99.5940417,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4653884
            }, 
            {
                "search" : "",
                "lng" : -100.0854729,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0084639
            }, 
            {
                "search" : "",
                "lng" : -99.84375,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.311143
            }, 
            {
                "search" : "",
                "lng" : -99.3201359,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1842187
            }, 
            {
                "search" : "",
                "lng" : -105.3215833,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3183589
            }, 
            {
                "search" : "",
                "lng" : -97.0007976,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9055288
            }, 
            {
                "search" : "",
                "lng" : -96.9676313,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1494466
            }, 
            {
                "search" : "",
                "lng" : -90.8069296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0637522
            }, 
            {
                "search" : "",
                "lng" : -102.2383333,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2872222
            }, 
            {
                "search" : "",
                "lng" : -103.4691038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6947783
            }, 
            {
                "search" : "",
                "lng" : -99.0877671,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6737096
            }, 
            {
                "search" : "",
                "lng" : -109.9111663,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.482478
            }, 
            {
                "search" : "",
                "lng" : -96.9578418,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4546808
            }, 
            {
                "search" : "",
                "lng" : -103.461617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4652179
            }, 
            {
                "search" : "",
                "lng" : -94.5519227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9984855
            }, 
            {
                "search" : "",
                "lng" : -106.4777438,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7381683
            }, 
            {
                "search" : "",
                "lng" : -100.2620934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.691806
            }, 
            {
                "search" : "",
                "lng" : -99.4606836,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1808939
            }, 
            {
                "search" : "",
                "lng" : -99.2268394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5334286
            }, 
            {
                "search" : "",
                "lng" : -99.156609,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3528759
            }, 
            {
                "search" : "",
                "lng" : -99.0616002,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.505553
            }, 
            {
                "search" : "",
                "lng" : -99.213711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9287138
            }, 
            {
                "search" : "",
                "lng" : -100.982617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1408372
            }, 
            {
                "search" : "",
                "lng" : -103.3284521,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6579889
            }, 
            {
                "search" : "",
                "lng" : -98.7742935,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0253981
            }, 
            {
                "search" : "",
                "lng" : -98.4637429,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6073828
            }, 
            {
                "search" : "",
                "lng" : -100.4462861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5334645
            }, 
            {
                "search" : "",
                "lng" : -99.2167067,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4113217
            }, 
            {
                "search" : "",
                "lng" : -96.116099,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1659912
            }, 
            {
                "search" : "",
                "lng" : -83.753428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 9.748917
            }, 
            {
                "search" : "",
                "lng" : -99.0510357,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5897938
            }, 
            {
                "search" : "",
                "lng" : -104.4810648,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0533791
            }, 
            {
                "search" : "",
                "lng" : -98.6487431,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1701897
            }, 
            {
                "search" : "",
                "lng" : -99.2465764,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5163744
            }, 
            {
                "search" : "",
                "lng" : -106.4273325,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2272138
            }, 
            {
                "search" : "",
                "lng" : -103.3152364,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6413853
            }, 
            {
                "search" : "",
                "lng" : -93.1209484,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7579126
            }, 
            {
                "search" : "",
                "lng" : -108.9912471,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7828007
            }, 
            {
                "search" : "",
                "lng" : -98.7374486,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.123507
            }, 
            {
                "search" : "",
                "lng" : -101.1279991,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8740052
            }, 
            {
                "search" : "",
                "lng" : -97.4320891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5340816
            }, 
            {
                "search" : "",
                "lng" : -100.9907353,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1474795
            }, 
            {
                "search" : "",
                "lng" : -115.4452197,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6604166
            }, 
            {
                "search" : "",
                "lng" : -99.236138,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9566533
            }, 
            {
                "search" : "",
                "lng" : -99.219076,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2241147
            }, 
            {
                "search" : "",
                "lng" : -99.103646,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3676599
            }, 
            {
                "search" : "",
                "lng" : -99.2198341,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5453094
            }, 
            {
                "search" : "",
                "lng" : -102.0638652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4173092
            }, 
            {
                "search" : "",
                "lng" : -99.3917881,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9202101
            }, 
            {
                "search" : "",
                "lng" : -99.0692947,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2916218
            }, 
            {
                "search" : "",
                "lng" : -117.0705222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5214394
            }, 
            {
                "search" : "",
                "lng" : -100.187672,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6450957
            }, 
            {
                "search" : "",
                "lng" : -92.2614714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9198995
            }, 
            {
                "search" : "",
                "lng" : -92.2768414,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.8935167
            }, 
            {
                "search" : "",
                "lng" : -102.1318294,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4488974
            }, 
            {
                "search" : "",
                "lng" : -102.7002512,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8096887
            }, 
            {
                "search" : "",
                "lng" : -99.2827163,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9650675
            }, 
            {
                "search" : "",
                "lng" : -102.066514,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4058899
            }, 
            {
                "search" : "",
                "lng" : -99.1732705,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4235001
            }, 
            {
                "search" : "",
                "lng" : -102.2876178,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9767057
            }, 
            {
                "search" : "",
                "lng" : -103.7220168,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2337106
            }, 
            {
                "search" : "",
                "lng" : -101.6656188,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1337118
            }, 
            {
                "search" : "",
                "lng" : -96.1222175,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1649482
            }, 
            {
                "search" : "",
                "lng" : -100.3575743,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6702403
            }, 
            {
                "search" : "",
                "lng" : -100.9050069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4204747
            }, 
            {
                "search" : "",
                "lng" : -106.0805624,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6551896
            }, 
            {
                "search" : "",
                "lng" : -107.36107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8157756
            }, 
            {
                "search" : "",
                "lng" : -103.5928664,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9805843
            }, 
            {
                "search" : "",
                "lng" : -96.1652694,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1847236
            }, 
            {
                "search" : "",
                "lng" : -101.9121235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4138174
            }, 
            {
                "search" : "",
                "lng" : -98.9500652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9233609
            }, 
            {
                "search" : "",
                "lng" : -103.400611,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7447544
            }, 
            {
                "search" : "",
                "lng" : -102.7796034,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3319135
            }, 
            {
                "search" : "",
                "lng" : -99.5277183,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.6486243
            }, 
            {
                "search" : "",
                "lng" : -99.0359412,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4946581
            }, 
            {
                "search" : "",
                "lng" : -106.9651726,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0902174
            }, 
            {
                "search" : "",
                "lng" : -103.3887172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5870799
            }, 
            {
                "search" : "",
                "lng" : -102.7340147,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.062846
            }, 
            {
                "search" : "",
                "lng" : -99.4707184,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1780875
            }, 
            {
                "search" : "",
                "lng" : -102.3450776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0886761
            }, 
            {
                "search" : "",
                "lng" : -103.4444617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5458203
            }, 
            {
                "search" : "",
                "lng" : -102.5670448,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7722317
            }, 
            {
                "search" : "",
                "lng" : -103.4444608,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5378488
            }, 
            {
                "search" : "",
                "lng" : -99.2023053,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.530433
            }, 
            {
                "search" : "",
                "lng" : -100.3624177,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4298061
            }, 
            {
                "search" : "",
                "lng" : -99.9971627,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9322605
            }, 
            {
                "search" : "",
                "lng" : -99.1514312,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3874088
            }, 
            {
                "search" : "",
                "lng" : -103.3298331,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6642334
            }, 
            {
                "search" : "",
                "lng" : -109.9313529,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.365868
            }, 
            {
                "search" : "",
                "lng" : -99.9003262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8622281
            }, 
            {
                "search" : "",
                "lng" : -101.1948019,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5767417
            }, 
            {
                "search" : "",
                "lng" : -99.6791604,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.849445
            }, 
            {
                "search" : "",
                "lng" : -103.4495029,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.2952686
            }, 
            {
                "search" : "",
                "lng" : -101.3574935,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6798486
            }, 
            {
                "search" : "",
                "lng" : -99.4763933,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5174468
            }, 
            {
                "search" : "",
                "lng" : -110.9637138,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1127668
            }, 
            {
                "search" : "",
                "lng" : -99.1245679,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3209575
            }, 
            {
                "search" : "",
                "lng" : -100.7794573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5168471
            }, 
            {
                "search" : "",
                "lng" : -98.977577,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8287271
            }, 
            {
                "search" : "",
                "lng" : -100.3726094,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5888972
            }, 
            {
                "search" : "",
                "lng" : -109.0997222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.5049999
            }, 
            {
                "search" : "",
                "lng" : -97.803993,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.827851
            }, 
            {
                "search" : "",
                "lng" : -99.6132583,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.5426307
            }, 
            {
                "search" : "",
                "lng" : -96.124205,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0704024
            }, 
            {
                "search" : "",
                "lng" : -94.5237707,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.004285
            }, 
            {
                "search" : "",
                "lng" : -100.5267428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9973185
            }, 
            {
                "search" : "",
                "lng" : -98.6613591,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2831848
            }, 
            {
                "search" : "",
                "lng" : -103.0674999,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.1955555
            }, 
            {
                "search" : "",
                "lng" : -100.4242134,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6052147
            }, 
            {
                "search" : "",
                "lng" : -102.2934809,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8751639
            }, 
            {
                "search" : "",
                "lng" : -99.1816584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4097112
            }, 
            {
                "search" : "",
                "lng" : -98.9056149,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4933276
            }, 
            {
                "search" : "",
                "lng" : -102.8772292,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1793305
            }, 
            {
                "search" : "",
                "lng" : -99.0703168,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8625616
            }, 
            {
                "search" : "",
                "lng" : -98.9964286,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3865506
            }, 
            {
                "search" : "",
                "lng" : -98.2747066,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3190818
            }, 
            {
                "search" : "",
                "lng" : -99.5331415,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4708243
            }, 
            {
                "search" : "",
                "lng" : -99.8993436,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6666387
            }, 
            {
                "search" : "",
                "lng" : -98.1914388,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.0427176
            }, 
            {
                "search" : "",
                "lng" : -99.2263228,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3196311
            }, 
            {
                "search" : "",
                "lng" : -101.012052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4235311
            }, 
            {
                "search" : "",
                "lng" : -97.8552831,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2148057
            }, 
            {
                "search" : "",
                "lng" : -101.0058288,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4325966
            }, 
            {
                "search" : "",
                "lng" : -99.2507309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4289523
            }, 
            {
                "search" : "",
                "lng" : -99.1870715,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3764015
            }, 
            {
                "search" : "",
                "lng" : -104.0365678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5473688
            }, 
            {
                "search" : "",
                "lng" : -103.3598317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6710886
            }, 
            {
                "search" : "",
                "lng" : -99.0108046,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2795782
            }, 
            {
                "search" : "",
                "lng" : -99.2249523,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7134448
            }, 
            {
                "search" : "",
                "lng" : -116.9926454,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5185548
            }, 
            {
                "search" : "",
                "lng" : -97.2473024,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7692189
            }, 
            {
                "search" : "",
                "lng" : -102.0472604,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4258334
            }, 
            {
                "search" : "",
                "lng" : -101.1969563,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6763298
            }, 
            {
                "search" : "",
                "lng" : -99.1177602,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3571138
            }, 
            {
                "search" : "",
                "lng" : -99.990464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9323238
            }, 
            {
                "search" : "",
                "lng" : -110.9649102,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.2836806
            }, 
            {
                "search" : "",
                "lng" : -100.3139317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6777843
            }, 
            {
                "search" : "",
                "lng" : -93.097112,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7428562
            }, 
            {
                "search" : "",
                "lng" : -99.475726,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2155375
            }, 
            {
                "search" : "",
                "lng" : -100.3662746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5776763
            }, 
            {
                "search" : "",
                "lng" : -106.0850518,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6454593
            }, 
            {
                "search" : "",
                "lng" : -99.2515632,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9379656
            }, 
            {
                "search" : "",
                "lng" : -108.4673051,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5666987
            }, 
            {
                "search" : "",
                "lng" : -98.2404397,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3139243
            }, 
            {
                "search" : "",
                "lng" : -96.7169768,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0741845
            }, 
            {
                "search" : "",
                "lng" : -99.0218689,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3821168
            }, 
            {
                "search" : "",
                "lng" : -106.1017474,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6489833
            }, 
            {
                "search" : "",
                "lng" : -103.3656362,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6869921
            }, 
            {
                "search" : "",
                "lng" : -104.366462,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0461532
            }, 
            {
                "search" : "",
                "lng" : -108.0655971,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4580516
            }, 
            {
                "search" : "",
                "lng" : -98.5704433,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7637724
            }, 
            {
                "search" : "",
                "lng" : -98.7305658,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1183486
            }, 
            {
                "search" : "",
                "lng" : -106.4035716,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7096236
            }, 
            {
                "search" : "",
                "lng" : -96.7065601,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0817035
            }, 
            {
                "search" : "",
                "lng" : -105.4253391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.0449564
            }, 
            {
                "search" : "",
                "lng" : -103.4635053,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7085413
            }, 
            {
                "search" : "",
                "lng" : -102.3492071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9471123
            }, 
            {
                "search" : "",
                "lng" : -107.5862134,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8413725
            }, 
            {
                "search" : "",
                "lng" : -100.9974967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1882738
            }, 
            {
                "search" : "",
                "lng" : -101.6916161,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1636775
            }, 
            {
                "search" : "",
                "lng" : -104.6534252,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0294922
            }, 
            {
                "search" : "",
                "lng" : -103.407468,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6381778
            }, 
            {
                "search" : "",
                "lng" : -98.9807823,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3876248
            }, 
            {
                "search" : "",
                "lng" : -96.8797266,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5161525
            }, 
            {
                "search" : "",
                "lng" : -99.1810795,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2644228
            }, 
            {
                "search" : "",
                "lng" : -100.3880481,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5819309
            }, 
            {
                "search" : "",
                "lng" : -96.1346231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1554057
            }, 
            {
                "search" : "",
                "lng" : -97.9678004,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.924668
            }, 
            {
                "search" : "",
                "lng" : -94.5450714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9926912
            }, 
            {
                "search" : "",
                "lng" : -103.4643283,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.212565
            }, 
            {
                "search" : "",
                "lng" : -98.5943078,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.2309078
            }, 
            {
                "search" : "",
                "lng" : -99.5049719,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4876932
            }, 
            {
                "search" : "",
                "lng" : -98.4301546,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.283486
            }, 
            {
                "search" : "",
                "lng" : -97.9233026,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3197296
            }, 
            {
                "search" : "",
                "lng" : -98.94842,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8271022
            }, 
            {
                "search" : "",
                "lng" : -100.6687604,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3532331
            }, 
            {
                "search" : "",
                "lng" : -103.269139,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7699613
            }, 
            {
                "search" : "",
                "lng" : -101.5269589,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4536351
            }, 
            {
                "search" : "",
                "lng" : -99.0581927,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4221677
            }, 
            {
                "search" : "",
                "lng" : -99.2106551,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2304048
            }, 
            {
                "search" : "",
                "lng" : -96.1296906,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1865865
            }, 
            {
                "search" : "",
                "lng" : -100.8263371,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5277132
            }, 
            {
                "search" : "",
                "lng" : -99.1114252,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4822041
            }, 
            {
                "search" : "",
                "lng" : -104.6738671,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0200333
            }, 
            {
                "search" : "",
                "lng" : -98.7851493,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.144674
            }, 
            {
                "search" : "",
                "lng" : -98.6624551,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2796518
            }, 
            {
                "search" : "",
                "lng" : -99.6048617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.338866
            }, 
            {
                "search" : "",
                "lng" : -99.6528572,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2739473
            }, 
            {
                "search" : "",
                "lng" : -99.2318453,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9223877
            }, 
            {
                "search" : "",
                "lng" : -99.5670528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9109485
            }, 
            {
                "search" : "",
                "lng" : -99.2346899,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.500398
            }, 
            {
                "search" : "",
                "lng" : -99.6667062,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.290785
            }, 
            {
                "search" : "",
                "lng" : -102.3289517,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.246938
            }, 
            {
                "search" : "",
                "lng" : -101.0124912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1482031
            }, 
            {
                "search" : "",
                "lng" : -101.012388,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.144217
            }, 
            {
                "search" : "",
                "lng" : -101.3662253,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6984144
            }, 
            {
                "search" : "",
                "lng" : -100.3913662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.581947
            }, 
            {
                "search" : "",
                "lng" : -99.2146562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4778613
            }, 
            {
                "search" : "",
                "lng" : -99.1973253,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3415922
            }, 
            {
                "search" : "",
                "lng" : -110.9607888,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0890048
            }, 
            {
                "search" : "",
                "lng" : -99.2390247,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4262578
            }, 
            {
                "search" : "",
                "lng" : -99.1719692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2928196
            }, 
            {
                "search" : "",
                "lng" : -93.1712534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0721174
            }, 
            {
                "search" : "",
                "lng" : -110.3123467,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1382136
            }, 
            {
                "search" : "",
                "lng" : -110.3064502,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1681018
            }, 
            {
                "search" : "",
                "lng" : -110.3341769,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1410968
            }, 
            {
                "search" : "",
                "lng" : -110.3160784,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1499622
            }, 
            {
                "search" : "",
                "lng" : -110.3093154,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1413408
            }, 
            {
                "search" : "",
                "lng" : -110.294543,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1451164
            }, 
            {
                "search" : "",
                "lng" : -99.1672444,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3728355
            }, 
            {
                "search" : "",
                "lng" : -97.909324,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3681638
            }, 
            {
                "search" : "",
                "lng" : -99.2215804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9682639
            }, 
            {
                "search" : "",
                "lng" : -100.3104022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6901968
            }, 
            {
                "search" : "",
                "lng" : -106.5137018,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6149598
            }, 
            {
                "search" : "",
                "lng" : -106.5075892,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6086854
            }, 
            {
                "search" : "",
                "lng" : -99.9939244,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9313476
            }, 
            {
                "search" : "",
                "lng" : -101.1922701,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7004835
            }, 
            {
                "search" : "",
                "lng" : -100.8135502,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5327755
            }, 
            {
                "search" : "",
                "lng" : -98.9953813,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2217695
            }, 
            {
                "search" : "",
                "lng" : -99.2107169,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.458957
            }, 
            {
                "search" : "",
                "lng" : -102.3519706,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7000596
            }, 
            {
                "search" : "",
                "lng" : -100.9999126,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4174344
            }, 
            {
                "search" : "",
                "lng" : -96.1312696,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2000451
            }, 
            {
                "search" : "",
                "lng" : -111.0527228,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0911893
            }, 
            {
                "search" : "",
                "lng" : -110.9662912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1109792
            }, 
            {
                "search" : "",
                "lng" : -106.0807351,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6717205
            }, 
            {
                "search" : "",
                "lng" : -104.3021657,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0543019
            }, 
            {
                "search" : "",
                "lng" : -99.1860548,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4580475
            }, 
            {
                "search" : "",
                "lng" : -107.7300302,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.3570418
            }, 
            {
                "search" : "",
                "lng" : -110.3184115,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1464638
            }, 
            {
                "search" : "",
                "lng" : -100.3619066,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6414463
            }, 
            {
                "search" : "",
                "lng" : -102.7868155,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8018168
            }, 
            {
                "search" : "",
                "lng" : -102.536644,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2930165
            }, 
            {
                "search" : "",
                "lng" : -99.1001506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7531281
            }, 
            {
                "search" : "",
                "lng" : -99.6644506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2746263
            }, 
            {
                "search" : "",
                "lng" : -105.0825593,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8777236
            }, 
            {
                "search" : "",
                "lng" : -100.2897455,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7476574
            }, 
            {
                "search" : "",
                "lng" : -103.7113531,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2541095
            }, 
            {
                "search" : "",
                "lng" : -109.940064,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4934041
            }, 
            {
                "search" : "",
                "lng" : -117.0303764,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5182679
            }, 
            {
                "search" : "",
                "lng" : -102.3104673,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 38.0638981
            }, 
            {
                "search" : "",
                "lng" : -99.255304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.383127
            }, 
            {
                "search" : "",
                "lng" : -104.5338168,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1260432
            }, 
            {
                "search" : "",
                "lng" : -97.3649683,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8147668
            }, 
            {
                "search" : "",
                "lng" : -96.1592059,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2071764
            }, 
            {
                "search" : "",
                "lng" : -110.9679238,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1084411
            }, 
            {
                "search" : "",
                "lng" : -97.3570576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8160106
            }, 
            {
                "search" : "",
                "lng" : -105.4594703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.1560516
            }, 
            {
                "search" : "",
                "lng" : -103.3302966,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7169601
            }, 
            {
                "search" : "",
                "lng" : -97.8815045,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2835558
            }, 
            {
                "search" : "",
                "lng" : -99.2581094,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9481141
            }, 
            {
                "search" : "",
                "lng" : -99.2300481,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9061191
            }, 
            {
                "search" : "",
                "lng" : -102.3457392,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7046061
            }, 
            {
                "search" : "",
                "lng" : -100.404272,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6565355
            }, 
            {
                "search" : "",
                "lng" : -98.519859,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9654063
            }, 
            {
                "search" : "",
                "lng" : -109.9429799,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.494032
            }, 
            {
                "search" : "",
                "lng" : -99.5450974,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.4391926
            }, 
            {
                "search" : "",
                "lng" : -102.9778617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.637432
            }, 
            {
                "search" : "",
                "lng" : -103.433083,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6389694
            }, 
            {
                "search" : "",
                "lng" : -99.2368833,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.466357
            }, 
            {
                "search" : "",
                "lng" : -99.1531826,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7237988
            }, 
            {
                "search" : "",
                "lng" : -100.7470077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9216949
            }, 
            {
                "search" : "",
                "lng" : -106.0749169,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6629188
            }, 
            {
                "search" : "",
                "lng" : -99.6447556,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3061635
            }, 
            {
                "search" : "",
                "lng" : -99.2323261,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5786623
            }, 
            {
                "search" : "",
                "lng" : -99.2107906,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4953724
            }, 
            {
                "search" : "",
                "lng" : -103.4188711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6711536
            }, 
            {
                "search" : "",
                "lng" : -100.0858333,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2877777
            }, 
            {
                "search" : "",
                "lng" : -99.6464649,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2678691
            }, 
            {
                "search" : "",
                "lng" : -99.159237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7445459
            }, 
            {
                "search" : "",
                "lng" : -99.1574614,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3355568
            }, 
            {
                "search" : "",
                "lng" : -103.5095831,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5628297
            }, 
            {
                "search" : "",
                "lng" : -102.2815649,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8735874
            }, 
            {
                "search" : "",
                "lng" : -102.0458961,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3999414
            }, 
            {
                "search" : "",
                "lng" : -116.9067404,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.505564
            }, 
            {
                "search" : "",
                "lng" : -102.6077727,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7754837
            }, 
            {
                "search" : "",
                "lng" : -99.1965946,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9223119
            }, 
            {
                "search" : "",
                "lng" : -99.6423567,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9591016
            }, 
            {
                "search" : "",
                "lng" : -95.2221001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3356674
            }, 
            {
                "search" : "",
                "lng" : -99.9989316,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9336753
            }, 
            {
                "search" : "",
                "lng" : -99.1228442,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3362608
            }, 
            {
                "search" : "",
                "lng" : -100.3300623,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6071541
            }, 
            {
                "search" : "",
                "lng" : -99.6706715,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.2596019
            }, 
            {
                "search" : "",
                "lng" : -109.9481973,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4861206
            }, 
            {
                "search" : "",
                "lng" : -97.8635723,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2210195
            }, 
            {
                "search" : "",
                "lng" : -99.1263924,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.454066
            }, 
            {
                "search" : "",
                "lng" : -110.9732652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6271664
            }, 
            {
                "search" : "",
                "lng" : -98.373917,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2774262
            }, 
            {
                "search" : "",
                "lng" : -106.3923968,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6570201
            }, 
            {
                "search" : "",
                "lng" : -94.4585646,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1430628
            }, 
            {
                "search" : "",
                "lng" : -96.1246446,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1800893
            }, 
            {
                "search" : "",
                "lng" : -99.1837496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3565622
            }, 
            {
                "search" : "",
                "lng" : -99.1636267,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4457649
            }, 
            {
                "search" : "",
                "lng" : -98.3722332,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0937379
            }, 
            {
                "search" : "",
                "lng" : -99.1773813,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6637459
            }, 
            {
                "search" : "",
                "lng" : -99.1191784,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4929504
            }, 
            {
                "search" : "",
                "lng" : -99.1163425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4972458
            }, 
            {
                "search" : "",
                "lng" : -99.3051087,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3709213
            }, 
            {
                "search" : "",
                "lng" : -100.3316431,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.651402
            }, 
            {
                "search" : "",
                "lng" : -109.9194638,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.8829357
            }, 
            {
                "search" : "",
                "lng" : -109.861242,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.908564
            }, 
            {
                "search" : "",
                "lng" : -91.9695895,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4873662
            }, 
            {
                "search" : "",
                "lng" : -109.9167001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.888205
            }, 
            {
                "search" : "",
                "lng" : -109.7071999,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.0881999
            }, 
            {
                "search" : "",
                "lng" : -109.733895,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.002526
            }, 
            {
                "search" : "",
                "lng" : -109.6975705,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.0610516
            }, 
            {
                "search" : "",
                "lng" : -101.6168624,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5388168
            }, 
            {
                "search" : "",
                "lng" : -98.738787,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1214328
            }, 
            {
                "search" : "",
                "lng" : -110.947772,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0999067
            }, 
            {
                "search" : "",
                "lng" : -99.1241596,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7394478
            }, 
            {
                "search" : "",
                "lng" : -105.6777517,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.931899
            }, 
            {
                "search" : "",
                "lng" : -100.4019835,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5928855
            }, 
            {
                "search" : "",
                "lng" : -100.4026865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6085583
            }, 
            {
                "search" : "",
                "lng" : -108.46124,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.571508
            }, 
            {
                "search" : "",
                "lng" : -108.9894605,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7984534
            }, 
            {
                "search" : "",
                "lng" : -99.1821199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3071505
            }, 
            {
                "search" : "",
                "lng" : -109.1690518,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.9166283
            }, 
            {
                "search" : "",
                "lng" : -103.2403346,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6628961
            }, 
            {
                "search" : "",
                "lng" : -102.7723973,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3468618
            }, 
            {
                "search" : "",
                "lng" : -96.9063903,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5670841
            }, 
            {
                "search" : "",
                "lng" : -100.9785808,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1399619
            }, 
            {
                "search" : "",
                "lng" : -101.6852569,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0982702
            }, 
            {
                "search" : "",
                "lng" : -102.7206747,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8463282
            }, 
            {
                "search" : "",
                "lng" : -102.8426624,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1918243
            }, 
            {
                "search" : "",
                "lng" : -99.6455107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2919722
            }, 
            {
                "search" : "",
                "lng" : -99.6072894,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1797162
            }, 
            {
                "search" : "",
                "lng" : -110.7990571,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9581419
            }, 
            {
                "search" : "",
                "lng" : -95.7973189,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3667399
            }, 
            {
                "search" : "",
                "lng" : -99.1788346,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3865908
            }, 
            {
                "search" : "",
                "lng" : -99.1256262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6822973
            }, 
            {
                "search" : "",
                "lng" : -99.5592077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4117213
            }, 
            {
                "search" : "",
                "lng" : -107.3960273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7903787
            }, 
            {
                "search" : "",
                "lng" : -99.6502825,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2862714
            }, 
            {
                "search" : "",
                "lng" : -99.12616,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4854447
            }, 
            {
                "search" : "",
                "lng" : -99.13098,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4771655
            }, 
            {
                "search" : "",
                "lng" : -100.367103,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.675278
            }, 
            {
                "search" : "",
                "lng" : -96.9194247,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5317249
            }, 
            {
                "search" : "",
                "lng" : -100.9573884,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1590431
            }, 
            {
                "search" : "",
                "lng" : -99.1750243,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2867752
            }, 
            {
                "search" : "",
                "lng" : -92.6370868,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7333467
            }, 
            {
                "search" : "",
                "lng" : -102.1606047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9441153
            }, 
            {
                "search" : "",
                "lng" : -102.2352106,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8705498
            }, 
            {
                "search" : "",
                "lng" : -96.1301703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1717957
            }, 
            {
                "search" : "",
                "lng" : -101.3622786,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.688333
            }, 
            {
                "search" : "",
                "lng" : -101.1837026,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6961838
            }, 
            {
                "search" : "",
                "lng" : -100.9344593,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.144732
            }, 
            {
                "search" : "",
                "lng" : -100.9798926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1499504
            }, 
            {
                "search" : "",
                "lng" : -99.9058521,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7278966
            }, 
            {
                "search" : "",
                "lng" : -110.9486322,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0942173
            }, 
            {
                "search" : "",
                "lng" : -99.1076218,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6297475
            }, 
            {
                "search" : "",
                "lng" : -98.988065,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.41456
            }, 
            {
                "search" : "",
                "lng" : -99.2119262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9271362
            }, 
            {
                "search" : "",
                "lng" : -100.282816,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.743988
            }, 
            {
                "search" : "",
                "lng" : -103.3660366,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.681434
            }, 
            {
                "search" : "",
                "lng" : -102.4085298,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0268783
            }, 
            {
                "search" : "",
                "lng" : -101.7010077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1524163
            }, 
            {
                "search" : "",
                "lng" : -100.2312505,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7175403
            }, 
            {
                "search" : "",
                "lng" : -98.2860564,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0722979
            }, 
            {
                "search" : "",
                "lng" : -103.3302951,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6762065
            }, 
            {
                "search" : "",
                "lng" : -99.2378282,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9245571
            }, 
            {
                "search" : "",
                "lng" : -96.1119757,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1550302
            }, 
            {
                "search" : "",
                "lng" : -99.5044361,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5344475
            }, 
            {
                "search" : "",
                "lng" : -110.9449361,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1006442
            }, 
            {
                "search" : "",
                "lng" : -95.7575291,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7663112
            }, 
            {
                "search" : "",
                "lng" : -108.9829779,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.811663
            }, 
            {
                "search" : "",
                "lng" : -103.4265613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6388934
            }, 
            {
                "search" : "",
                "lng" : -104.8520584,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4782789
            }, 
            {
                "search" : "",
                "lng" : -98.1378862,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4103653
            }, 
            {
                "search" : "",
                "lng" : -99.2048317,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2828788
            }, 
            {
                "search" : "",
                "lng" : -100.4018841,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6148025
            }, 
            {
                "search" : "",
                "lng" : -96.9556104,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4531489
            }, 
            {
                "search" : "",
                "lng" : -100.9704786,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1535388
            }, 
            {
                "search" : "",
                "lng" : -99.18493,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4086628
            }, 
            {
                "search" : "",
                "lng" : -101.985369,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2658552
            }, 
            {
                "search" : "",
                "lng" : -100.129937,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1918779
            }, 
            {
                "search" : "",
                "lng" : -100.9494832,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4548771
            }, 
            {
                "search" : "",
                "lng" : -106.1117678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6624959
            }, 
            {
                "search" : "",
                "lng" : -100.9918516,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1413719
            }, 
            {
                "search" : "",
                "lng" : -102.329205,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.868971
            }, 
            {
                "search" : "",
                "lng" : -95.7094023,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.0751004
            }, 
            {
                "search" : "",
                "lng" : -97.8671309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3064892
            }, 
            {
                "search" : "",
                "lng" : -102.2403165,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.5262911
            }, 
            {
                "search" : "",
                "lng" : -99.1757164,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3678486
            }, 
            {
                "search" : "",
                "lng" : -101.6736501,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1633024
            }, 
            {
                "search" : "",
                "lng" : -110.9773782,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1502833
            }, 
            {
                "search" : "",
                "lng" : -103.325548,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6845871
            }, 
            {
                "search" : "",
                "lng" : -99.6119506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3308211
            }, 
            {
                "search" : "",
                "lng" : -96.1249798,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1693635
            }, 
            {
                "search" : "",
                "lng" : -103.437772,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2859987
            }, 
            {
                "search" : "",
                "lng" : -100.371093,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.650801
            }, 
            {
                "search" : "",
                "lng" : -98.9725347,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7522362
            }, 
            {
                "search" : "",
                "lng" : -99.0290845,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.34592
            }, 
            {
                "search" : "",
                "lng" : -107.4052118,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7689373
            }, 
            {
                "search" : "",
                "lng" : -97.2098156,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9695213
            }, 
            {
                "search" : "",
                "lng" : -99.4581761,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3058055
            }, 
            {
                "search" : "",
                "lng" : -102.0626526,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9698655
            }, 
            {
                "search" : "",
                "lng" : -99.7361672,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8774788
            }, 
            {
                "search" : "",
                "lng" : -100.9790934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1651361
            }, 
            {
                "search" : "",
                "lng" : -99.1437149,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7399127
            }, 
            {
                "search" : "",
                "lng" : -96.8673128,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1850754
            }, 
            {
                "search" : "",
                "lng" : -96.9532805,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5536527
            }, 
            {
                "search" : "",
                "lng" : -101.005091,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1439311
            }, 
            {
                "search" : "",
                "lng" : -105.2191465,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6325683
            }, 
            {
                "search" : "",
                "lng" : -100.3653109,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.720711
            }, 
            {
                "search" : "",
                "lng" : -101.1802776,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6725306
            }, 
            {
                "search" : "",
                "lng" : -106.1357076,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6672379
            }, 
            {
                "search" : "",
                "lng" : -100.6414511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.6449858
            }, 
            {
                "search" : "",
                "lng" : -102.0556625,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4083551
            }, 
            {
                "search" : "",
                "lng" : -110.9675711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0727136
            }, 
            {
                "search" : "",
                "lng" : -99.4989252,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5430898
            }, 
            {
                "search" : "",
                "lng" : -99.4853542,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5344996
            }, 
            {
                "search" : "",
                "lng" : -103.0705997,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6246446
            }, 
            {
                "search" : "",
                "lng" : -102.0418203,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3438338
            }, 
            {
                "search" : "",
                "lng" : -101.6840214,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.120564
            }, 
            {
                "search" : "",
                "lng" : -99.1666026,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3523195
            }, 
            {
                "search" : "",
                "lng" : -101.6880696,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1108153
            }, 
            {
                "search" : "",
                "lng" : -92.9187641,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9911374
            }, 
            {
                "search" : "",
                "lng" : -97.8159111,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6683739
            }, 
            {
                "search" : "",
                "lng" : -109.7116654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.0646074
            }, 
            {
                "search" : "",
                "lng" : -96.9334828,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5349423
            }, 
            {
                "search" : "",
                "lng" : -99.0154709,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3717643
            }, 
            {
                "search" : "",
                "lng" : -97.50887,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.858914
            }, 
            {
                "search" : "",
                "lng" : -97.848804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2584526
            }, 
            {
                "search" : "",
                "lng" : -111.3560598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.0105682
            }, 
            {
                "search" : "",
                "lng" : -103.8738031,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9173829
            }, 
            {
                "search" : "",
                "lng" : -100.1474469,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4923136
            }, 
            {
                "search" : "",
                "lng" : -116.5731986,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7869852
            }, 
            {
                "search" : "",
                "lng" : -102.7946451,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7401585
            }, 
            {
                "search" : "",
                "lng" : -92.4646701,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.1385534
            }, 
            {
                "search" : "",
                "lng" : -99.7606956,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5695034
            }, 
            {
                "search" : "",
                "lng" : -99.536004,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0180679
            }, 
            {
                "search" : "",
                "lng" : -103.3088944,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.7792559
            }, 
            {
                "search" : "",
                "lng" : -100.012623,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.444265
            }, 
            {
                "search" : "",
                "lng" : -99.255842,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.571995
            }, 
            {
                "search" : "",
                "lng" : -99.0665261,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7909628
            }, 
            {
                "search" : "",
                "lng" : -109.6266654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9134771
            }, 
            {
                "search" : "",
                "lng" : -99.1017133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.984853
            }, 
            {
                "search" : "",
                "lng" : -107.99763,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.0902521
            }, 
            {
                "search" : "",
                "lng" : -99.9297261,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9070925
            }, 
            {
                "search" : "",
                "lng" : -89.6091485,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0380238
            }, 
            {
                "search" : "",
                "lng" : -87.2301256,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4997581
            }, 
            {
                "search" : "",
                "lng" : -99.5131772,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4010771
            }, 
            {
                "search" : "",
                "lng" : -97.4002319,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9604545
            }, 
            {
                "search" : "",
                "lng" : -86.9036204,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1345053
            }, 
            {
                "search" : "",
                "lng" : -96.9417879,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4958307
            }, 
            {
                "search" : "",
                "lng" : -105.7703358,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.1641043
            }, 
            {
                "search" : "",
                "lng" : -98.4015101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9329658
            }, 
            {
                "search" : "",
                "lng" : -89.6363461,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0814118
            }, 
            {
                "search" : "",
                "lng" : -100.1158404,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2063924
            }, 
            {
                "search" : "",
                "lng" : -89.6215789,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9987083
            }, 
            {
                "search" : "",
                "lng" : -102.2623154,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.5988395
            }, 
            {
                "search" : "",
                "lng" : -86.8207789,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1846991
            }, 
            {
                "search" : "",
                "lng" : -101.52818,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1050055
            }, 
            {
                "search" : "",
                "lng" : -100.8990546,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6164826
            }, 
            {
                "search" : "",
                "lng" : -97.1493998,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.2673664
            }, 
            {
                "search" : "",
                "lng" : -104.8856938,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4857121
            }, 
            {
                "search" : "",
                "lng" : -95.0836193,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.5452704
            }, 
            {
                "search" : "",
                "lng" : -103.2414698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8669888
            }, 
            {
                "search" : "",
                "lng" : -99.6118629,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3940094
            }, 
            {
                "search" : "",
                "lng" : -99.614678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9674545
            }, 
            {
                "search" : "",
                "lng" : -100.7606655,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0385384
            }, 
            {
                "search" : "",
                "lng" : -104.4244528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.5494432
            }, 
            {
                "search" : "",
                "lng" : -100.4516898,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5428203
            }, 
            {
                "search" : "",
                "lng" : -103.6302777,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1861111
            }, 
            {
                "search" : "",
                "lng" : -101.2357547,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6613758
            }, 
            {
                "search" : "",
                "lng" : -102.0236135,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9150534
            }, 
            {
                "search" : "",
                "lng" : -65.8393338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3786196
            }, 
            {
                "search" : "",
                "lng" : -116.5213826,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6597782
            }, 
            {
                "search" : "",
                "lng" : -99.5261662,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8140165
            }, 
            {
                "search" : "",
                "lng" : -105.3647297,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.953528
            }, 
            {
                "search" : "",
                "lng" : -100.5091952,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5344344
            }, 
            {
                "search" : "",
                "lng" : -90.4989412,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8680671
            }, 
            {
                "search" : "",
                "lng" : -116.6621259,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.8657166
            }, 
            {
                "search" : "",
                "lng" : -99.1276598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3983254
            }, 
            {
                "search" : "",
                "lng" : -99.1675894,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4453668
            }, 
            {
                "search" : "",
                "lng" : -105.4839489,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.2733755
            }, 
            {
                "search" : "",
                "lng" : -105.1395302,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1777411
            }, 
            {
                "search" : "",
                "lng" : -98.7321396,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7931558
            }, 
            {
                "search" : "",
                "lng" : -98.7862422,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2612588
            }, 
            {
                "search" : "",
                "lng" : -102.0516959,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4259437
            }, 
            {
                "search" : "",
                "lng" : -100.9948315,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5134143
            }, 
            {
                "search" : "",
                "lng" : -106.8105064,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.4466985
            }, 
            {
                "search" : "",
                "lng" : -100.6844284,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5446948
            }, 
            {
                "search" : "",
                "lng" : -103.0216979,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.9956931
            }, 
            {
                "search" : "",
                "lng" : -103.4491355,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6406716
            }, 
            {
                "search" : "",
                "lng" : -105.0638483,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.884095
            }, 
            {
                "search" : "",
                "lng" : -100.1652119,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4214923
            }, 
            {
                "search" : "",
                "lng" : -99.8339412,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.1977153
            }, 
            {
                "search" : "",
                "lng" : -116.9991434,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5201699
            }, 
            {
                "search" : "",
                "lng" : -92.9546652,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.995742
            }, 
            {
                "search" : "",
                "lng" : -99.8208632,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8736647
            }, 
            {
                "search" : "",
                "lng" : -99.1707409,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9806545
            }, 
            {
                "search" : "",
                "lng" : -99.1980808,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4373179
            }, 
            {
                "search" : "",
                "lng" : -99.1849733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4395496
            }, 
            {
                "search" : "",
                "lng" : -99.2083953,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4306575
            }, 
            {
                "search" : "",
                "lng" : -99.1920064,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4399763
            }, 
            {
                "search" : "",
                "lng" : -99.1897538,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4562713
            }, 
            {
                "search" : "",
                "lng" : -101.6970994,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1414689
            }, 
            {
                "search" : "",
                "lng" : -103.504199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.442031
            }, 
            {
                "search" : "",
                "lng" : -100.9702658,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1493098
            }, 
            {
                "search" : "",
                "lng" : -100.2941831,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8071998
            }, 
            {
                "search" : "",
                "lng" : -98.280171,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.055938
            }, 
            {
                "search" : "",
                "lng" : -99.2047347,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3499546
            }, 
            {
                "search" : "",
                "lng" : -99.224058,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2048631
            }, 
            {
                "search" : "",
                "lng" : -104.6583735,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0282918
            }, 
            {
                "search" : "",
                "lng" : -98.1502433,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8419162
            }, 
            {
                "search" : "",
                "lng" : -99.1469878,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7324541
            }, 
            {
                "search" : "",
                "lng" : -103.3875289,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6610364
            }, 
            {
                "search" : "",
                "lng" : -99.8148223,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2241732
            }, 
            {
                "search" : "",
                "lng" : -101.5287591,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6550628
            }, 
            {
                "search" : "",
                "lng" : -97.9109454,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.4075066
            }, 
            {
                "search" : "",
                "lng" : -100.3014209,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6312006
            }, 
            {
                "search" : "",
                "lng" : -101.5969367,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5073777
            }, 
            {
                "search" : "",
                "lng" : -99.3406218,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0562371
            }, 
            {
                "search" : "",
                "lng" : -108.0140527,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.1237377
            }, 
            {
                "search" : "",
                "lng" : -97.8644576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2344642
            }, 
            {
                "search" : "",
                "lng" : -98.2398972,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2148703
            }, 
            {
                "search" : "",
                "lng" : -97.4167817,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4650175
            }, 
            {
                "search" : "",
                "lng" : -99.6698596,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2951135
            }, 
            {
                "search" : "",
                "lng" : -100.5263161,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2898597
            }, 
            {
                "search" : "",
                "lng" : -98.7338957,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0833083
            }, 
            {
                "search" : "",
                "lng" : -99.975792,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4249148
            }, 
            {
                "search" : "",
                "lng" : -97.0767365,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.8719795
            }, 
            {
                "search" : "",
                "lng" : -100.9256071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1221535
            }, 
            {
                "search" : "",
                "lng" : -98.814357,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.137072
            }, 
            {
                "search" : "",
                "lng" : -107.9003296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.4203688
            }, 
            {
                "search" : "",
                "lng" : -93.1627384,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.773547
            }, 
            {
                "search" : "",
                "lng" : -93.0799531,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7551533
            }, 
            {
                "search" : "",
                "lng" : -100.3761787,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0013257
            }, 
            {
                "search" : "",
                "lng" : -103.0119448,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.6472204
            }, 
            {
                "search" : "",
                "lng" : -100.0419728,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0436608
            }, 
            {
                "search" : "",
                "lng" : -98.9733142,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8493305
            }, 
            {
                "search" : "",
                "lng" : -97.3818852,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4405996
            }, 
            {
                "search" : "",
                "lng" : -111.6488835,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.0281434
            }, 
            {
                "search" : "",
                "lng" : -99.3465254,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0580123
            }, 
            {
                "search" : "",
                "lng" : -99.0258304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.8874478
            }, 
            {
                "search" : "",
                "lng" : -97.8694309,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2619607
            }, 
            {
                "search" : "",
                "lng" : -92.2731955,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9280192
            }, 
            {
                "search" : "",
                "lng" : -100.4017501,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6573447
            }, 
            {
                "search" : "",
                "lng" : -106.4450042,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2443491
            }, 
            {
                "search" : "",
                "lng" : -106.4472439,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.244502
            }, 
            {
                "search" : "",
                "lng" : -96.6623154,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0796517
            }, 
            {
                "search" : "",
                "lng" : -99.260471,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4734469
            }, 
            {
                "search" : "",
                "lng" : -101.1934215,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1227492
            }, 
            {
                "search" : "",
                "lng" : -86.8423145,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.166217
            }, 
            {
                "search" : "",
                "lng" : -102.469587,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5881049
            }, 
            {
                "search" : "",
                "lng" : -106.4783264,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6668376
            }, 
            {
                "search" : "",
                "lng" : -99.1873798,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3708977
            }, 
            {
                "search" : "",
                "lng" : -99.1879237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2478709
            }, 
            {
                "search" : "",
                "lng" : -99.9023383,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.867162
            }, 
            {
                "search" : "",
                "lng" : -87.082588,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6350137
            }, 
            {
                "search" : "",
                "lng" : -96.1401067,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.7785251
            }, 
            {
                "search" : "",
                "lng" : -98.9447211,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8321728
            }, 
            {
                "search" : "",
                "lng" : -105.2298928,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6353642
            }, 
            {
                "search" : "",
                "lng" : -101.5167598,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8812476
            }, 
            {
                "search" : "",
                "lng" : -96.9679667,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4573943
            }, 
            {
                "search" : "",
                "lng" : -117.0524198,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5340931
            }, 
            {
                "search" : "",
                "lng" : -111.0301179,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0818051
            }, 
            {
                "search" : "",
                "lng" : -111.0007934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0837612
            }, 
            {
                "search" : "",
                "lng" : -102.2989395,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.924655
            }, 
            {
                "search" : "",
                "lng" : -103.3774733,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6828392
            }, 
            {
                "search" : "",
                "lng" : -99.2030801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9305867
            }, 
            {
                "search" : "",
                "lng" : -103.6071479,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8713062
            }, 
            {
                "search" : "",
                "lng" : -96.7217914,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0730848
            }, 
            {
                "search" : "",
                "lng" : -103.439219,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6836819
            }, 
            {
                "search" : "",
                "lng" : -99.1511305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7445582
            }, 
            {
                "search" : "",
                "lng" : -102.3320708,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2824231
            }, 
            {
                "search" : "",
                "lng" : -99.4943273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.4875201
            }, 
            {
                "search" : "",
                "lng" : -100.3461172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6876025
            }, 
            {
                "search" : "",
                "lng" : -100.7204712,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0246199
            }, 
            {
                "search" : "",
                "lng" : -98.9912941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3954069
            }, 
            {
                "search" : "",
                "lng" : -99.1642319,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2903915
            }, 
            {
                "search" : "",
                "lng" : -99.2909804,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3626419
            }, 
            {
                "search" : "",
                "lng" : -4.42139880000002,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 36.7212737
            }, 
            {
                "search" : "",
                "lng" : -101.2370198,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6980719
            }, 
            {
                "search" : "",
                "lng" : -99.0528716,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3472733
            }, 
            {
                "search" : "",
                "lng" : -101.004085,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4223298
            }, 
            {
                "search" : "",
                "lng" : -103.384062,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.71817
            }, 
            {
                "search" : "",
                "lng" : -98.4373351,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9053658
            }, 
            {
                "search" : "",
                "lng" : -104.0115223,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3194074
            }, 
            {
                "search" : "",
                "lng" : -99.8733444,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8031794
            }, 
            {
                "search" : "",
                "lng" : -100.8234236,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5233928
            }, 
            {
                "search" : "",
                "lng" : -97.9258574,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0324328
            }, 
            {
                "search" : "",
                "lng" : -110.9596534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.1157022
            }, 
            {
                "search" : "",
                "lng" : -102.563176,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.434436
            }, 
            {
                "search" : "",
                "lng" : -100.7225944,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.8148044
            }, 
            {
                "search" : "",
                "lng" : -98.9821371,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7337143
            }, 
            {
                "search" : "",
                "lng" : -100.3125395,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7406435
            }, 
            {
                "search" : "",
                "lng" : -99.1704965,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3637008
            }, 
            {
                "search" : "",
                "lng" : -102.3028739,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8791633
            }, 
            {
                "search" : "",
                "lng" : -98.4403227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2740781
            }, 
            {
                "search" : "",
                "lng" : -100.963216,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4680654
            }, 
            {
                "search" : "",
                "lng" : -98.1957828,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3211738
            }, 
            {
                "search" : "",
                "lng" : -92.6375988,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7369928
            }, 
            {
                "search" : "",
                "lng" : -99.1169819,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3240315
            }, 
            {
                "search" : "",
                "lng" : -100.3865757,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6083642
            }, 
            {
                "search" : "",
                "lng" : -98.9683073,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7122732
            }, 
            {
                "search" : "",
                "lng" : -100.0107869,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5715602
            }, 
            {
                "search" : "",
                "lng" : -105.2344859,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6527258
            }, 
            {
                "search" : "",
                "lng" : -99.1752977,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4535061
            }, 
            {
                "search" : "",
                "lng" : -103.3973867,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5250353
            }, 
            {
                "search" : "",
                "lng" : -100.0150649,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.404238
            }, 
            {
                "search" : "",
                "lng" : -100.9335018,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.14042
            }, 
            {
                "search" : "",
                "lng" : -98.6524164,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6522811
            }, 
            {
                "search" : "",
                "lng" : -102.2977499,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9011979
            }, 
            {
                "search" : "",
                "lng" : -101.4107617,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9433994
            }, 
            {
                "search" : "",
                "lng" : -99.565491,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.16688
            }, 
            {
                "search" : "",
                "lng" : -103.3524312,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6769092
            }, 
            {
                "search" : "",
                "lng" : -101.1892747,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6998864
            }, 
            {
                "search" : "",
                "lng" : -99.1819234,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4487966
            }, 
            {
                "search" : "",
                "lng" : -99.6270303,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1351852
            }, 
            {
                "search" : "",
                "lng" : -99.6564745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2800352
            }, 
            {
                "search" : "",
                "lng" : -98.2076282,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7680193
            }, 
            {
                "search" : "",
                "lng" : -99.2088686,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4741447
            }, 
            {
                "search" : "",
                "lng" : -99.6306541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2697227
            }, 
            {
                "search" : "",
                "lng" : -99.5776051,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8647068
            }, 
            {
                "search" : "",
                "lng" : -99.5113396,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5639738
            }, 
            {
                "search" : "",
                "lng" : -101.242954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0034302
            }, 
            {
                "search" : "",
                "lng" : -100.386847,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6012861
            }, 
            {
                "search" : "",
                "lng" : -100.556615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6926127
            }, 
            {
                "search" : "",
                "lng" : -100.3649462,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2045733
            }, 
            {
                "search" : "",
                "lng" : -98.5407917,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6537964
            }, 
            {
                "search" : "",
                "lng" : -108.0762625,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4682648
            }, 
            {
                "search" : "",
                "lng" : -99.6118885,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2566011
            }, 
            {
                "search" : "",
                "lng" : -98.3767532,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0867723
            }, 
            {
                "search" : "",
                "lng" : -99.311711,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6183311
            }, 
            {
                "search" : "",
                "lng" : -97.9352653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3904989
            }, 
            {
                "search" : "",
                "lng" : -100.2590222,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6733919
            }, 
            {
                "search" : "",
                "lng" : -103.4428513,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6260529
            }, 
            {
                "search" : "",
                "lng" : -92.940203,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9999214
            }, 
            {
                "search" : "",
                "lng" : -103.4417168,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5399402
            }, 
            {
                "search" : "",
                "lng" : -98.9427333,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2759433
            }, 
            {
                "search" : "",
                "lng" : -99.3396331,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9078259
            }, 
            {
                "search" : "",
                "lng" : -99.7759403,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.7206733
            }, 
            {
                "search" : "",
                "lng" : -99.2045953,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8726916
            }, 
            {
                "search" : "",
                "lng" : -106.4035612,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.6617903
            }, 
            {
                "search" : "",
                "lng" : -99.1585997,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4226528
            }, 
            {
                "search" : "",
                "lng" : -105.3615134,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7820021
            }, 
            {
                "search" : "",
                "lng" : -100.7428066,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9156189
            }, 
            {
                "search" : "",
                "lng" : -107.3939979,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7779966
            }, 
            {
                "search" : "",
                "lng" : -97.8568883,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3141845
            }, 
            {
                "search" : "",
                "lng" : -99.1353173,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3962295
            }, 
            {
                "search" : "",
                "lng" : -101.4222636,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9010344
            }, 
            {
                "search" : "",
                "lng" : -99.2000743,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4409769
            }, 
            {
                "search" : "",
                "lng" : -99.2000941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.627835
            }, 
            {
                "search" : "",
                "lng" : -99.6035911,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.5551293
            }, 
            {
                "search" : "",
                "lng" : -99.5580591,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.254225
            }, 
            {
                "search" : "",
                "lng" : -99.6052453,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2604578
            }, 
            {
                "search" : "",
                "lng" : -101.0349639,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.346372
            }, 
            {
                "search" : "",
                "lng" : -94.4314366,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1426929
            }, 
            {
                "search" : "",
                "lng" : -94.9145656,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9491782
            }, 
            {
                "search" : "",
                "lng" : -96.1519909,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1922181
            }, 
            {
                "search" : "",
                "lng" : -98.2346493,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.322726
            }, 
            {
                "search" : "",
                "lng" : -99.2740176,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.444405
            }, 
            {
                "search" : "",
                "lng" : -97.9025286,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9646929
            }, 
            {
                "search" : "",
                "lng" : -99.1414182,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4388321
            }, 
            {
                "search" : "",
                "lng" : -98.4084852,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1317079
            }, 
            {
                "search" : "",
                "lng" : -100.1566705,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1941826
            }, 
            {
                "search" : "",
                "lng" : -97.4314579,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5420267
            }, 
            {
                "search" : "",
                "lng" : -117.0039495,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.4668754
            }, 
            {
                "search" : "",
                "lng" : -102.3448277,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6937113
            }, 
            {
                "search" : "",
                "lng" : -99.1346547,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3984744
            }, 
            {
                "search" : "",
                "lng" : -100.4388573,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.886691
            }, 
            {
                "search" : "",
                "lng" : -100.3299238,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6768093
            }, 
            {
                "search" : "",
                "lng" : -100.346449,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.693683
            }, 
            {
                "search" : "",
                "lng" : -100.3816557,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5719001
            }, 
            {
                "search" : "",
                "lng" : -109.6851526,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.3766498
            }, 
            {
                "search" : "",
                "lng" : -103.3066885,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6942423
            }, 
            {
                "search" : "",
                "lng" : -103.4085106,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6309294
            }, 
            {
                "search" : "",
                "lng" : -99.0097066,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.289207
            }, 
            {
                "search" : "",
                "lng" : -103.3953266,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6933396
            }, 
            {
                "search" : "",
                "lng" : -94.5623697,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9922526
            }, 
            {
                "search" : "",
                "lng" : -102.7690744,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.81493
            }, 
            {
                "search" : "",
                "lng" : -103.4357674,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5394812
            }, 
            {
                "search" : "",
                "lng" : -96.670134,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7891138
            }, 
            {
                "search" : "",
                "lng" : -100.3476843,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7201996
            }, 
            {
                "search" : "",
                "lng" : -101.2021564,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5711939
            }, 
            {
                "search" : "",
                "lng" : -109.9578736,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5022627
            }, 
            {
                "search" : "",
                "lng" : -98.1371696,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4154062
            }, 
            {
                "search" : "",
                "lng" : -99.0849702,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5206397
            }, 
            {
                "search" : "",
                "lng" : -99.0783613,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.652916
            }, 
            {
                "search" : "",
                "lng" : -98.3666894,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0843793
            }, 
            {
                "search" : "",
                "lng" : -105.3649057,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.4895309
            }, 
            {
                "search" : "",
                "lng" : -100.3228196,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7861112
            }, 
            {
                "search" : "",
                "lng" : -99.1858456,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9601198
            }, 
            {
                "search" : "",
                "lng" : -102.321334,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2248086
            }, 
            {
                "search" : "",
                "lng" : -100.4305971,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6643254
            }, 
            {
                "search" : "",
                "lng" : -97.4529599,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5390765
            }, 
            {
                "search" : "",
                "lng" : -101.1837125,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6795855
            }, 
            {
                "search" : "",
                "lng" : -106.4149234,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2324203
            }, 
            {
                "search" : "",
                "lng" : -103.4195379,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6353797
            }, 
            {
                "search" : "",
                "lng" : -106.4611891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7192549
            }, 
            {
                "search" : "",
                "lng" : -96.7173952,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0625149
            }, 
            {
                "search" : "",
                "lng" : -99.4032451,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.564415
            }, 
            {
                "search" : "",
                "lng" : -109.9350823,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5001115
            }, 
            {
                "search" : "",
                "lng" : -101.6120811,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5401406
            }, 
            {
                "search" : "",
                "lng" : -103.4918245,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5884091
            }, 
            {
                "search" : "",
                "lng" : -102.9925219,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.6348837
            }, 
            {
                "search" : "",
                "lng" : -112.0519564,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6756348
            }, 
            {
                "search" : "",
                "lng" : -102.77527,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8022118
            }, 
            {
                "search" : "",
                "lng" : -110.955321,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0949566
            }, 
            {
                "search" : "",
                "lng" : -110.9579121,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0945954
            }, 
            {
                "search" : "",
                "lng" : -104.6459032,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0257128
            }, 
            {
                "search" : "",
                "lng" : -97.4092207,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9563576
            }, 
            {
                "search" : "",
                "lng" : -98.9302706,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4109865
            }, 
            {
                "search" : "",
                "lng" : -100.4063615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6831407
            }, 
            {
                "search" : "",
                "lng" : -99.657646,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.291316
            }, 
            {
                "search" : "",
                "lng" : -101.936628,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3509174
            }, 
            {
                "search" : "",
                "lng" : -98.951701,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8349744
            }, 
            {
                "search" : "",
                "lng" : -99.6577494,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.286842
            }, 
            {
                "search" : "",
                "lng" : -99.1833955,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6103059
            }, 
            {
                "search" : "",
                "lng" : -105.2023654,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8131442
            }, 
            {
                "search" : "",
                "lng" : -99.1616944,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3891111
            }, 
            {
                "search" : "",
                "lng" : -98.3758622,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.076247
            }, 
            {
                "search" : "",
                "lng" : -119.7871247,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 36.7377981
            }, 
            {
                "search" : "",
                "lng" : -102.8819139,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.1784491
            }, 
            {
                "search" : "",
                "lng" : -94.4571105,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1452067
            }, 
            {
                "search" : "",
                "lng" : -101.0011496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4062773
            }, 
            {
                "search" : "",
                "lng" : -105.1441697,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1856457
            }, 
            {
                "search" : "",
                "lng" : -100.3845671,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6178899
            }, 
            {
                "search" : "",
                "lng" : -100.3876694,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7818783
            }, 
            {
                "search" : "",
                "lng" : -99.197865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6586106
            }, 
            {
                "search" : "",
                "lng" : -101.7905181,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8075124
            }, 
            {
                "search" : "",
                "lng" : -97.0939983,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8534987
            }, 
            {
                "search" : "",
                "lng" : -97.114854,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8624026
            }, 
            {
                "search" : "",
                "lng" : -99.1058213,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4561891
            }, 
            {
                "search" : "",
                "lng" : -92.25257,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9175833
            }, 
            {
                "search" : "",
                "lng" : -106.4126133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.654931
            }, 
            {
                "search" : "",
                "lng" : -94.4508678,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1396761
            }, 
            {
                "search" : "",
                "lng" : -101.6300253,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1099946
            }, 
            {
                "search" : "",
                "lng" : -102.975718,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6402753
            }, 
            {
                "search" : "",
                "lng" : -96.9142331,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5198844
            }, 
            {
                "search" : "",
                "lng" : -99.1952534,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4034907
            }, 
            {
                "search" : "",
                "lng" : -100.3160687,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6669148
            }, 
            {
                "search" : "",
                "lng" : -100.394698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5862157
            }, 
            {
                "search" : "",
                "lng" : -100.8457523,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.3558197
            }, 
            {
                "search" : "",
                "lng" : -92.1417588,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.2409149
            }, 
            {
                "search" : "",
                "lng" : -100.6878512,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5474034
            }, 
            {
                "search" : "",
                "lng" : -98.9896643,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3994934
            }, 
            {
                "search" : "",
                "lng" : -106.8651511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.4026389
            }, 
            {
                "search" : "",
                "lng" : -104.4082908,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.5458844
            }, 
            {
                "search" : "",
                "lng" : -99.1769922,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2930914
            }, 
            {
                "search" : "",
                "lng" : -99.0673223,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4022974
            }, 
            {
                "search" : "",
                "lng" : -99.0897471,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4374733
            }, 
            {
                "search" : "",
                "lng" : -102.2789954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8991992
            }, 
            {
                "search" : "",
                "lng" : -99.1592961,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4171268
            }, 
            {
                "search" : "",
                "lng" : -104.6690467,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0258398
            }, 
            {
                "search" : "",
                "lng" : -106.0818533,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6459557
            }, 
            {
                "search" : "",
                "lng" : -106.0989434,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6531858
            }, 
            {
                "search" : "",
                "lng" : -101.0105979,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.3913645
            }, 
            {
                "search" : "",
                "lng" : -100.1940007,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6601473
            }, 
            {
                "search" : "",
                "lng" : -97.8439136,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2492827
            }, 
            {
                "search" : "",
                "lng" : -100.3292056,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6707796
            }, 
            {
                "search" : "",
                "lng" : -92.0458949,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.4847697
            }, 
            {
                "search" : "",
                "lng" : -98.8632562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6870948
            }, 
            {
                "search" : "",
                "lng" : -99.1751318,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9000667
            }, 
            {
                "search" : "",
                "lng" : -101.1957172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5739314
            }, 
            {
                "search" : "",
                "lng" : -96.7166957,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0764186
            }, 
            {
                "search" : "",
                "lng" : -99.247768,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6314016
            }, 
            {
                "search" : "",
                "lng" : -94.5762389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.988396
            }, 
            {
                "search" : "",
                "lng" : -106.4648251,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7307616
            }, 
            {
                "search" : "",
                "lng" : -101.0004572,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1871638
            }, 
            {
                "search" : "",
                "lng" : -99.1657833,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8832248
            }, 
            {
                "search" : "",
                "lng" : -99.7118483,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2708841
            }, 
            {
                "search" : "",
                "lng" : -86.822296,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.161008
            }, 
            {
                "search" : "",
                "lng" : -94.5665969,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9876623
            }, 
            {
                "search" : "",
                "lng" : -99.1214822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3015921
            }, 
            {
                "search" : "",
                "lng" : -98.7405993,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0676282
            }, 
            {
                "search" : "",
                "lng" : -103.3549117,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5916704
            }, 
            {
                "search" : "",
                "lng" : -99.1921976,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3688174
            }, 
            {
                "search" : "",
                "lng" : -100.7363071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8007827
            }, 
            {
                "search" : "",
                "lng" : -99.2603258,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5136817
            }, 
            {
                "search" : "",
                "lng" : -100.6341637,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.6459755
            }, 
            {
                "search" : "",
                "lng" : -96.1120248,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1105889
            }, 
            {
                "search" : "",
                "lng" : -103.1889464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2971255
            }, 
            {
                "search" : "",
                "lng" : -104.6684362,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0047253
            }, 
            {
                "search" : "",
                "lng" : -99.1744213,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9025505
            }, 
            {
                "search" : "",
                "lng" : -101.6405209,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1022974
            }, 
            {
                "search" : "",
                "lng" : -101.5471557,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.645929
            }, 
            {
                "search" : "",
                "lng" : -103.4602057,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7231007
            }, 
            {
                "search" : "",
                "lng" : -99.2092754,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8752189
            }, 
            {
                "search" : "",
                "lng" : -99.1456675,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4385323
            }, 
            {
                "search" : "",
                "lng" : -102.3115019,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9289497
            }, 
            {
                "search" : "",
                "lng" : -99.243568,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5512153
            }, 
            {
                "search" : "",
                "lng" : -99.2634767,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5074944
            }, 
            {
                "search" : "",
                "lng" : -105.2446235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6771733
            }, 
            {
                "search" : "",
                "lng" : -102.2957951,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9674147
            }, 
            {
                "search" : "",
                "lng" : -101.0152895,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.3783686
            }, 
            {
                "search" : "",
                "lng" : -100.411583,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5836012
            }, 
            {
                "search" : "",
                "lng" : -117.0070877,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5188228
            }, 
            {
                "search" : "",
                "lng" : -99.0380778,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5823162
            }, 
            {
                "search" : "",
                "lng" : -100.3667387,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7020058
            }, 
            {
                "search" : "",
                "lng" : -101.5850651,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.6592775
            }, 
            {
                "search" : "",
                "lng" : -99.235658,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5565549
            }, 
            {
                "search" : "",
                "lng" : -103.3809047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5725071
            }, 
            {
                "search" : "",
                "lng" : -100.7951001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5145488
            }, 
            {
                "search" : "",
                "lng" : -100.9941147,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.3780787
            }, 
            {
                "search" : "",
                "lng" : -103.4035428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7062093
            }, 
            {
                "search" : "",
                "lng" : -105.2186077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6358351
            }, 
            {
                "search" : "",
                "lng" : -105.2163273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6382969
            }, 
            {
                "search" : "",
                "lng" : -100.4538237,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6853029
            }, 
            {
                "search" : "",
                "lng" : -99.2206402,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4833782
            }, 
            {
                "search" : "",
                "lng" : -103.3973511,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5181688
            }, 
            {
                "search" : "",
                "lng" : -103.2889172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6945638
            }, 
            {
                "search" : "",
                "lng" : -102.4598682,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.169458
            }, 
            {
                "search" : "",
                "lng" : -99.6596981,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2726392
            }, 
            {
                "search" : "",
                "lng" : -96.1294515,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1637735
            }, 
            {
                "search" : "",
                "lng" : -101.3941683,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6895335
            }, 
            {
                "search" : "",
                "lng" : -100.7124692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0284612
            }, 
            {
                "search" : "",
                "lng" : -99.5548394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2456629
            }, 
            {
                "search" : "",
                "lng" : -98.2380688,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0380038
            }, 
            {
                "search" : "",
                "lng" : -106.0720024,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6339555
            }, 
            {
                "search" : "",
                "lng" : -99.074288,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8755609
            }, 
            {
                "search" : "",
                "lng" : -99.6724744,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2980142
            }, 
            {
                "search" : "",
                "lng" : -99.6807685,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2831242
            }, 
            {
                "search" : "",
                "lng" : -99.6810069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2863293
            }, 
            {
                "search" : "",
                "lng" : -99.6480334,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3663503
            }, 
            {
                "search" : "",
                "lng" : -103.0061871,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.6482372
            }, 
            {
                "search" : "",
                "lng" : -99.1552907,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3350284
            }, 
            {
                "search" : "",
                "lng" : -106.8930969,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.9220417
            }, 
            {
                "search" : "",
                "lng" : -103.3489938,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6908274
            }, 
            {
                "search" : "",
                "lng" : -112.0548641,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 30.6780922
            }, 
            {
                "search" : "",
                "lng" : -99.1157639,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4462416
            }, 
            {
                "search" : "",
                "lng" : -101.2366955,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6874938
            }, 
            {
                "search" : "",
                "lng" : -91.9740653,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5135453
            }, 
            {
                "search" : "",
                "lng" : -103.3657109,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7353795
            }, 
            {
                "search" : "",
                "lng" : -99.5214316,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3418409
            }, 
            {
                "search" : "",
                "lng" : -110.95381,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.036389
            }, 
            {
                "search" : "",
                "lng" : -103.4481288,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5772681
            }, 
            {
                "search" : "",
                "lng" : -101.2151044,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6998913
            }, 
            {
                "search" : "",
                "lng" : -109.4550863,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.064529
            }, 
            {
                "search" : "",
                "lng" : -99.9612276,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8979116
            }, 
            {
                "search" : "",
                "lng" : -100.4055717,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6412217
            }, 
            {
                "search" : "",
                "lng" : -106.1034086,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6767797
            }, 
            {
                "search" : "",
                "lng" : -99.1495107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4356338
            }, 
            {
                "search" : "",
                "lng" : -110.9558403,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0735801
            }, 
            {
                "search" : "",
                "lng" : -100.4072072,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5855686
            }, 
            {
                "search" : "",
                "lng" : -99.6483969,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2776219
            }, 
            {
                "search" : "",
                "lng" : -99.6470809,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2753786
            }, 
            {
                "search" : "",
                "lng" : -103.4204209,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6621387
            }, 
            {
                "search" : "",
                "lng" : -99.1419616,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3615089
            }, 
            {
                "search" : "",
                "lng" : -99.2142936,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9241254
            }, 
            {
                "search" : "",
                "lng" : -101.1801391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7062094
            }, 
            {
                "search" : "",
                "lng" : -106.4586816,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7289585
            }, 
            {
                "search" : "",
                "lng" : -99.1715892,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6758035
            }, 
            {
                "search" : "",
                "lng" : -99.5397344,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3448477
            }, 
            {
                "search" : "",
                "lng" : -99.138935,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5139158
            }, 
            {
                "search" : "",
                "lng" : -98.7788174,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.120242
            }, 
            {
                "search" : "",
                "lng" : -101.6963914,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1511498
            }, 
            {
                "search" : "",
                "lng" : -99.1825831,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.367112
            }, 
            {
                "search" : "",
                "lng" : -99.0347413,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3970357
            }, 
            {
                "search" : "",
                "lng" : -99.1273664,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.385057
            }, 
            {
                "search" : "",
                "lng" : -99.1944913,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4234148
            }, 
            {
                "search" : "",
                "lng" : -100.9516837,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1768329
            }, 
            {
                "search" : "",
                "lng" : -105.673435,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9322908
            }, 
            {
                "search" : "",
                "lng" : -97.9588092,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2761285
            }, 
            {
                "search" : "",
                "lng" : -97.806419,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.817683
            }, 
            {
                "search" : "",
                "lng" : -100.3293255,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.787801
            }, 
            {
                "search" : "",
                "lng" : -99.133477,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.385302
            }, 
            {
                "search" : "",
                "lng" : -99.2337955,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8378058
            }, 
            {
                "search" : "",
                "lng" : -102.1738022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2241419
            }, 
            {
                "search" : "",
                "lng" : -106.4409714,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7306598
            }, 
            {
                "search" : "",
                "lng" : -107.3922245,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8175518
            }, 
            {
                "search" : "",
                "lng" : -97.1076703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8527436
            }, 
            {
                "search" : "",
                "lng" : -99.1748712,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4601172
            }, 
            {
                "search" : "",
                "lng" : -97.1089696,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.842249
            }, 
            {
                "search" : "",
                "lng" : -98.5763331,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5863558
            }, 
            {
                "search" : "",
                "lng" : -99.794895,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.705789
            }, 
            {
                "search" : "",
                "lng" : -98.7583068,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7025805
            }, 
            {
                "search" : "",
                "lng" : -99.8945927,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5264758
            }, 
            {
                "search" : "",
                "lng" : -94.1481726,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1363918
            }, 
            {
                "search" : "",
                "lng" : -101.432179,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9503778
            }, 
            {
                "search" : "",
                "lng" : -102.2911072,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9766118
            }, 
            {
                "search" : "",
                "lng" : -100.1213167,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2118804
            }, 
            {
                "search" : "",
                "lng" : -97.1122311,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.033831
            }, 
            {
                "search" : "",
                "lng" : -100.3729496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6130346
            }, 
            {
                "search" : "",
                "lng" : -92.6177479,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7042605
            }, 
            {
                "search" : "",
                "lng" : -98.7653418,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0934965
            }, 
            {
                "search" : "",
                "lng" : -96.7530794,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.05952
            }, 
            {
                "search" : "",
                "lng" : -99.1681973,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3485302
            }, 
            {
                "search" : "",
                "lng" : -101.4414418,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9421031
            }, 
            {
                "search" : "",
                "lng" : -96.1459053,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.2440685
            }, 
            {
                "search" : "",
                "lng" : -99.2023409,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9206548
            }, 
            {
                "search" : "",
                "lng" : -100.4914272,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6848755
            }, 
            {
                "search" : "",
                "lng" : -92.1313188,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.2464854
            }, 
            {
                "search" : "",
                "lng" : -93.1071304,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.752773
            }, 
            {
                "search" : "",
                "lng" : -92.2555467,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.9022447
            }, 
            {
                "search" : "",
                "lng" : -92.3684359,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3641275
            }, 
            {
                "search" : "",
                "lng" : -107.3907761,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8701744
            }, 
            {
                "search" : "",
                "lng" : -100.2776066,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6552841
            }, 
            {
                "search" : "",
                "lng" : -98.205941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.013059
            }, 
            {
                "search" : "",
                "lng" : -98.2205503,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0007976
            }, 
            {
                "search" : "",
                "lng" : -99.0197318,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6248648
            }, 
            {
                "search" : "",
                "lng" : -98.8621964,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5124874
            }, 
            {
                "search" : "",
                "lng" : -98.2208029,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0445107
            }, 
            {
                "search" : "",
                "lng" : -100.727291,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.893095
            }, 
            {
                "search" : "",
                "lng" : -99.24301,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9383859
            }, 
            {
                "search" : "",
                "lng" : -99.2383967,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5680491
            }, 
            {
                "search" : "",
                "lng" : -99.2304621,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5279362
            }, 
            {
                "search" : "",
                "lng" : -98.8317464,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3373151
            }, 
            {
                "search" : "",
                "lng" : -103.0427227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.8324026
            }, 
            {
                "search" : "",
                "lng" : -99.3978939,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.727412
            }, 
            {
                "search" : "",
                "lng" : -99.1860163,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.6165097
            }, 
            {
                "search" : "",
                "lng" : -100.1766289,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6811005
            }, 
            {
                "search" : "",
                "lng" : -98.2104052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0093849
            }, 
            {
                "search" : "",
                "lng" : -99.5094389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2663797
            }, 
            {
                "search" : "",
                "lng" : -100.9529323,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4838276
            }, 
            {
                "search" : "",
                "lng" : -93.3783937,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9822363
            }, 
            {
                "search" : "",
                "lng" : -98.8910901,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9326422
            }, 
            {
                "search" : "",
                "lng" : -102.4138738,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.519222
            }, 
            {
                "search" : "",
                "lng" : -96.1150034,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.148671
            }, 
            {
                "search" : "",
                "lng" : -99.1740397,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3593753
            }, 
            {
                "search" : "",
                "lng" : -97.5199581,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0152771
            }, 
            {
                "search" : "",
                "lng" : -96.7165071,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8112383
            }, 
            {
                "search" : "",
                "lng" : -100.165888,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9493406
            }, 
            {
                "search" : "",
                "lng" : -101.8545589,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0368309
            }, 
            {
                "search" : "",
                "lng" : -99.1107602,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3853439
            }, 
            {
                "search" : "",
                "lng" : -106.116073,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6911912
            }, 
            {
                "search" : "",
                "lng" : -99.1571457,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3651671
            }, 
            {
                "search" : "",
                "lng" : -99.4676086,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1932184
            }, 
            {
                "search" : "",
                "lng" : -98.2073801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9358471
            }, 
            {
                "search" : "",
                "lng" : -98.2478489,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9884682
            }, 
            {
                "search" : "",
                "lng" : -104.6863403,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2323753
            }, 
            {
                "search" : "",
                "lng" : -99.1441684,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8103776
            }, 
            {
                "search" : "",
                "lng" : -97.0876114,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.84257
            }, 
            {
                "search" : "",
                "lng" : -98.2369847,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9829011
            }, 
            {
                "search" : "",
                "lng" : -99.1754773,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2663853
            }, 
            {
                "search" : "",
                "lng" : -100.5294374,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.2885794
            }, 
            {
                "search" : "",
                "lng" : -99.9016976,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5261479
            }, 
            {
                "search" : "",
                "lng" : -97.9748753,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9420195
            }, 
            {
                "search" : "",
                "lng" : -97.8715245,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2833476
            }, 
            {
                "search" : "",
                "lng" : -98.7454425,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1168102
            }, 
            {
                "search" : "",
                "lng" : -98.5709822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.769879
            }, 
            {
                "search" : "",
                "lng" : -100.4694981,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6347843
            }, 
            {
                "search" : "",
                "lng" : -94.1436033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1425936
            }, 
            {
                "search" : "",
                "lng" : -97.853629,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2970234
            }, 
            {
                "search" : "",
                "lng" : -99.14382,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.7733432
            }, 
            {
                "search" : "",
                "lng" : -93.0981177,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7628478
            }, 
            {
                "search" : "",
                "lng" : -99.8943412,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5188861
            }, 
            {
                "search" : "",
                "lng" : -100.4054508,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5558881
            }, 
            {
                "search" : "",
                "lng" : -99.1935014,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2798986
            }, 
            {
                "search" : "",
                "lng" : -100.6979482,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3263674
            }, 
            {
                "search" : "",
                "lng" : -98.0801788,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.9827572
            }, 
            {
                "search" : "",
                "lng" : -92.5979394,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.7616617
            }, 
            {
                "search" : "",
                "lng" : -99.6266648,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2636551
            }, 
            {
                "search" : "",
                "lng" : -92.5907704,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.7618058
            }, 
            {
                "search" : "",
                "lng" : -98.2173351,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0024503
            }, 
            {
                "search" : "",
                "lng" : -98.2427432,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0450857
            }, 
            {
                "search" : "",
                "lng" : -97.0892363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8408806
            }, 
            {
                "search" : "",
                "lng" : -99.1596479,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.365313
            }, 
            {
                "search" : "",
                "lng" : -101.9378137,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3536825
            }, 
            {
                "search" : "",
                "lng" : -110.3077199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1433234
            }, 
            {
                "search" : "",
                "lng" : -103.5311215,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5399464
            }, 
            {
                "search" : "",
                "lng" : -98.5618432,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.5587083
            }, 
            {
                "search" : "",
                "lng" : -101.0282236,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.383716
            }, 
            {
                "search" : "",
                "lng" : -103.444343,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4705009
            }, 
            {
                "search" : "",
                "lng" : -99.234739,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5275224
            }, 
            {
                "search" : "",
                "lng" : -107.068615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.8154559
            }, 
            {
                "search" : "",
                "lng" : -98.0418216,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1824032
            }, 
            {
                "search" : "",
                "lng" : -101.5768203,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2963053
            }, 
            {
                "search" : "",
                "lng" : -108.9955022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7906734
            }, 
            {
                "search" : "",
                "lng" : -100.4435848,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7309571
            }, 
            {
                "search" : "",
                "lng" : -100.781363,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.536914
            }, 
            {
                "search" : "",
                "lng" : -99.6321632,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2265772
            }, 
            {
                "search" : "",
                "lng" : -101.6566777,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1475556
            }, 
            {
                "search" : "",
                "lng" : -100.9820187,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4495522
            }, 
            {
                "search" : "",
                "lng" : -95.2083276,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.4401262
            }, 
            {
                "search" : "",
                "lng" : -99.572698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2677947
            }, 
            {
                "search" : "",
                "lng" : -96.6459107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.028519
            }, 
            {
                "search" : "",
                "lng" : -99.2768387,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3658302
            }, 
            {
                "search" : "",
                "lng" : -102.3994867,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0269286
            }, 
            {
                "search" : "",
                "lng" : -99.2433471,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8276017
            }, 
            {
                "search" : "",
                "lng" : -94.9010703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9505735
            }, 
            {
                "search" : "",
                "lng" : -99.2609758,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3734509
            }, 
            {
                "search" : "",
                "lng" : -101.7199032,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1497611
            }, 
            {
                "search" : "",
                "lng" : -92.9416348,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.9884821
            }, 
            {
                "search" : "",
                "lng" : -92.9508389,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.996455
            }, 
            {
                "search" : "",
                "lng" : -98.5219796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9047393
            }, 
            {
                "search" : "",
                "lng" : -98.4363305,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2984737
            }, 
            {
                "search" : "",
                "lng" : -95.1785199,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.1696686
            }, 
            {
                "search" : "",
                "lng" : -97.3570233,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3275748
            }, 
            {
                "search" : "",
                "lng" : -100.5250172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.7099143
            }, 
            {
                "search" : "",
                "lng" : -99.1219671,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3069344
            }, 
            {
                "search" : "",
                "lng" : -98.7739946,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0741127
            }, 
            {
                "search" : "",
                "lng" : -98.7500045,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1059199
            }, 
            {
                "search" : "",
                "lng" : -100.8836562,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.690675
            }, 
            {
                "search" : "",
                "lng" : -101.4580316,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2388883
            }, 
            {
                "search" : "",
                "lng" : -100.2635612,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7033941
            }, 
            {
                "search" : "",
                "lng" : 66.9749730999999,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 39.627012
            }, 
            {
                "search" : "",
                "lng" : -101.0154086,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1781724
            }, 
            {
                "search" : "",
                "lng" : -101.5144971,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.8760627
            }, 
            {
                "search" : "",
                "lng" : -101.1970294,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.577463
            }, 
            {
                "search" : "",
                "lng" : -102.478915,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1638491
            }, 
            {
                "search" : "",
                "lng" : -105.2043702,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.813166
            }, 
            {
                "search" : "",
                "lng" : -102.3069934,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9288714
            }, 
            {
                "search" : "",
                "lng" : -99.1259793,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4122454
            }, 
            {
                "search" : "",
                "lng" : -100.4124127,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5788542
            }, 
            {
                "search" : "",
                "lng" : -98.146801,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4104703
            }, 
            {
                "search" : "",
                "lng" : -99.1669241,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3989545
            }, 
            {
                "search" : "",
                "lng" : -110.99601,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0907741
            }, 
            {
                "search" : "",
                "lng" : -101.1900528,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.392169
            }, 
            {
                "search" : "",
                "lng" : -95.2119814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.2072577
            }, 
            {
                "search" : "",
                "lng" : -101.4634703,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2296544
            }, 
            {
                "search" : "",
                "lng" : -99.1592969,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2965052
            }, 
            {
                "search" : "",
                "lng" : -99.1349814,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2879168
            }, 
            {
                "search" : "",
                "lng" : -100.3668218,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6891606
            }, 
            {
                "search" : "",
                "lng" : -104.8929306,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4781277
            }, 
            {
                "search" : "",
                "lng" : -86.8461672,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1575645
            }, 
            {
                "search" : "",
                "lng" : -102.8869085,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.3543457
            }, 
            {
                "search" : "",
                "lng" : -99.1559924,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4154885
            }, 
            {
                "search" : "",
                "lng" : -110.9953484,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0818399
            }, 
            {
                "search" : "",
                "lng" : -104.213113,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.79845
            }, 
            {
                "search" : "",
                "lng" : -101.2141863,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6693837
            }, 
            {
                "search" : "",
                "lng" : -99.6630052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2772455
            }, 
            {
                "search" : "",
                "lng" : -88.4791376,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1817393
            }, 
            {
                "search" : "",
                "lng" : -97.6702594,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9104425
            }, 
            {
                "search" : "",
                "lng" : -98.966659,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.6072859
            }, 
            {
                "search" : "",
                "lng" : -103.4393719,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5902943
            }, 
            {
                "search" : "",
                "lng" : -102.467185,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1714119
            }, 
            {
                "search" : "",
                "lng" : -92.6507087,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7388442
            }, 
            {
                "search" : "",
                "lng" : -102.5664902,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7686279
            }, 
            {
                "search" : "",
                "lng" : -100.9483074,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5411499
            }, 
            {
                "search" : "",
                "lng" : -102.1841756,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4384251
            }, 
            {
                "search" : "",
                "lng" : -99.1293371,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2970108
            }, 
            {
                "search" : "",
                "lng" : -99.2210148,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6597174
            }, 
            {
                "search" : "",
                "lng" : -101.5177018,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6510035
            }, 
            {
                "search" : "",
                "lng" : -99.1129974,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3085999
            }, 
            {
                "search" : "",
                "lng" : -99.8615807,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7855559
            }, 
            {
                "search" : "",
                "lng" : -100.9592273,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4392584
            }, 
            {
                "search" : "",
                "lng" : -107.3968001,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.829737
            }, 
            {
                "search" : "",
                "lng" : -96.717783,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0566108
            }, 
            {
                "search" : "",
                "lng" : -115.4723683,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.6643254
            }, 
            {
                "search" : "",
                "lng" : -99.0802624,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3793721
            }, 
            {
                "search" : "",
                "lng" : -96.9154292,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.844597
            }, 
            {
                "search" : "",
                "lng" : -99.2427744,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.479521
            }, 
            {
                "search" : "",
                "lng" : -107.4122069,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8181687
            }, 
            {
                "search" : "",
                "lng" : -99.1312746,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4392245
            }, 
            {
                "search" : "",
                "lng" : -98.1075753,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4339092
            }, 
            {
                "search" : "",
                "lng" : -100.9544926,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1146137
            }, 
            {
                "search" : "",
                "lng" : -99.2140186,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5240919
            }, 
            {
                "search" : "",
                "lng" : -99.8688098,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8795595
            }, 
            {
                "search" : "",
                "lng" : -99.1876339,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3739795
            }, 
            {
                "search" : "",
                "lng" : -110.9492133,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.0852216
            }, 
            {
                "search" : "",
                "lng" : -94.4119987,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.1509448
            }, 
            {
                "search" : "",
                "lng" : -96.9236849,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.534839
            }, 
            {
                "search" : "",
                "lng" : -101.189145,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6852296
            }, 
            {
                "search" : "",
                "lng" : -99.2389608,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3993367
            }, 
            {
                "search" : "",
                "lng" : -100.4028109,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7251178
            }, 
            {
                "search" : "",
                "lng" : -99.3406039,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.3026076
            }, 
            {
                "search" : "",
                "lng" : -103.3873235,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6534289
            }, 
            {
                "search" : "",
                "lng" : -99.2216941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3055049
            }, 
            {
                "search" : "",
                "lng" : -103.4260373,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5606242
            }, 
            {
                "search" : "",
                "lng" : -99.2251084,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.932452
            }, 
            {
                "search" : "",
                "lng" : -98.1987656,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0128707
            }, 
            {
                "search" : "",
                "lng" : -97.4216692,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.94567
            }, 
            {
                "search" : "",
                "lng" : -103.4314032,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5611108
            }, 
            {
                "search" : "",
                "lng" : -98.9043422,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.82892
            }, 
            {
                "search" : "",
                "lng" : -107.3978487,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7960799
            }, 
            {
                "search" : "",
                "lng" : -103.2703393,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.3033203
            }, 
            {
                "search" : "",
                "lng" : -99.1988003,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.337471
            }, 
            {
                "search" : "",
                "lng" : -107.5087813,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.7759678
            }, 
            {
                "search" : "",
                "lng" : -101.4246048,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.93314
            }, 
            {
                "search" : "",
                "lng" : -108.9970585,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8060435
            }, 
            {
                "search" : "",
                "lng" : -102.3212762,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8948823
            }, 
            {
                "search" : "",
                "lng" : -98.1950054,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.003584
            }, 
            {
                "search" : "",
                "lng" : -106.4202405,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2227204
            }, 
            {
                "search" : "",
                "lng" : -98.3789329,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0822845
            }, 
            {
                "search" : "",
                "lng" : -102.2989255,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8827325
            }, 
            {
                "search" : "",
                "lng" : -100.2972399,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6900871
            }, 
            {
                "search" : "",
                "lng" : -101.6976543,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1475819
            }, 
            {
                "search" : "",
                "lng" : -99.1019261,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6501238
            }, 
            {
                "search" : "",
                "lng" : -101.6106565,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5119686
            }, 
            {
                "search" : "",
                "lng" : -106.0915033,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.6231646
            }, 
            {
                "search" : "",
                "lng" : -103.4479314,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4736865
            }, 
            {
                "search" : "",
                "lng" : -99.1851957,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3783132
            }, 
            {
                "search" : "",
                "lng" : -96.7397007,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.0637962
            }, 
            {
                "search" : "",
                "lng" : -112.8954541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.2820675
            }, 
            {
                "search" : "",
                "lng" : -93.2246941,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.2667025
            }, 
            {
                "search" : "",
                "lng" : -99.1879047,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3632158
            }, 
            {
                "search" : "",
                "lng" : -101.3690393,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6676014
            }, 
            {
                "search" : "",
                "lng" : -102.2844807,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.9677833
            }, 
            {
                "search" : "",
                "lng" : -101.7811615,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8149794
            }, 
            {
                "search" : "",
                "lng" : -101.1877628,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1196009
            }, 
            {
                "search" : "",
                "lng" : -71.6126885,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : -33.047238
            }, 
            {
                "search" : "",
                "lng" : -103.453392,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6348303
            }, 
            {
                "search" : "",
                "lng" : -101.2548589,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.0221594
            }, 
            {
                "search" : "",
                "lng" : -116.9863646,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 32.5001946
            }, 
            {
                "search" : "",
                "lng" : -106.6843975,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9018979
            }, 
            {
                "search" : "",
                "lng" : -102.255262,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.8789163
            }, 
            {
                "search" : "",
                "lng" : -99.1758522,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3715065
            }, 
            {
                "search" : "",
                "lng" : -98.8986806,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2835211
            }, 
            {
                "search" : "",
                "lng" : -99.2461405,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9404778
            }, 
            {
                "search" : "",
                "lng" : -101.287647,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.963443
            }, 
            {
                "search" : "",
                "lng" : -99.1431476,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2953258
            }, 
            {
                "search" : "",
                "lng" : -102.3087092,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9073515
            }, 
            {
                "search" : "",
                "lng" : -107.0779748,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.8130337
            }, 
            {
                "search" : "",
                "lng" : -99.0752488,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3332451
            }, 
            {
                "search" : "",
                "lng" : -101.5281085,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.4442068
            }, 
            {
                "search" : "",
                "lng" : -99.1625067,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.413459
            }, 
            {
                "search" : "",
                "lng" : -103.8401192,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.8917291
            }, 
            {
                "search" : "",
                "lng" : -103.3946077,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6688888
            }, 
            {
                "search" : "",
                "lng" : -100.2878579,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7447288
            }, 
            {
                "search" : "",
                "lng" : -98.2205465,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3199976
            }, 
            {
                "search" : "",
                "lng" : -93.876869,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.281481
            }, 
            {
                "search" : "",
                "lng" : -103.4765206,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5673346
            }, 
            {
                "search" : "",
                "lng" : -99.0521391,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3048045
            }, 
            {
                "search" : "",
                "lng" : -99.2324556,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3668381
            }, 
            {
                "search" : "",
                "lng" : -103.4041997,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.659716
            }, 
            {
                "search" : "",
                "lng" : -99.1582856,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4448153
            }, 
            {
                "search" : "",
                "lng" : -99.1292519,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3335483
            }, 
            {
                "search" : "",
                "lng" : -103.4069093,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6667538
            }, 
            {
                "search" : "",
                "lng" : -100.3000796,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7462163
            }, 
            {
                "search" : "",
                "lng" : -103.5102244,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5650447
            }, 
            {
                "search" : "",
                "lng" : -100.2609912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.7518473
            }, 
            {
                "search" : "",
                "lng" : -99.9771576,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 17.7903254
            }, 
            {
                "search" : "",
                "lng" : -102.8462493,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.3687449
            }, 
            {
                "search" : "",
                "lng" : -99.1863856,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4362455
            }, 
            {
                "search" : "",
                "lng" : -93.1604131,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7535422
            }, 
            {
                "search" : "",
                "lng" : -99.314775,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.54145
            }, 
            {
                "search" : "",
                "lng" : -98.9528506,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8203305
            }, 
            {
                "search" : "",
                "lng" : -93.9019972,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.237793
            }, 
            {
                "search" : "",
                "lng" : -93.1316989,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.7620517
            }, 
            {
                "search" : "",
                "lng" : -98.053687,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.3411824
            }, 
            {
                "search" : "",
                "lng" : -99.0876748,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6161251
            }, 
            {
                "search" : "",
                "lng" : -101.3029821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.9280741
            }, 
            {
                "search" : "",
                "lng" : -103.3905553,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7190585
            }, 
            {
                "search" : "",
                "lng" : -99.1885541,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5613347
            }, 
            {
                "search" : "",
                "lng" : -106.3695966,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.600786
            }, 
            {
                "search" : "",
                "lng" : -110.320774,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.1271487
            }, 
            {
                "search" : "",
                "lng" : -105.6756266,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9292889
            }, 
            {
                "search" : "",
                "lng" : -99.8706029,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7854273
            }, 
            {
                "search" : "",
                "lng" : -99.1951291,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4074232
            }, 
            {
                "search" : "",
                "lng" : -99.6067531,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5573493
            }, 
            {
                "search" : "",
                "lng" : -100.7433424,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9015652
            }, 
            {
                "search" : "",
                "lng" : -99.0886757,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4183335
            }, 
            {
                "search" : "",
                "lng" : -99.2043596,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4031004
            }, 
            {
                "search" : "",
                "lng" : -99.061936,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3894903
            }, 
            {
                "search" : "",
                "lng" : -99.2675743,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4984657
            }, 
            {
                "search" : "",
                "lng" : -99.897097,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8644512
            }, 
            {
                "search" : "",
                "lng" : -98.4293891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9131227
            }, 
            {
                "search" : "",
                "lng" : -99.9014867,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8545823
            }, 
            {
                "search" : "",
                "lng" : -109.4519211,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.0810012
            }, 
            {
                "search" : "",
                "lng" : -109.4516618,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.0762718
            }, 
            {
                "search" : "",
                "lng" : -100.9216819,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.147225
            }, 
            {
                "search" : "",
                "lng" : -99.1886749,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4846606
            }, 
            {
                "search" : "",
                "lng" : -99.0086601,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9849661
            }, 
            {
                "search" : "",
                "lng" : -96.8959496,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5311654
            }, 
            {
                "search" : "",
                "lng" : -92.2536082,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 14.8882563
            }, 
            {
                "search" : "",
                "lng" : -99.1838269,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3500532
            }, 
            {
                "search" : "",
                "lng" : -99.1201474,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2994011
            }, 
            {
                "search" : "",
                "lng" : -100.5352052,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 28.7039797
            }, 
            {
                "search" : "",
                "lng" : -97.0734142,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 15.8711999
            }, 
            {
                "search" : "",
                "lng" : -110.9410783,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.2980032
            }, 
            {
                "search" : "",
                "lng" : -103.4992328,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5810042
            }, 
            {
                "search" : "",
                "lng" : -101.192172,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6869606
            }, 
            {
                "search" : "",
                "lng" : -107.3974972,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8164449
            }, 
            {
                "search" : "",
                "lng" : -97.5873659,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8658977
            }, 
            {
                "search" : "",
                "lng" : -100.962686,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.4773669
            }, 
            {
                "search" : "",
                "lng" : -98.2712308,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.078415
            }, 
            {
                "search" : "",
                "lng" : -101.1884543,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5834185
            }, 
            {
                "search" : "",
                "lng" : -99.27837,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.336049
            }, 
            {
                "search" : "",
                "lng" : -104.8847101,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.4929253
            }, 
            {
                "search" : "",
                "lng" : -100.9857785,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.1524793
            }, 
            {
                "search" : "",
                "lng" : -100.2759105,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6174225
            }, 
            {
                "search" : "",
                "lng" : -99.2322924,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6068603
            }, 
            {
                "search" : "",
                "lng" : -98.2656412,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.064004
            }, 
            {
                "search" : "",
                "lng" : -99.1148005,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3732066
            }, 
            {
                "search" : "",
                "lng" : -101.6793212,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1261939
            }, 
            {
                "search" : "",
                "lng" : -101.3484022,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6777933
            }, 
            {
                "search" : "",
                "lng" : -105.1763279,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.7006881
            }, 
            {
                "search" : "",
                "lng" : -99.0871745,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6254965
            }, 
            {
                "search" : "",
                "lng" : -100.1688114,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8051521
            }, 
            {
                "search" : "",
                "lng" : -101.4215236,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 26.9080378
            }, 
            {
                "search" : "",
                "lng" : -99.2122216,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.659898
            }, 
            {
                "search" : "",
                "lng" : -96.1324452,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1792704
            }, 
            {
                "search" : "",
                "lng" : -96.168357,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2070134
            }, 
            {
                "search" : "",
                "lng" : -101.3758277,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6945591
            }, 
            {
                "search" : "",
                "lng" : -99.9770739,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.8604067
            }, 
            {
                "search" : "",
                "lng" : -99.8431428,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.8442497
            }, 
            {
                "search" : "",
                "lng" : -104.2208359,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8067875
            }, 
            {
                "search" : "",
                "lng" : -106.4319048,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 31.7185682
            }, 
            {
                "search" : "",
                "lng" : -96.1320636,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1955437
            }, 
            {
                "search" : "",
                "lng" : -99.0059,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.983089
            }, 
            {
                "search" : "",
                "lng" : -99.0425338,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4982047
            }, 
            {
                "search" : "",
                "lng" : -100.9133542,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 29.3023274
            }, 
            {
                "search" : "",
                "lng" : -100.2916083,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.646523
            }, 
            {
                "search" : "",
                "lng" : -101.5199997,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0919551
            }, 
            {
                "search" : "",
                "lng" : -101.0218215,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0057925
            }, 
            {
                "search" : "",
                "lng" : -98.9746578,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7421938
            }, 
            {
                "search" : "",
                "lng" : -66.58973,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 6.42375
            }, 
            {
                "search" : "",
                "lng" : -99.5615689,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8588709
            }, 
            {
                "search" : "",
                "lng" : -92.713193,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.432502
            }, 
            {
                "search" : "",
                "lng" : -99.6601454,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2558405
            }, 
            {
                "search" : "",
                "lng" : -111.6628218,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.0376061
            }, 
            {
                "search" : "",
                "lng" : -103.7190345,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2630858
            }, 
            {
                "search" : "",
                "lng" : -99.5297499,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2614676
            }, 
            {
                "search" : "",
                "lng" : -98.1529793,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3512509
            }, 
            {
                "search" : "",
                "lng" : -96.5783387,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2601605
            }, 
            {
                "search" : "",
                "lng" : -100.9206659,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.1445552
            }, 
            {
                "search" : "",
                "lng" : -109.9358475,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.5117252
            }, 
            {
                "search" : "",
                "lng" : -99.2024038,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5661305
            }, 
            {
                "search" : "",
                "lng" : -99.2020255,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5859363
            }, 
            {
                "search" : "",
                "lng" : -99.0956014,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6339764
            }, 
            {
                "search" : "",
                "lng" : -99.0803645,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6262983
            }, 
            {
                "search" : "",
                "lng" : -99.0426227,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5744277
            }, 
            {
                "search" : "",
                "lng" : -98.7484954,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1182391
            }, 
            {
                "search" : "",
                "lng" : -102.5301799,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.7565157
            }, 
            {
                "search" : "",
                "lng" : -107.4346912,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8017144
            }, 
            {
                "search" : "",
                "lng" : -99.6326252,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2628885
            }, 
            {
                "search" : "",
                "lng" : -99.5930865,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2305873
            }, 
            {
                "search" : "",
                "lng" : -99.0085753,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 21.9926026
            }, 
            {
                "search" : "",
                "lng" : -97.2437162,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5603254
            }, 
            {
                "search" : "",
                "lng" : -99.600698,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2621204
            }, 
            {
                "search" : "",
                "lng" : -101.2001074,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.7043277
            }, 
            {
                "search" : "",
                "lng" : -99.8770966,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.5306505
            }, 
            {
                "search" : "",
                "lng" : -98.7336433,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.1261551
            }, 
            {
                "search" : "",
                "lng" : -105.231871,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6389
            }, 
            {
                "search" : "",
                "lng" : -99.6424891,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3150589
            }, 
            {
                "search" : "",
                "lng" : -102.3459312,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 23.2876529
            }, 
            {
                "search" : "",
                "lng" : -107.4288821,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.8401207
            }, 
            {
                "search" : "",
                "lng" : -101.3741868,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.7028758
            }, 
            {
                "search" : "",
                "lng" : -97.8746039,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 22.2479504
            }, 
            {
                "search" : "",
                "lng" : -98.2112842,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.0156441
            }, 
            {
                "search" : "",
                "lng" : -100.256927,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.693593
            }, 
            {
                "search" : "",
                "lng" : -99.214115,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5198988
            }, 
            {
                "search" : "",
                "lng" : -99.2270558,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5219996
            }, 
            {
                "search" : "",
                "lng" : -99.2161397,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5191825
            }, 
            {
                "search" : "",
                "lng" : -99.224397,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5218613
            }, 
            {
                "search" : "",
                "lng" : -99.1404486,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.4288347
            }, 
            {
                "search" : "",
                "lng" : -97.4969449,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.8640507
            }, 
            {
                "search" : "",
                "lng" : -99.6387889,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2602978
            }, 
            {
                "search" : "",
                "lng" : -103.4188316,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.6432858
            }, 
            {
                "search" : "",
                "lng" : -100.3841772,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6511451
            }, 
            {
                "search" : "",
                "lng" : -90.5324289,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.8102817
            }, 
            {
                "search" : "",
                "lng" : -96.1021443,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.103257
            }, 
            {
                "search" : "",
                "lng" : -99.153961,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3945776
            }, 
            {
                "search" : "",
                "lng" : -99.1552861,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3828146
            }, 
            {
                "search" : "",
                "lng" : -98.9462486,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9199961
            }, 
            {
                "search" : "",
                "lng" : -99.1184732,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.3344378
            }, 
            {
                "search" : "",
                "lng" : -101.6474231,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5993274
            }, 
            {
                "search" : "",
                "lng" : -109.937632,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 27.497746
            }, 
            {
                "search" : "",
                "lng" : -99.2022706,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 18.9272205
            }, 
            {
                "search" : "",
                "lng" : -100.7463189,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.9121826
            }, 
            {
                "search" : "",
                "lng" : -99.1798468,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.2975476
            }, 
            {
                "search" : "",
                "lng" : -98.3721958,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.0808002
            }, 
            {
                "search" : "",
                "lng" : -100.3092351,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.6769915
            }, 
            {
                "search" : "",
                "lng" : -95.2087625,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 16.1842839
            }, 
            {
                "search" : "",
                "lng" : -100.8762274,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 20.2143462
            }, 
            {
                "search" : "",
                "lng" : -99.462014,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.1878539
            }, 
            {
                "search" : "",
                "lng" : -104.6659107,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 24.0293803
            }, 
            {
                "search" : "",
                "lng" : -99.1025822,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.6414883
            }, 
            {
                "search" : "",
                "lng" : -108.476204,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 25.5908915
            }, 
            {
                "search" : "",
                "lng" : -96.9173669,
                "rad" : 1,
                "type" : "poi",
                "category" : "csv-coords",
                "lat" : 19.5135133
            }
        ]

result = []

for (let i=0; i<location.length; i++){

    let lat_i = Number(location[i].lat);
    let lon_i = Number(location[i].lng);

    let insert = true;

    for (let j=0; j<location.length; j++){

        if (i != j && insert){

            let lat_j = Number(location[j].lat);
            let lon_j = Number(location[j].lng);
    
            if (Math.sqrt(Math.pow(lon_i - lon_j, 2) + Math.pow(lat_i - lat_j, 2)) < 0.0049){
                insert = false;
            }
        }
    }
    if (insert){
        result.push(location[i]);
    }
}
// console.log(location.length, result.length);
console.log(JSON.stringify(result));