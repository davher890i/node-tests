var _ = require('lodash');
var fs = require('fs');

// MongoDB connection
var MongoClient = require('mongodb').MongoClient;
var urlPushDB = 'mongodb://infinia:14fi4IA@ds145191-a0.mlab.com:45191,ds145191-a1.mlab.com:45191/push?replicaSet=rs-ds145191';

var pushMongoPromise = new Promise((resolve, reject) => {
    MongoClient.connect(urlPushDB, function(err, db) {
        if (err || !db) {
            console.log('Error connecting to MongoDB', err);
        } else {
            resolve(db);
            db.on('close', function() {
                console.log('Connection closed. Reconnecting...');
                mongoConnection(urlIndoorDB, urlPushDB, urlClustersDB, urlCampaignsDB);
            });
        }
    });
}).then(db => {
    var devicesCollection = db.collection('devices');

    var path = __dirname + '/' + process.argv[2]
    // var path = __dirname + '/firebase_ids/Guadalajara_5a6b96ddc9e77c0005bc2dbf.csv';
    fs.readFile(path, (err, data) => {
        if (err) {
            console.log(err);
            throw err;
        } else {
            var idArray = data.toString().toLowerCase().split('\n');
             devicesCollection.find({
                app_token: 'a99a8b892d3e5b7eba174e1d41e9e42f',
                'info.sdk_version' : 'android22',
                advertisingId: {
                    $in: idArray
                }
            }).toArray((err, data) => {
                _.map(_.uniqBy(data), (d) => {
                    if (d.info && d.info.device_token){
                        console.log(d.info.device_token);
                    }
                });
                process.exit();
            });
        }
    });
});
