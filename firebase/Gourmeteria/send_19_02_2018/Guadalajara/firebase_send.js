var FCM = require('fcm-node');
var fs = require('fs');

var atts = {
    title: "En la Gourmetería",
    body: "seguro te diviertes #tenloporseguro",
    url: "https://api.infiniadmp.com/pushwoosh/click?id_notification=14",
    banner: "https://s3-eu-west-1.amazonaws.com/infinia-sdk/gourmeteria_banner.jpg",
    api_key: "AAAAvbyr7gM:APA91bHXNz2vnbj-zDPMzYg0EUbIenHdSEyEyrOywONlvfgpcPoBTFth-3YGA1Mcl26V3i3tpntlKZduPoGgqG-qagGURdwmLUXU3ChTPdah96wj5yO0awJn0w1vX5rcKkr6wR-1TbDl",
    icon: "https://s3-eu-west-1.amazonaws.com/infinia-sdk/gourmeteria_logo.jpg"
}

var successCount = 0;
var failedCount = 0;
var startTime = new Date();
var endTime = new Date();

var failed = [];

if (process.argv.length === 3) {
    var path = __dirname + '/' + process.argv[2];
    fs.readFile(path, (err, data) => {
        if (err) {
            throw err;
        } else {
            var deviceTokens = data.toString().split('\n');
            deviceTokens = deviceTokens.filter((d) => {
                if (d) {
                    return d;
                }
            });
            const defCampaign = {
                title: atts.title,
                body: atts.body,
                url: atts.url,
                banner: atts.banner,
                api_key: atts.api_key,
                icon: atts.icon,
            };

            sendFirebase(defCampaign, deviceTokens, 0);
        }
    });
}

function sendFirebase(defCampaign, deviceTokens, index) {

    var tokenArray = deviceTokens.slice(index, index + 10);
    publish(tokenArray, defCampaign, (r) => {
        index = index + 10;
        if (index < deviceTokens.length) {
            sendFirebase(defCampaign, deviceTokens, index);
        } else {
            console.log("Total:", successCount + failedCount);
            console.log("Success:", successCount);
            console.log("Failed:", failedCount);
            console.log(JSON.stringify(failed));
            process.exit();
        }
    });
}

function publish(deviceTokens, defCampaign, callback) {

    if (deviceTokens && deviceTokens.length > 0) {
        var serverKey = '';
        var fcm = new FCM(defCampaign.api_key);
        var message;

        var banner = defCampaign.banner;
        if (banner == '-')
            banner = '';
        var icon = defCampaign.icon;
        if (icon == '-')
            icon = '';

        message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            registration_ids: deviceTokens,
            data: { //you can send only notification or only data(or include both)
                title: defCampaign.title,
                body: defCampaign.body,
                open_url: defCampaign.url,
                banner_url: banner,
                icon_url: icon,
                isInfinia: 1
            }
        };

        fcm.send(message, function(error, response) {
            console.log({ tokens : deviceTokens, error : error, response : response} );
            if (JSON.parse(response)) {
                failedCount = failedCount + JSON.parse(response).failure;
                successCount = successCount + JSON.parse(response).success;
                if (JSON.parse(response).failure > 0){
                    failed.push(deviceTokens[0]);
                }
            }
            if (error){
                if (JSON.parse(error).failure > 0){
                    failed.push(deviceTokens[0]);
                    failedCount = failedCount + JSON.parse(error).failure;
                }
            } 
            callback(1);
        });
    } else {
        callback(0);
    }
};
