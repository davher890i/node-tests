var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');

var elasticPoiUrl = 'http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/factual/poi/_search';

var writableStream = fs.createWriteStream("csv/parques.csv");
writableStream.on("finish", function() {
    console.log("DONE!", body.hits.hits.length);
    process.exit();
});

var csvStream = csv.createWriteStream({
    headers: true
});
csvStream.pipe(writableStream);

request({
    method: 'POST',
    uri: elasticPoiUrl + "?scroll=1m",
    headers: {
        'content-type': 'application/json'
    },
    body: {
        "size": 1000,
        // "_source" : ["lat", "lon"],
        "query": {
            "bool": {
                "must": [{
                        "match": {
                            "country": "es"
                        }
                    },
                    // {
                    //     "range": {
                    //         "categoryIds": {
                    //             "gte" : 123,
                    //             "lte" : 156
                    //         }
                    //     }
                    // },
                    {
                        "terms": {
                            "name": [
                                "parque"
                            ]
                        }
                    },
                    {
                        "exists" : {
                            "field" : "lat"
                        }
                    }
                ]
            }
        }
    },
    json: true
}, (error, response, body) => {
    if (!error && !body.error && body.hits && body.hits.hits) {

        for (var i = 0; i < body.hits.hits.length; i++) {
            var hit = body.hits.hits[i]._source;
            csvStream.write(hit);
        }
        if (body._scroll_id) {
            getScrollHits(csvStream, writableStream, body._scroll_id);
        } else {
            csvStream.end();
        }
    } else {
        console.log('Error in elastic query:', body);
    }
});

function getScrollHits(csvStream, fileStream, scrollId) {
    let req = {
        method: 'POST',
        uri: elasticPoiUrl + "/scroll",
        headers: {
            'content-type': 'application/json'
        },
        body: {
            "scroll": "1m",
            "scroll_id": scrollId
        },
        json: true
    };
    request(req, (error, response, body) => {
        if (!error && !body.error && body.hits && body.hits.hits) {

            for (var i = 0; i < body.hits.hits.length; i++) {
                var hit = body.hits.hits[i]._source;
                csvStream.write(hit);
            }
            if (body._scroll_id) {
                getScrollHits(csvStream, fileStream, body._scroll_id);
            } else {
                csvStream.end();
            }
        } else {
            console.log('Error in elastic query', req, body);
        }
    });
}
