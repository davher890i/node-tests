var RedisSMQ = require("rsmq");

var rsmq = new RedisSMQ({
    host: '192.168.1.42'
});

var q = 'pushDevices';

// rsmq.popMessage({
//     qname: q
// }, function(err, resp) {
//     console.log(resp);
// });

for (var i = 0; i < 10; i++) {
    rsmq.sendMessage({
        qname: q,
        message: "Message " + i
    }, function(err, resp) {
        if (err){
            console.log('Error', err);
        }
        if (resp) {
            console.log("Message sent. ID:", resp);
        }
    });
}

// var kue = require('kue');
//
// var queue = kue.createQueue({
//     // prefix: 'push',
//     redis: 'redis://192.168.1.42:6379'
//     // {
//     //     port: 6379,
//     //     host: '192.168.1.42',
//     //     auth: '',
//     // db: 3, // if provided select a non-default redis db
//     // options: {
//     //     // see https://github.com/mranney/node_redis#rediscreateclient
//     // }
//     // }
// });
//
// queue.process('push', 1, function(job, done) {
//     if (job.data.counter == 0)
//         console.log('Start' + new Date());
//
//     if (job.data.counter == 999)
//         console.log('Final' + new Date());
//
//     //finish job, pass to completed
//     done && done();
// });
