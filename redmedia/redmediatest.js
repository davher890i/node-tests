var mysql = require('mysql');
var fs = require('fs');
var csv = require('fast-csv');

// MySQL database connection
var mysqlPool = mysql.createPool({
    connectionLimit: 10,
    host: 'redmediav1.cvccucruyafd.eu-west-1.rds.amazonaws.com',
    user: 'redmedia',
    password: 'r3dm3d1a',
    database: 'redmedia'
});

let writeStream1 = fs.createWriteStream('sale_point_can_buy_product.txt');
let writeStream2 = fs.createWriteStream('products_by_sale_points.txt');
let writeStream3 = fs.createWriteStream('sale_points_campaigns.txt');

var csvFile = process.argv[2];
var stream = fs.createReadStream(csvFile);

var csvStream = csv({delimiter : ";" })
.on("data", function(data) {

	let idSalePoint = data[0].trim();
	let salePoint = data[1].trim();
	let format = data[2].trim();
	let measure = data[3].trim().replace('*', 'x');
	let language = data[4].trim();
	let campaign = data[5].trim();
	let material = data[6].trim();
	let materialMeasure = data[7].trim().replace('*', 'x');
	var product = material + ' ' + materialMeasure + ' ' + language;

	var querySP = "SELECT * FROM sale_points WHERE LOWER(id_reference) = ?;";
	mysqlPool.query(querySP, [idSalePoint.toLowerCase()], (err, result) => {
		if (result && result.length > 0){
			let idSalePoint = JSON.parse(JSON.stringify(result))[0].idsale_points;

			// writeStream3.write('\ns ' + idSalePoint);

			var queryP = "SELECT * FROM products WHERE LOWER(product) = ?;";
			mysqlPool.query(queryP, [product.toLowerCase()], (err, result) => {
				if (result && result.length > 0){
					let idProduct = JSON.parse(JSON.stringify(result))[0].idproducts;

					var queryC = "SELECT * FROM creativities WHERE LOWER(creativity) = ? AND ( NOW() BETWEEN startdate AND enddate );";
					mysqlPool.query(queryC, [campaign.toLowerCase()], (err, result) => {
						if (result && result.length > 0){

							let idCampaign = JSON.parse(JSON.stringify(result))[0].idcreativities;

							writeStream3.write('\nc ' + idCampaign);

							// var query1 = "SELECT * FROM sale_point_can_buy_product WHERE sale_point_id = ? AND product_id = ? AND creativity_id = ?";
							// mysqlPool.query(query1, [idSalePoint, idProduct, idCampaign], (err, result) => {
							// 	if (!result || result.length == 0){
							let insert1 = '\nINSERT INTO sale_point_can_buy_product VALUES (' + idSalePoint + ', ' + idProduct + ', ' + idCampaign +') ON DUPLICATE KEY UPDATE sale_point_id = ' + idSalePoint + ', product_id = ' + idProduct + ', creativity_id = ' + idCampaign + ';';
							writeStream1.write(insert1);
							// 	}
							// });

							// var query2 = "SELECT * FROM products_by_sale_points WHERE username = ? AND product_id = ? AND campaign_id = ?";
							// mysqlPool.query(query2, [idSalePoint, idProduct, idCampaign], (err, result) => {
							// 	if (!result || result.length == 0){
							let insert2 = '\nINSERT INTO products_by_sale_points VALUES (' + idSalePoint + ', "' + salePoint + '", "' + format + '", "' + measure + '", "' + language + '", "' + campaign + '", "' + material + '", "' + materialMeasure + '", "' + product + '", ' + idCampaign + ', "Material para ' + format + ' ' + measure + '");';
							writeStream2.write(insert2);
							// 	}								
							// });
						}
						else {
							console.log('No campaign in date', campaign.toLowerCase());
						}
					});
				}
				else {
					console.log('No Product', product); 
				}
			});
		}
		else {
			console.log('No Sale Point', salePoint);
		}
	});
})
// .on("end", function(){
//     process.exit(1);
// });

stream.pipe(csvStream);

