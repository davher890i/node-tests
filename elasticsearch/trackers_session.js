var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');
var moment = require('moment')

let indexName = "indoor_visitors"
var elasticUrl = 'http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/';

var writableStream = fs.createWriteStream("../csv/trackers.csv");
writableStream.on("finish", function() {
    console.log("DONE!", body.hits.hits.length);
    process.exit();
});

var csvStream = csv.createWriteStream({
    headers: true
});
csvStream.pipe(writableStream);

request({
    method: 'POST',
    uri: elasticUrl + indexName + "/_search?scroll=1m",
    headers: {
        'content-type': 'application/json'
    },
    body: {
        "size": 1000,
        "_source" : ["ap_mac", "client_mac", "rssi", "session_duration", "occurrence", "date"],
        "query": {
            "bool": {
                "must": [
                {
                  "terms": {
                    "ap_mac": [
                      "e0:cb:bc:48:50:e2"
                    ]
                  }
                },
                {
                  "range": {
                    "date": {
                      "gte": 1541030400000,
                      "lte": 1548979200000
                    }
                  }
                }
                ]
            }
        }
    },
    json: true
}, (error, response, body) => {
    if (!error && !body.error && body.hits && body.hits.hits) {

        for (var i = 0; i < body.hits.hits.length; i++) {
            var hit = body.hits.hits[i]._source;

            let d = moment(hit["date"])
            hit["day"] = d.format("YYYY/MM/DD")
            csvStream.write(hit);
        }
        if (body._scroll_id) {
            getScrollHits(csvStream, writableStream, body._scroll_id);
        } else {
            csvStream.end();
        }
    } else {
        console.log('Error in elastic query:', body);
    }
});

function getScrollHits(csvStream, fileStream, scrollId) {
    let req = {
        method: 'POST',
        uri: elasticUrl + "_search/scroll",
        headers: {
            'content-type': 'application/json'
        },
        body: {
            "scroll": "1m",
            "scroll_id": scrollId
        },
        json: true
    };
    request(req, (error, response, body) => {
        if (!error && !body.error && body.hits && body.hits.hits) {

            for (var i = 0; i < body.hits.hits.length; i++) {
                var hit = body.hits.hits[i]._source;

                let d = moment(hit["date"])
                hit["day"] = d.format("YYYY/MM/DD")
                csvStream.write(hit);
            }
            if (body._scroll_id) {
                getScrollHits(csvStream, fileStream, body._scroll_id);
            } else {
                csvStream.end();
            }
        } else {
            console.log('Error in elastic query scroll', req, body);
        }
    });
}
