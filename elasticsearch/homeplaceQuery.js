var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');

var writableStream = fs.createWriteStream("homepalce.csv");
writableStream.on("finish", function() {
    console.log("DONE!", body.hits.hits.length);
    process.exit();
});
var csvStream = csv.createWriteStream({
    headers: true
});
csvStream.pipe(writableStream);

var points = new Set();
request({
    method: 'POST',
    uri: "http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/enrichment_data/user/_search?scroll=1m",
    headers: {
        'content-type': 'application/json'
    },
    body: {
        "size": 1000,
		"_source": [ "homeplace" ],
		"query": {
			"bool": {
				"must": [
					{
						"match": {
							"country": "US"
						}
					},
					{
						"exists": {
							"field": "homeplace"
						}
					}
				]
			}
		}
    },
    json: true
}, (error, response, body) => {
	if (!error && !body.error && body.hits && body.hits.hits) {

        for (var i = 0; i < body.hits.hits.length; i++) {
            var hit = body.hits.hits[i]._source;
            // csvStream.write(hit);
            points.add(hit);
        }
        if (body._scroll_id) {
            getScrollHits(csvStream, writableStream, body._scroll_id);
        } else {
        	console.log('End first');
        	csvStream.write([points]);
            csvStream.end();
        }
    } else {
        console.log('Error in elastic query:', error || body.error);
    }
});


function getScrollHits(csvStream, fileStream, scrollId) {

    request({
        method: 'POST',
        uri: "http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/_search/scroll",
        headers: {
            'content-type': 'application/json'
        },
        body: {
            "scroll": "1m",
            "scroll_id": scrollId
        },
        json: true
    }, (error, response, body) => {
    	console.log("get Scroll", scrollId, body.hits.hits.length);
        if (!error && !body.error && body.hits && body.hits.hits) {

            for (var i = 0; i < body.hits.hits.length; i++) {
                var hit = body.hits.hits[i]._source;
                csvStream.write(hit);
            }
            if (body._scroll_id) {
                getScrollHits(csvStream, fileStream, body._scroll_id);
            } else {
            	console.log('End second');
            	csvStream.write([points]);
                csvStream.end();
            }
        } else {
            console.log('Error in elastic scroll query', error, body);
        }
    });
}