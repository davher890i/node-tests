var request = require('request');
var crypto = require('crypto');

var countryPolygonsFile = require('./ESP_adm0.json').geometries;
var provinciaPolygonsFile = require('./ESP_adm2.json').geometries;
var municipioPolygonsFile = require('./ESP_adm4.json').geometries;
var distritoPolygonsFile = require('./GEO_SC_V1_2014_DATOS_region.json').geometries;

function insertPolygonFile(jsonObj, index, level, callback){

	if (jsonObj[index]){	
		var polygon = {
			level : level,
			polygon : jsonObj[index],
			id : crypto.createHash('md5').update(JSON.stringify(jsonObj[index])).digest("hex")
		}

		insertPolygon(polygon, (error, response, body) => {
			if (error) {
				console.log('Error', error);
			}
			else {
				console.log(level, index);
			}
			insertPolygonFile(jsonObj, ++index, level, callback);
		});
	}
	else {
		callback();
	}
}

function insertPolygon(polygon, cb){
	request({
	    method: 'POST',
	    uri: "http://search-dmp-poi-b5ehns3mgq4rfs37d7edvn5pn4.eu-west-1.es.amazonaws.com/cartodb/carto",
	    headers: {
	        'content-type': 'application/json'
	    },
	    body: polygon,
	    json: true
	}, cb);
}

insertPolygonFile(countryPolygonsFile, 0, 0, () => {
	insertPolygonFile(provinciaPolygonsFile, 0, 1, () => {
		insertPolygonFile(municipioPolygonsFile, 0, 2, () => {
			insertPolygonFile(distritoPolygonsFile, 0, 3, () => {
				console.log('END');
			})
		})
	})
})