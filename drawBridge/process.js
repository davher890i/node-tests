var AWS = require('aws-sdk');
var sts = new AWS.STS();
var s3 = new AWS.S3();
var zlib = require('zlib');
var fs = require('fs');
var moment = require('moment');

var d = new Date();
var fileName = __dirname + '/' + process.argv[2];
var gzip = zlib.createGzip();
var inp = fs.createReadStream(fileName);
var s3Filename = '15418_' + d.getTime() + '.tsv.gz';
var out = fs.createWriteStream(s3Filename);
var gzipPipe = inp.pipe(gzip).pipe(out);

gzipPipe.on('finish', () => {
    console.log('File created and compressed', s3Filename);

	fs.readFile(s3Filename, function(err, data) {
		if (err) {
		    console.log(err);
		} else {
		    var base64data = new Buffer(data, 'binary');
		    
			s3.putObject({
			    Bucket: 'drawbridge-infiniamobile-15418',
			    Key: 'incoming/' + moment().format("YYYY-MM-DD") + '/' + s3Filename,
			    Body: base64data
			}, function(err, result) {
				if (err){
					console.log('Err', err);
				}
				else {
					console.log(result);
				}
			    fs.exists(s3Filename, (exists) => {
			        if (exists) {
			            fs.unlink(s3Filename, () => {

			            });
			        }
			    });
			});
		}
	});
});