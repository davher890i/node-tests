var AWS = require('aws-sdk');
var sts = new AWS.STS();

var stsPromise = new Promise((resolve, reject) => {
    var params = {
        RoleArn: 'arn:aws:iam::304259841217:role/externalDynamo',
        RoleSessionName: 'clustersession'
    };
    sts.assumeRole(params, function(err, data) {
        if (err) {
            reject(err);
        } else {
            resolve(data);
        }
    });
});

stsPromise.then((data) => {
    console.log(data);

    var creds = new AWS.Credentials({
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    });
    var sqsClient = new AWS.SQS({
        region: "eu-west-1",
        credentials: creds
    });
    var params = {};
    sqsClient.listQueues(params, function(err, data) {
        if (err) {
            console.error("Error inserting new item in DynamoDB", err);
            res.status(500).json({
                status: 'error',
                error: err
            });
        } else {
            console.log(data);
            res.status(200).json({
                status: 'ok',
                data: data
            });
        }
    });
}).catch((error) => {
    console.log('STS promise error', error);
    res.status(500).json({
        status: 'error',
        error: error
    });
});
