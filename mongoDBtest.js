var _ = require('lodash');

// MongoDB connection
var MongoClient = require('mongodb').MongoClient;
var mongoUrl = 'mongodb://' + process.env.mongodb_user + ':' + process.env.mongodb_password + '@' + process.env.mongodb_host + ':' + process.env.mongodb_port + '/admin?replicaSet=rs-ds145191';

global.mongoDBClient = {};
MongoClient.connect(mongoUrl, function(err, db) {

    global.mongoDBClient.clustersDB  = db.db(process.env.mongodb_clusters_database);
    global.mongoDBClient.indoorDB  = db.db(process.env.mongodb_indoor_database);
    global.mongoDBClient.pushDB  = db.db(process.env.mongodb_push_database);
    global.mongoDBClient.campaignsDB  = db.db(process.env.mongodb_campaigns_database);

    var myobj = { name: "Company Inc", address: "Highway 37" };
	global.mongoDBClient.pushDB.collection("customers").insertOne(myobj, function(err, res) {
		if (err) throw err;
		console.log("1 document inserted");
	});

});
