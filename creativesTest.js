var request = require('request');


var promiseArray = [];
var creatives = [{
        c: "creatives\/df3f5d488767cb9ac33c9d13c13199e8.jpg",
        f: "df3f5d488767cb9ac33c9d13c13199e8.jpg"
    },
    {
        c: "creatives\/92d9f8404b3fc7727c67ef1ecec65a15.gif",
        f: "92d9f8404b3fc7727c67ef1ecec65a15.gif"
    },
    {
        c: "creatives\/e59d94c15d31f1739eb82e81d73f49df.png",
        f: "e59d94c15d31f1739eb82e81d73f49df.png"
    },
    {
        c: "creatives\/a0b6711f5e0423f355edb8325c1bc3d8.jpg",
        f: "a0b6711f5e0423f355edb8325c1bc3d8.jpg"
    },
    {
        c: "creatives\/9f3963de3517ffe28a146db49677d284.gif",
        f: "9f3963de3517ffe28a146db49677d284.gif"
    },
    {
        c: "creatives\/9f1c49bef6f2ed940e9a4cad4520ab18.jpg",
        f: "9f1c49bef6f2ed940e9a4cad4520ab18.jpg"
    },
    {
        c: "creatives\/9e5a29daa0d86eb9947bcd03ba4f0969.jpg",
        f: "9e5a29daa0d86eb9947bcd03ba4f0969.jpg"
    },
    {
        c: "creatives\/9d75197c10a2924cd9c39dd950249580.gif",
        f: "9d75197c10a2924cd9c39dd950249580.gif"
    },
    {
        c: "creatives\/9d37e967d2007d7a6b6aa1a87807aa7b.gif",
        f: "9d37e967d2007d7a6b6aa1a87807aa7b.gif"
    },
    {
        c: "creatives\/9c37afaf8199f5c348a8b2e9f2dc6103.jpg",
        f: "9c37afaf8199f5c348a8b2e9f2dc6103.jpg"
    }
];

for (var i = 0; i < creatives.length; i++) {

    var creative = creatives[i].c;
    var filename = creatives[i].f;
    var promise = new Promise((resolve, reject) => {
        request({
            method: 'POST',
            uri: "http://localhost:3000/mediamath/creative_assets/upload",
            headers: {
                'content-type': 'application/json'
            },
            body: {
                url: "https:\/\/production-infinia.s3.amazonaws.com\/" + creative,
                filename: filename
            },
            json: true
        }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                var approve = {
                    'is_https': true,
                    'advertiserid': 191597,
                    'concept.1': 1275287,
                    'landingPage.1': 'https://ad.doubleclick.net/ddm/trackclk/N6603.283670.MOBEXT/B11243917.150010798;dc_trk_aid=320760966;dc_trk_cid=81334912;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=',
                    'click_url.1': 'https://ad.doubleclick.net/ddm/trackclk/N6603.283670.MOBEXT/B11243917.150010798;dc_trk_aid=320760966;dc_trk_cid=81334912;dc_lat=;dc_rdid=;tag_for_child_directed_treatment='
                };
                if (body.data[0].file_role == 'PRIMARY') {
                    approve['primary.1'] = approve.data[0].file_name;
                }
                if (body.data[0].file_role == 'BACKUP') {
                    approve['backup.1'] = approve.data[0].file_name;
                }
                request({
                    method: 'POST',
                    uri: "http://localhost:3000/mediamath/creative_assets/approve",
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: approve,
                    json: true
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(body);
                    }
                });
            }
        });
    });
    promiseArray.push(promise);
}

Promise.all(promiseArray).then(values => {
    console.log('Values', values);
}).catch(err => {
    console.log('Error promise', err);
})
