var AWS = require('aws-sdk');
var sts = new AWS.STS();
var moment = require('moment');

var stsPromise = new Promise((resolve, reject) => {
    var params = {
        RoleArn: 'arn:aws:iam::304259841217:role/externalDynamo',
        RoleSessionName: 'clustersession'
    };
    sts.assumeRole(params, function(err, data) {
        if (err) {
            reject(err);
        } else {
            resolve(new AWS.Credentials({
                accessKeyId: data.Credentials.AccessKeyId,
                secretAccessKey: data.Credentials.SecretAccessKey,
                sessionToken: data.Credentials.SessionToken
            }));
        }
    });
});

function scan(dynamoClient, params, callback, items = []) {
    dynamoClient.scan(params, function(err, data) {
        if (err) {
            callback(err, null);
        } else {
            if (data.Items.length > 0) {
                items = items.concat(data.Items);
            }
            if (data.LastEvaluatedKey) {
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                scan(dynamoClient, params, callback, items);
            } else {
                data.Items = items;
                callback(null, data);
            }
        }
    });
}

// MongoDB connection
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://' + process.env.mongodb_user + ':' + process.env.mongodb_password + '@' + process.env.mongodb_host + ':' + process.env.mongodb_port + '/' + process.env.mongodb_clusters_database + '?replicaSet=rs-ds145191';

MongoClient.connect(url, function(err, db) {
    if (err || !db) {
        console.log('Error connecting to MongoDB', err);
    } else {
        stsPromise.then(creds => {
            var docClient = new AWS.DynamoDB.DocumentClient({
                region: "eu-west-1",
                credentials: creds
            });

            var params = {
                TableName: "INFINIA.DMP.CLUSTER.RULES",
                FilterExpression: "#id = :id",
                ExpressionAttributeNames: {
                    "#id": "id",
                },
                ExpressionAttributeValues: {
                    ":id": "224"
                }
            };

            scan(docClient, params, (err, result) => {
                if (err) {
                    console.log('Error', err);
                } else {
                    console.log('Result', result);
                    var collection = db.collection('INFINIA.DMP.CLUSTER.RULES');
                    for (var i = 0; i < result.Items.length; i++) {
                        collection.insert(result.Items[i], {}, (err, item) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log(item);
                            }
                        });
                    }
                }
            });
        });
    }
});
