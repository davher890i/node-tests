var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');
var mysql = require('mysql');
var _ = require('lodash');

var json = 
[{"occurrences":497,"date":"2019-04-01","uniquePerm":497,"totalReturnDays":5256,"visitorReturn":358,"totalTime":5190,"halfTime":10.442655935613683,"averageReturnTime":14.681564245810057,"uniqueVisitors":1692},{"occurrences":499,"date":"2019-04-02","uniquePerm":499,"totalReturnDays":5347,"visitorReturn":300,"totalTime":5948,"halfTime":11.919839679358718,"averageReturnTime":17.823333333333334,"uniqueVisitors":2091},{"occurrences":454,"date":"2019-04-03","uniquePerm":454,"totalReturnDays":4965,"visitorReturn":262,"totalTime":5098,"halfTime":11.22907488986784,"averageReturnTime":18.950381679389313,"uniqueVisitors":1700},{"occurrences":471,"date":"2019-04-04","uniquePerm":471,"totalReturnDays":5453,"visitorReturn":294,"totalTime":5176,"halfTime":10.989384288747345,"averageReturnTime":18.547619047619047,"uniqueVisitors":2034},{"occurrences":531,"date":"2019-04-05","uniquePerm":531,"totalReturnDays":6149,"visitorReturn":328,"totalTime":5561,"halfTime":10.472693032015066,"averageReturnTime":18.746951219512194,"uniqueVisitors":1755},{"occurrences":843,"date":"2019-04-06","uniquePerm":843,"totalReturnDays":10793,"visitorReturn":452,"totalTime":7642,"halfTime":9.065243179122183,"averageReturnTime":23.878318584070797,"uniqueVisitors":2965},{"occurrences":36,"date":"2019-04-07","uniquePerm":36,"totalReturnDays":353,"visitorReturn":19,"totalTime":1290,"halfTime":35.833333333333336,"averageReturnTime":18.57894736842105,"uniqueVisitors":286},{"occurrences":431,"date":"2019-04-08","uniquePerm":431,"totalReturnDays":4610,"visitorReturn":267,"totalTime":4565,"halfTime":10.591647331786543,"averageReturnTime":17.265917602996254,"uniqueVisitors":1565},{"occurrences":375,"date":"2019-04-09","uniquePerm":375,"totalReturnDays":5334,"visitorReturn":238,"totalTime":3686,"halfTime":9.829333333333333,"averageReturnTime":22.41176470588235,"uniqueVisitors":1623},{"occurrences":479,"date":"2019-04-10","uniquePerm":479,"totalReturnDays":5375,"visitorReturn":295,"totalTime":4415,"halfTime":9.217118997912317,"averageReturnTime":18.220338983050848,"uniqueVisitors":1743},{"occurrences":463,"date":"2019-04-11","uniquePerm":463,"totalReturnDays":5593,"visitorReturn":269,"totalTime":3976,"halfTime":8.587473002159827,"averageReturnTime":20.79182156133829,"uniqueVisitors":1739},{"occurrences":533,"date":"2019-04-12","uniquePerm":533,"totalReturnDays":6930,"visitorReturn":325,"totalTime":4435,"halfTime":8.320825515947467,"averageReturnTime":21.323076923076922,"uniqueVisitors":1927},{"occurrences":999,"date":"2019-04-13","uniquePerm":999,"totalReturnDays":12617,"visitorReturn":553,"totalTime":8815,"halfTime":8.823823823823824,"averageReturnTime":22.815551537070526,"uniqueVisitors":3212},{"occurrences":17,"date":"2019-04-14","uniquePerm":17,"totalReturnDays":161,"visitorReturn":10,"totalTime":644,"halfTime":37.88235294117647,"averageReturnTime":16.1,"uniqueVisitors":227},{"occurrences":557,"date":"2019-04-15","uniquePerm":557,"totalReturnDays":6802,"visitorReturn":377,"totalTime":4129,"halfTime":7.412926391382406,"averageReturnTime":18.042440318302386,"uniqueVisitors":1711},{"occurrences":491,"date":"2019-04-16","uniquePerm":491,"totalReturnDays":6099,"visitorReturn":295,"totalTime":3532,"halfTime":7.193482688391039,"averageReturnTime":20.674576271186442,"uniqueVisitors":1922},{"occurrences":1020,"date":"2019-04-17","uniquePerm":1020,"totalReturnDays":12664,"visitorReturn":680,"totalTime":10634,"halfTime":10.425490196078432,"averageReturnTime":18.623529411764707,"uniqueVisitors":2942},{"occurrences":74,"date":"2019-04-18","uniquePerm":74,"totalReturnDays":566,"visitorReturn":36,"totalTime":541,"halfTime":7.3108108108108105,"averageReturnTime":15.722222222222221,"uniqueVisitors":475},{"occurrences":61,"date":"2019-04-19","uniquePerm":61,"totalReturnDays":784,"visitorReturn":41,"totalTime":532,"halfTime":8.721311475409836,"averageReturnTime":19.121951219512194,"uniqueVisitors":406},{"occurrences":1572,"date":"2019-04-20","uniquePerm":1573,"totalReturnDays":19772,"visitorReturn":972,"totalTime":19809,"halfTime":12.601145038167939,"averageReturnTime":20.34156378600823,"uniqueVisitors":4769},{"occurrences":54,"date":"2019-04-21","uniquePerm":54,"totalReturnDays":321,"visitorReturn":24,"totalTime":873,"halfTime":16.166666666666668,"averageReturnTime":13.375,"uniqueVisitors":370},{"occurrences":1483,"date":"2019-04-22","uniquePerm":1484,"totalReturnDays":12942,"visitorReturn":626,"totalTime":18138,"halfTime":12.230613621038435,"averageReturnTime":20.6741214057508,"uniqueVisitors":5403},{"occurrences":116,"date":"2019-04-23","uniquePerm":120,"totalReturnDays":1098,"visitorReturn":69,"totalTime":1462,"halfTime":12.60344827586207,"averageReturnTime":15.91304347826087,"uniqueVisitors":703},{"occurrences":878,"date":"2019-04-24","uniquePerm":879,"totalReturnDays":10953,"visitorReturn":615,"totalTime":9831,"halfTime":11.197038724373577,"averageReturnTime":17.809756097560975,"uniqueVisitors":3578},{"occurrences":1380,"date":"2019-04-25","uniquePerm":1381,"totalReturnDays":18325,"visitorReturn":860,"totalTime":17955,"halfTime":13.01086956521739,"averageReturnTime":21.308139534883722,"uniqueVisitors":4288},{"occurrences":890,"date":"2019-04-26","uniquePerm":891,"totalReturnDays":10235,"visitorReturn":547,"totalTime":10054,"halfTime":11.296629213483145,"averageReturnTime":18.711151736745887,"uniqueVisitors":2713},{"occurrences":1271,"date":"2019-04-27","uniquePerm":1271,"totalReturnDays":16786,"visitorReturn":795,"totalTime":15499,"halfTime":12.194335169158142,"averageReturnTime":21.11446540880503,"uniqueVisitors":4144},{"occurrences":86,"date":"2019-04-28","uniquePerm":86,"totalReturnDays":687,"visitorReturn":55,"totalTime":1251,"halfTime":14.546511627906977,"averageReturnTime":12.49090909090909,"uniqueVisitors":468},{"occurrences":989,"date":"2019-04-29","uniquePerm":989,"totalReturnDays":13366,"visitorReturn":706,"totalTime":11338,"halfTime":11.464105156723964,"averageReturnTime":18.93201133144476,"uniqueVisitors":2833},{"occurrences":986,"date":"2019-04-30","uniquePerm":986,"totalReturnDays":11548,"visitorReturn":624,"totalTime":10402,"halfTime":10.549695740365111,"averageReturnTime":18.506410256410255,"uniqueVisitors":2857},{"occurrences":88,"date":"2019-05-01","uniquePerm":88,"totalReturnDays":1053,"visitorReturn":63,"totalTime":1466,"halfTime":16.65909090909091,"averageReturnTime":16.714285714285715,"uniqueVisitors":521},{"occurrences":972,"date":"2019-05-02","uniquePerm":972,"totalReturnDays":10576,"visitorReturn":709,"totalTime":10274,"halfTime":10.569958847736626,"averageReturnTime":14.916784203102962,"uniqueVisitors":2638},{"occurrences":997,"date":"2019-05-03","uniquePerm":998,"totalReturnDays":12963,"visitorReturn":609,"totalTime":10932,"halfTime":10.964894684052156,"averageReturnTime":21.285714285714285,"uniqueVisitors":3037},{"occurrences":1439,"date":"2019-05-04","uniquePerm":1441,"totalReturnDays":18827,"visitorReturn":851,"totalTime":16655,"halfTime":11.574009728978456,"averageReturnTime":22.123384253819037,"uniqueVisitors":3560},{"occurrences":544,"date":"2019-05-05","uniquePerm":546,"totalReturnDays":6110,"visitorReturn":350,"totalTime":6648,"halfTime":12.220588235294118,"averageReturnTime":17.457142857142856,"uniqueVisitors":1641},{"occurrences":859,"date":"2019-05-06","uniquePerm":860,"totalReturnDays":9890,"visitorReturn":595,"totalTime":10477,"halfTime":12.19674039580908,"averageReturnTime":16.6218487394958,"uniqueVisitors":3856},{"occurrences":538,"date":"2019-05-07","uniquePerm":538,"totalReturnDays":5585,"visitorReturn":348,"totalTime":4844,"halfTime":9.003717472118959,"averageReturnTime":16.048850574712645,"uniqueVisitors":2170},{"occurrences":723,"date":"2019-05-08","uniquePerm":723,"totalReturnDays":7756,"visitorReturn":500,"totalTime":6449,"halfTime":8.919778699861688,"averageReturnTime":15.512,"uniqueVisitors":2343},{"occurrences":755,"date":"2019-05-09","uniquePerm":755,"totalReturnDays":7988,"visitorReturn":485,"totalTime":8118,"halfTime":10.752317880794703,"averageReturnTime":16.470103092783503,"uniqueVisitors":3494},{"occurrences":909,"date":"2019-05-10","uniquePerm":909,"totalReturnDays":10921,"visitorReturn":613,"totalTime":9784,"halfTime":10.763476347634764,"averageReturnTime":17.815660685154974,"uniqueVisitors":4160},{"occurrences":1268,"date":"2019-05-11","uniquePerm":1268,"totalReturnDays":15330,"visitorReturn":741,"totalTime":16101,"halfTime":12.697949526813881,"averageReturnTime":20.68825910931174,"uniqueVisitors":3478},{"occurrences":87,"date":"2019-05-12","uniquePerm":88,"totalReturnDays":820,"visitorReturn":51,"totalTime":848,"halfTime":9.74712643678161,"averageReturnTime":16.07843137254902,"uniqueVisitors":469}]

// var daysArray={};

// var objArray = [];

// _.map(json, (val) => {

// _.map(val, (value, day) => {

//     if (!daysArray[day]){
//         daysArray[day] = {};
//     }

//     Object.assign(daysArray[day], value);
//     });
// });

// _.map(daysArray, (val, day) => {

//     val.day = day;
//     console.log(val);
//     objArray.push(val);
// });

// console.log(objArray);


let objArray = json;

var csvStream = csv.createWriteStream({
        headers: true
    }),
    writableStream = fs.createWriteStream("indoor.csv");
writableStream.on("finish", function() {
    console.log("DONE!", objArray.length);
});
csvStream.pipe(writableStream);
for (var i = 0; i < objArray.length; i++) {
    csvStream.write(objArray[i]);
}
csvStream.end();
