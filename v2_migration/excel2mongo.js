var _ = require('lodash');
var fs = require('fs');
var csv = require('fast-csv');
var moment = require('moment');
var mysql = require('mysql');

// MySQL database connection
var mysqlPool = mysql.createPool({
    connectionLimit: 10,
    host: 'infinia-staging.cbzqxna4rbqs.eu-west-1.rds.amazonaws.com',
    user: 'infinia_pre',
    password: 'YXzZe2kGgJ4W83ZY',
    database: 'infinia_platform',
});

// MongoDB connection
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://infinia:14fi4IA@ds145191-a0.mlab.com:45191/campaigns';

MongoClient.connect(url, function(err, db) {
    if (err || !db) {
        console.log('Error connecting to MongoDB', err);
    } else {
        if (process.argv.length != 4) {
            console.log('Wrong number of arguments');
            process.exit();
        }
        var queryStr = "INSERT INTO campaigns (bill_to, budget, budget_capping_amount, budget_capping_time_amount, budget_capping_time_interval, budget_capping_type, created_at, ctr, data_cost, delivery_cost, dsp_message, ";
        queryStr += "dsp_status, end_date, frequency_amount, frequency_interval, frequency_type, goal_type, goal_value, id_office, id_trafficker, impressions, impressions_capping_amount, impressions_capping_time_amount, ";
        queryStr += "impressions_capping_time_interval, impressions_capping_type, io_ref, markups, mediamath_id, name, other_cost, post_click, post_click_time_unit, post_impression, post_impression_time_unit, start_date, status, ";
        queryStr += "third_party_pixel, updated_at, id_advertiser, id_agency, id_reseller) VALUES ('', 0, 0, 0, 'day', 'no-limit', NOW(), 0, 0, 0, '', 'SUCCESS', NOW(), 0, 'day', 'no-limit', '', 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, 0, '" + process.argv[3] + "', 0, 0, NULL, 0, NULL, ";
        queryStr += "NOW(), 0, NULL, NOW(), NULL, 9, 1000)";

        mysqlPool.query(queryStr, (error, body) => {
            if (error) {
                console.log(error);
            } else {
                var campaign = JSON.parse(JSON.stringify(body));
                var csvFile = process.argv[2];
                var stream = fs.createReadStream(csvFile);
                var promiseArray = [];
                var creative;
                var strategy;
                var startDate;

                var totalClicks = 0;
                var totalImpressions = 0;

                var performance = {};
                csv.fromStream(stream, {
                        headers: true
                    })
                    .on("data", function(data) {

                        var split = data['Fecha'].split('/');
                        startDate = moment();
                        startDate.day(parseInt(split[1]));
                        startDate.month(parseInt(split[0]) - 1);
                        startDate.year('20' + split[2]);
                        startDate = moment(startDate);

                        var clicks = Number(data['Clicks']);
                        totalClicks += clicks;
                        var impressions = Number(data['Imps'].replace('.', ''));
                        totalImpressions += impressions;

                        var reporting = {
                            ctr: clicks * 100 / impressions,
                            creativeName: data['Creatividad'],
                            totalConversions: 0,
                            endDate: startDate.format('YYYY-MM-DD'),
                            orderLineName: data['Estrategia'],
                            impressions: impressions,
                            postClickConversions: 0,
                            revenue: 0,
                            ctc: "0",
                            postViewConversions: 0,
                            clicks: clicks,
                            totalRevenue: 0,
                            campaignName: data['Campaña'],
                            postClickRevenue: 0,
                            startDate: startDate.format('YYYY-MM-DD'),
                            postViewRevenue: 0
                        };

                        // New creative
                        if (creative != data['Creatividad']) {
                            creative = data['Creatividad'];
                        }
                        // New strategy
                        if (strategy != data['Estrategia']) {
                            strategy = data['Estrategia'];
                        }

                        if (!performance[strategy]) {
                            performance[strategy] = {};
                        }
                        if (!performance[strategy][creative]) {
                            performance[strategy][creative] = {};
                        }
                        performance[strategy][creative][startDate.format('YYYY-MM-DD')] = reporting;
                    })
                    .on("end", function() {
                        console.log("done");
                        var reporting = {
                            idCampaign: campaign.insertId,
                            idCampaignPlatform: campaign.insertId,
                            campaignName: process.argv[3],
                            idReseller: 1000,
                            resellerName: 'MOBEXT',
                            agencyId: 9,
                            agencyName: 'HAVAS',
                            advertiserId: 0,
                            advertiserName: 0,
                            clicks: totalClicks,
                            impressions: totalImpressions,
                            ctr: totalClicks * 100 / totalImpressions,
                            performance: performance,
                            geo: {},
                            appTransparency: {},
                            startDate: startDate.toDate(),
                            endDate: startDate.toDate(),
                            status: 1
                        };
                        db.collection('reporting').insert(reporting, {}, (err, item) => {
                            if (err) {
                                console.log(err);
                            }
                            var queryStr = "UPDATE campaigns SET mediamath_id = " + campaign.insertId + " WHERE id = " + campaign.insertId;

                            mysqlPool.query(queryStr, (error, body) => {
                                if (error) {
                                    console.log(error);
                                } else {}
                                process.exit();
                            });
                            db.close();

                        });
                    });
            }
        });
    }
});
