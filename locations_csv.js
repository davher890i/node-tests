var fs = require('fs');
var csv = require('fast-csv');

var csvFile = process.argv[2];
var stream = fs.createReadStream(csvFile);

var attributes = [{
        values: [],
        name: "lat"
    },
    {
        values: [],
        name: "lon"
    },
    {
        values: [],
        name: "radius"
    }
];

var metadataLocation = [];
var csvStream = csv()
    .on("data", function(data) {

        if (data[0] && data[0] !== "null" && data[0] !== null && Number(data[0]) && data[1] && data[1] !== "null" && data[1] !== null && Number(data[1]) && data[2] && data[2] !== "null" && data[2] !== null && Number(data[2])) {
            attributes[0].values.push(Number(data[0].trim()));
            attributes[1].values.push(Number(data[1].trim()));
            attributes[2].values.push(Number(data[2].trim()));

            metadataLocation.push({
                "lng": Number(data[1].trim()),
                "rad": Number(data[2].trim()),
                "lat": Number(data[0].trim()),
                "type": "poi"
            });
        }
    })
    .on("end", function() {
        // console.log(JSON.stringify(attributes));

        console.log(JSON.stringify(metadataLocation));
    });

stream.pipe(csvStream);
