var request = require('request');
var csv = require('fast-csv');
var fs = require('fs');
var _ = require('lodash'); 

request({
    method: 'POST',
    uri: "http://ec2-34-244-105-190.eu-west-1.compute.amazonaws.com:9200/indoor.visitors/_doc/_search",
    headers: {
        'content-type': 'application/json'
    },
    body: {
	  "query": {
	    "bool": {
	      "must": {
	        "match_all": {}
	      },
	      "filter": [
	        {
	          "range": {
	            "date": {
	              "gte": "2018-04-11",
	              "lte": "2018-04-20",
	              "format": "yyyy-MM-dd"
	            }
	          }
	        },
	        {
	          "range": {
	            "rssi": {
	              "gt": 1
	            }
	          }
	        },
	        {
	          "terms": {
	            "ap_mac": [
	              "0c:8d:db:db:01:d7"
	            ]
	          }
	        }
	      ]
	    }
	  },
	  "size": 0,
	  "aggs": {
	    "client_mac": {
	      "terms": {
	        "field": "client_mac",
	        "size" : 2147483647
	      },
	      "aggs": {
	        "day": {
	          "date_histogram": {
	            "field": "date",
	            "interval": "1d"
	          },
	          "aggs": {
	            "occurrences": {
	              "sum": {
	                "field": "occurrence"
	              }
	            },
	            "occurrences_filter": {
	              "bucket_selector": {
	                "buckets_path": {
	                  "occurrences": "occurrences"
	                },
	                "script": "params.occurrences > 10 && params.occurrences < 480"
	              }
	            }
	          }
	        }
	      }
	    }
	  }
	},
    json: true
}, (error, response, body) => {

		var uniquePerm = 0
		_.map(body.aggregations.client_mac.buckets, (bucket) => {
			if (bucket.day.buckets.length > 0){
				uniquePerm++;
			}
		});
		console.log(uniquePerm);
});